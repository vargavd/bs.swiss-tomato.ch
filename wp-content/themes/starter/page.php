<?php
/*
 *
 * Page Template
 *
 */
get_header();

$is_page_parent = get_pages(['child_of' => get_the_ID()]);
$page_id 		= $post->post_parent ?: get_the_ID();
$hero_bg 		= get_the_post_thumbnail_url($page_id, 'full') ?: '';
$child_logo 	= get_field('child_logo', get_the_ID()) ?: '';

if (get_the_post_thumbnail_url(get_the_ID(), 'full')) {
	$hero_bg = get_the_post_thumbnail_url(get_the_ID(), 'full');
}
?>

<!-- Hero Header -->
<section id="hero__header" class="page__<?= $page_id ?>">
	<?php if ($hero_bg) : ?>
		<?= custom_image_size($hero_bg, 'full', get_the_title(), 'hero__header__bg', 'none') ?>
	<?php endif; ?>
	<div class="grid hero__header__grid inner narrow">
		<header class="hero__header">
			<?php if ($child_logo) : ?>
				<?= custom_image_size($child_logo, 'logo', get_the_title(), 'hero__header__logo', '', 'none') ?>
			<?php endif; ?>
			
			<h1 class="hero__header__title">
        <?php if ($post->post_parent) : ?>
          <a href="<?= the_permalink($post->post_parent) ?>" title="<?= get_the_title($post->post_parent) ?>" data-icon="" class="to__parent"><span class="screen-reader-text"><?= the_title($post->post_parent) ?></span></a>
        <?php endif; ?>

        <?= the_title() ?>
      </h1>
		</header>
	</div>
</section>

<?php while (have_posts()) : the_post(); ?>
	<?php the_content(); ?>
<?php endwhile; // End of the loop.
?>

<?php

get_footer();

// EOF