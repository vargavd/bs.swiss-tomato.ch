<?php
/*
 *
 * Template Name: Simple
 *
 */
get_header(); ?>

<?php while (have_posts()) : the_post(); ?>

	<article id="simple__container" <?php post_class('inner'); ?>>

		<div class="grid simple__grid">

			<!-- Meta -->
			<div class="simple__meta"></div>

			<!-- Content -->
			<div id="simple__content" class="simple__content">

				<header>
					<!-- Title -->
					<h1 class="simple__title"><?php the_title(); ?></h1>
				</header>

				<?php
				/* translators: %s: Name of current post */
				the_content(sprintf(
					__('Continue reading<span class="screen-reader-text"> "%s"</span>', 'bs'),
					get_the_title()
				));
				?>

			</div><!-- .entry-content -->

		</div><!-- .simple__grid -->

	</article><!-- #post -->

<?php endwhile; ?>

<?php

get_footer();
