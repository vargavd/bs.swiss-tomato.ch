<?php
/*
 *
 * Template - Single Events
 *
 */
get_header();

$post = get_the_ID();
$no_bg = has_post_thumbnail() ? '' : ' no__bg';

// Event info
$event_start  = trim(get_field('event_start')) ?: '';
$event_end 	  = trim(get_field('event_end')) ?: '';

?>

<article id="event__container" <?php post_class(); ?>>

	<!-- Header -->
	<div class="event__header<?= $no_bg ?>">

		<!-- Image -->
		<?php if (has_post_thumbnail()) : ?>
			<div class="event__thumbnail">
				<?php the_post_thumbnail() ?>
			</div>
		<?php endif; ?>

		<div class="event__wrapper">

			<!-- Title -->
			<header class="event__title">
				<h1><?php the_title(); ?></h1>
			</header>

			<!-- Event Info -->
			<div class="event__info">
				<?php if ($event_start) : ?>

					<?php

					if ($event_end) {

						// Event Start Split
						$start_year  = date_i18n('Y', strtotime($event_start));
						$start_month = date_i18n('m', strtotime($event_start));
						$start_day   = date_i18n('d', strtotime($event_start));

						// Event End Split
						$end_year  = date_i18n('Y', strtotime($event_end));
						$end_month = date_i18n('m', strtotime($event_end));
						$end_day   = date_i18n('d', strtotime($event_end));

						// Year ++
						if ($start_year < $end_year) {
							$event_start = date_i18n('d F Y', strtotime($event_start)) . ' - ' . date_i18n('d F Y', strtotime($event_end));
						}
						// Month ++
						if ($start_year === $end_year && $start_month < $end_month) {
							$event_start = date_i18n('d F', strtotime($event_start)) . ' - ' . date_i18n('d F', strtotime($event_end)) . ' ' . $start_year;
						}
						// Day ++
						if ($start_year === $end_year && $start_month === $end_month && $start_day < $end_day) {
							$event_start = date_i18n('d', strtotime($event_start)) . '-' . date_i18n('d F', strtotime($event_end)) . ' ' . $start_year;
						}
					}

					?>

					- <?= $event_start ?>

				<?php endif; ?>

			</div>

		</div>

	</div>

	<!-- Content -->
	<div class="event__content">

		<?php
		/* translators: %s: Name of current post */
		the_content(sprintf(
			__('Continue reading<span class="screen-reader-text"> "%s"</span>', 'bs'),
			get_the_title()
		));
		?>

		<?php
		wp_link_pages([
			'before'      => '<div class="page-links">' . esc_html__('Pages:', 'bs'),
			'after'       => '</div>',
			'link_before' => '<span class="page-number">',
			'link_after'  => '</span>',
		]);
		?>
	</div><!-- .entry-content -->

</article><!-- #post -->

<?php

get_footer();

// EOF