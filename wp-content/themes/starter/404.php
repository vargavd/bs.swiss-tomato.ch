<?php

/**
 * The template for displaying 404 Error
 */
get_header(); ?>

<!-- Hero Header -->
<div id="hero__header" class="page__404"></div>

<div id="content__container" class="inner">
	<div class="grid error404__grid col__2">
		<div class="error404__content">
			<header>
				<h1 class="entry__title"><?= esc_html__('Page not found', 'bs') ?></h1>
			</header>
			<p><?= esc_html__('We can’t seem to find the page you were looking for.', 'bs'); ?></p>
			<a href="<?= esc_url(home_url('/')) ?>" class="button red"><?= esc_html__('Back to Home', 'bs') ?></a>
		</div>
		<div class="error404__image"></div>
	</div>
</div>

<?php

get_footer();

// EOF