<?php

/**
 * WordPress Core - Customization
 * 
 * This file contains the following configurations:
 * 
 * // WP mail defaults
 * * Custom Page States
 * * Custom Error Message
 * * Searchable ACF fields
 */


/*  
// WP mail From Name
add_filter( 'wp_mail_from_name', 'configure_from_name' );
function configure_from_name( $name ) {
	return get_option('blogname');
}

// WP mail From Email
add_filter( 'wp_mail_from', 'configure_from_email' );
function configure_from_email( $email ) {
	return "partner@shoprenter.hu";
}
*/


// Custom Post States
add_filter('display_post_states', 'custom_post_states', 10, 2);

function custom_post_states($states, $post)
{
	if (('page' === get_post_type($post->ID))
		&& ('template-faq.php' === esc_html(get_page_template_slug($post->ID)))
	) {
		$states['custom_faq'] = esc_html__('FAQ Page', 'bs');
	}
	if (('page' === get_post_type($post->ID))
		&& ('template-contact.php' === esc_html(get_page_template_slug($post->ID)))
	) {
		$states['custom_contact'] = esc_html__('Contact Page', 'bs');
	}
	return $states;
}

// Change Error Message
add_filter('login_errors', 'wrong_login');

function wrong_login()
{
	return esc_html__('Wrong data', 'bs');
}


// Add Postmeta Table to Search
add_filter('posts_join', 'acf_search_join');

function acf_search_join($join)
{
	global $wpdb;
	//if ( is_admin() && is_search() && ! empty( $_GET['post_type'] ) && $_GET['post_type'] == 'ajde_events' && ! empty( $_GET['s'] ) ) {
	if (is_admin() && is_search()) {
		$join .= ' LEFT JOIN ' . $wpdb->postmeta . ' ON ' . $wpdb->posts . '.ID = ' . $wpdb->postmeta . '.post_id ';
	}
	return $join;
}


// All Postmeta is Searchable
add_filter('posts_where', 'acf_post_search_where');

function acf_post_search_where($where)
{
	global $pagenow, $wpdb;
	if (is_admin() && is_search()) {
		$where = preg_replace(
			"/\(\s*" . $wpdb->posts . ".post_title\s+LIKE\s*(\'[^\']+\')\s*\)/",
			"(" . $wpdb->posts . ".post_title LIKE $1) OR (" . $wpdb->postmeta . ".meta_value LIKE $1)",
			$where
		);
	}
	return $where;
}


// Prevent Duplicates
add_filter('posts_distinct', 'acf_search_distinct');

function acf_search_distinct($where)
{
	global $wpdb;

	if (is_admin() && is_search()) {
		return "DISTINCT";
	}

	return $where;
}


// Modify Edit Post Link Structure
add_filter('edit_post_link', 'edit_link_blank', 10, 3);
function edit_link_blank($link, $post_id, $text)
{
	if (false === strpos($link, 'target='))
		$link = str_replace('<a ', '<a ', $link);
	return $link;
}


// Modify Edit Term Link Structure
add_filter('edit_term_link', 'edit_term_link_blank', 10, 2);
function edit_term_link_blank($link, $term_id)
{
	if (false === strpos($link, 'target='))
		$link = str_replace('<a ', '<a class="post-edit-link" ', $link);
	return $link;
}


// EOF