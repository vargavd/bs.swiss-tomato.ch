<?php

/**
 * AJAX Functions - Search, News & Events
 * 
 * This file contains the following configurations:
 * 
 * * AJAX Filter + Loadmore
 */


// AJAX Loadmore INIT
add_action('init', 'ajax_filter_init');
function ajax_filter_init()
{

	// Custom Filter
	add_action('wp_ajax_ajaxfilter', 'ajax_filter'); // wp_ajax_{ACTION HERE} 
	add_action('wp_ajax_nopriv_ajaxfilter', 'ajax_filter');
}


function ajax_filter()
{

	// First check the nonce, if it fails the function will break
	check_ajax_referer('ajax-filter-loadmore-nonce', 'nonce');

	// Vars
	$cpt 	= isset($_POST['cpt']) ? sanitize_text_field($_POST['cpt']) : 'post';
	$ppp	= isset($_POST['ppp']) ? (int) $_POST['ppp'] : 1;
	$page 	= isset($_POST['page']) ? (int) $_POST['page'] : 1;

	$args = [
		'suppress_filters' => false,
		'post_type' 	   => $cpt,
		'post_status' 	   => 'publish',
		'posts_per_page'   => $ppp,
		'paged'			   => $page,
	];

	// If Post
	if ($cpt === 'post') {
		// Category
		if (isset($_POST['cat']) && $_POST['cat'] !== '') {
			$cat = (int) $_POST['cat'];
			$args['tax_query'][] = [
				[
					'taxonomy' => 'category',
					'field'	   => 'term_id',
					'terms'	   => $cat,
					'operator' => 'IN',
				]
			];
		}
	}

	// If Resources
	if ($cpt === 'resources') {

		// Product
		if (isset($_POST['product']) && $_POST['product'] !== '') {
			$product = (int) $_POST['product'];
			$args['tax_query'][] = [
				[
					'taxonomy' => 'product',
					'field'	   => 'term_id',
					'terms'    => $product,
					'operator' => 'IN',
				]
			];
		}

		// Resource type
		if (isset($_POST['resource__type']) && $_POST['resource__type'] !== '') {
			$resource_type = (int) $_POST['resource__type'];
			$args['tax_query'][] = [
				[
					'taxonomy' => 'resource-type',
					'field'	   => 'term_id',
					'terms'    => $resource_type,
					'operator' => 'IN',
				]
			];
		}

		// Search
		if (isset($_POST[$cpt . '__search']) && $_POST[$cpt . '__search'] !== '') {
			$args['s'] = $_POST[$cpt . '__search'];
		}
	}

	// Query
	$posts = new WP_Query($args);

	if ($posts->have_posts()) : ?>

		<?php ob_start(); ?>

		<div class="grid col__2 gap__2 <?= $cpt ?>__container">

			<?php
			// Loop
			while ($posts->have_posts()) : $posts->the_post();
				$post_id = get_the_ID() ?>
				<?php get_template_part('parts/components/filter', NULL, ['post_id' => $post_id, 'post_type' => $cpt]) ?>
			<?php endwhile;
			wp_reset_postdata(); ?>

		</div>

		<?php if ($posts->found_posts > $ppp) : ?>

			<div class="loadmore text-center">
				<button class="load__more <?= $cpt ?>__loader button" data-page="<?= $page + 1 ?>"><?= esc_html__('Load more', 'bs') ?></button>
			</div>

		<?php endif; ?>
	<?php

		// Pass the posts to variable
		$posts_html = ob_get_contents();
		// clear the buffer
		ob_end_clean();

	else :

		$posts_html = '<div class="grid ' . $cpt . '__container"><p class="no__result">' . esc_html__('Nothing found for your criteria', 'bs') . '</p></div>';

	endif;

	echo json_encode([
		'max_page' => $posts->max_num_pages,
		'content' => $posts_html
	]);
	exit();
}



// AJAX Loadmore INIT
add_action('init', 'ajax_loadmore_init');
function ajax_loadmore_init()
{

	// Load More 
	add_action('wp_ajax_loadmore', 'ajax_loadmore');
	add_action('wp_ajax_nopriv_loadmore', 'ajax_loadmore');
}


// AJAX Loadmore
function ajax_loadmore()
{

	// First check the nonce, if it fails the function will break
	check_ajax_referer('ajax-filter-loadmore-nonce', 'nonce');

	// Vars
	$cpt 	= isset($_POST['cpt']) ? sanitize_text_field($_POST['cpt']) : 'post';
	$ppp	= isset($_POST['ppp']) ? (int) $_POST['ppp'] : 1;
	$page 	= isset($_POST['page']) ? (int) $_POST['page'] : 1;

	$args = [
		'suppress_filters' => false,
		'post_type' 	   => $cpt,
		'post_status' 	   => 'publish',
		'posts_per_page'   => $ppp,
		'paged'			   => $page + 1,
	];

	// If Post
	if ($cpt === 'post') {
		// Category
		if (isset($_POST['cat']) && $_POST['cat'] !== '') {
			$cat = (int) $_POST['cat'];
			$args['tax_query'][] = [
				[
					'taxonomy' => 'category',
					'field'	   => 'term_id',
					'terms'	   => $cat,
					'operator' => 'IN',
				]
			];
		}
	}

	// If Resources
	if ($cpt === 'resources') {

		// Product
		if (isset($_POST['product']) && $_POST['product'] !== '') {
			$product = (int) $_POST['product'];
			$args['tax_query'][] = [
				[
					'taxonomy' => 'product',
					'field'	   => 'term_id',
					'terms'    => $product,
					'operator' => 'IN',
				]
			];
		}

		// Resource type
		if (isset($_POST['resource_type']) && $_POST['resource_type'] !== '') {
			$resource_type = (int) $_POST['resource_type'];
			$args['tax_query'][] = [
				[
					'taxonomy' => 'resource-type',
					'field'	   => 'term_id',
					'terms'    => $resource_type,
					'operator' => 'IN',
				]
			];
		}

		// Search
		if (isset($_POST[$cpt . '__search']) && $_POST[$cpt . '__search'] !== '') {
			$args['s'] = $_POST[$cpt . '__search'];
		}
	}


	// Query
	$posts = new WP_Query($args);

	/*
		pr1($args);
		echo '<br>';
		echo 'found: ' . $posts->found_posts;
		echo '<br>';
		echo 'max: ' . $posts->max_num_pages;
		echo '<br>';
		echo 'ppp: ' . $ppp;
		echo '<br>';
		echo 'page: ' . $page;*/


	if ($posts->have_posts()) : ?>

		<?php
		// Loop
		while ($posts->have_posts()) : $posts->the_post();
			$post_id = get_the_ID() ?>
			<?php get_template_part('parts/components/filter', NULL, ['post_id' => $post_id, 'post_type' => $cpt]) ?>
		<?php endwhile;
		wp_reset_postdata(); ?>

<?php

	endif;
	exit();
}


// EOF