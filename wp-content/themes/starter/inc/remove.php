<?php

/**
 * Remove Theme Parts
 * 
 * This file contains the following configurations:
 * 
 * * Remove Admin Bar Margin
 * * Remove Post's / Page's Author columns
 * * Remove CSS / JS versions
 * * Remove Xmlrpc / Pingbacks
 * * Remove Head actions
 */


// Remove Admin Bar Extra Margin
add_action('get_header', 'my_filter_head');

function my_filter_head()
{
	remove_action('wp_head', '_admin_bar_bump_cb');
}

/**
 * Unique Column for Featured Image
 * 
 * @param string[] $columns - An associative array of column headings.
 */
add_filter('manage_posts_columns', 'remove_posts_columns');
function remove_posts_columns($columns)
{
	//unset($columns['comments']);
	unset($columns['author']);
	return $columns;
}


/**
 * Remove Page's Columns
 * 
 * @param string[] $columns - An associative array of column headings.
 */
add_filter('manage_pages_columns', 'remove_pages_columns');
function remove_pages_columns($columns)
{
	unset($columns['comments']);
	unset($columns['author']);
	return $columns;
}


// Hide CSS / JS Version Number
add_filter('style_loader_src', 'remove_cssjs_ver', 10, 2);
add_filter('script_loader_src', 'remove_cssjs_ver', 10, 2);

function remove_cssjs_ver($src)
{
  if (strpos($src, 'themes/starter/assets')) {
    return $src;
  }

	if (strpos($src, '?ver=') && !strpos($src, 'style.min.css'))
		$src = remove_query_arg('ver', $src);
	return $src;
}

// Remove Xmlrpc, Pingback support
add_filter('xmlrpc_methods', 'remove_xmlrpc_pingback_ping');

function remove_xmlrpc_pingback_ping($methods)
{
	unset($methods['pingback.ping']);
	return $methods;
}

// Remove Head Actions
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'noindex');
remove_action('wp_head', 'wp_resource_hints', 2);
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'wp_oembed_add_host_js');
remove_action('wp_head', 'rest_output_link_wp_head', 10);
remove_action('wp_head', 'wp_oembed_add_discovery_links', 10);
remove_action('wp_head', 'print_emoji_detection_script', 7);


// Remove WP Styles
remove_action('admin_print_scripts', 'print_emoji_detection_script');
remove_action('wp_print_styles', 'print_emoji_styles');
remove_action('admin_print_styles', 'print_emoji_styles');


// EOF