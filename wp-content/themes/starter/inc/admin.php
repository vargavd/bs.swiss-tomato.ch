<?php

/** 	
 * Admin Customization
 * 
 * This file contains the following configurations:
 * 
 * * Date Range filter
 * * Admin Bar CSS
 * * Featured Image Column
 * // Custom admin column / row 
 */


// From Date To Date Filter class
class DateRange
{

	function __construct()
	{

		// If you do not want to remove default "by month filter", remove/comment this line
		add_filter('months_dropdown_results', '__return_empty_array');

		// Include CSS/JS, in our case jQuery UI datepicker
		add_action('admin_enqueue_scripts', [$this, 'jqueryui']);

		// HTML of the filter
		add_action('restrict_manage_posts', [$this, 'form']);

		// The function that filters posts
		add_action('pre_get_posts', [$this, 'filterquery']);
	}

	//If you use WooCommerce, you can skip this function completely
	function jqueryui()
	{
		wp_enqueue_style('jquery-ui', '//code.jquery.com/ui/1.12.1/themes/smoothness/jquery-ui.min.css');
		wp_enqueue_script('jquery-ui-datepicker');
	}

	// If you would like to move CSS and JavaScript to the external file - welcome
	function form()
	{

		$from = (isset($_GET['DateFrom']) && $_GET['DateFrom']) ? $_GET['DateFrom'] : '';
		$to = (isset($_GET['DateTo']) && $_GET['DateTo']) ? $_GET['DateTo'] : '';

		echo '<style>
			input[name="DateFrom"], input[name="DateTo"]{
				line-height: 28px;
				height: 28px;
				margin: 0;
				width:125px;
			}
			</style>
	 
			<input type="text" name="DateFrom" placeholder="' . esc_html__('Date From', 'bs') . '" value="' . esc_attr($from) . '" />
			<input type="text" name="DateTo" placeholder="' . esc_html__('Date To', 'bs') . '" value="' . esc_attr($to) . '" />

			<script>
			jQuery( function($) {
				var from = $(\'input[name="DateFrom"]\'),
					to = $(\'input[name="DateTo"]\');
	 
				$( \'input[name="DateFrom"], input[name="DateTo"]\' ).datepicker( {dateFormat : "mm/dd/yy"} );
					// by default, the dates look like this "April 3, 2017"
					// I decided to make it 2017-04-03 with this parameter datepicker({dateFormat : "mm/dd/yy"});
	 
					// the rest part of the script prevents from choosing incorrect date interval
					from.on( \'change\', function() {
					to.datepicker( \'option\', \'minDate\', from.val() );
				});
	 
				to.on( \'change\', function() {
					from.datepicker( \'option\', \'maxDate\', to.val() );
				});
	 
			});
			</script>';
	}

	// The main function that actually filters the posts
	function filterquery($admin_query)
	{
		global $pagenow;

		if (
			is_admin()
			&& $admin_query->is_main_query()
			// by default filter will be added to all post types, you can operate with $_GET['post_type'] to restrict it for some types
			&& in_array($pagenow, ['edit.php', 'upload.php'])
			&& (!empty($_GET['DateFrom']) || !empty($_GET['DateTo']))
		) {

			$admin_query->set('date_query', [
				'after' 	=> sanitize_text_field($_GET['DateFrom']), // any strtotime()-acceptable format!
				'before' 	=> sanitize_text_field($_GET['DateTo']),
				'inclusive' => true, // include the selected days as well
				'column'    => 'post_date' // 'post_modified', 'post_date_gmt', 'post_modified_gmt'
			]);
		}

		return $admin_query;
	}
}

new DateRange();

/**
 * Add User ID Class to Body
 */
add_action('init', 'admin_classes');
function admin_classes()
{
	if (is_user_logged_in()) {
		add_filter('admin_body_class', 'class_to_body_admin');
	}
}


// add 'class-name' to the $classes array
function class_to_body_admin($classes)
{
	global $current_user;

	if (!in_array('administrator', $current_user->roles) && $current_user->user_login !== SUPERADMIN) {
		return false;
	}

	$classes = SUPERADMIN;

	return $classes;
}



// Admin Bar Custom CSS
add_action('wp_head', 'my_admin_css');
function my_admin_css()
{
	if (is_user_logged_in()) { ?>
		<style type="text/css">
			#wpadminbar {
				width: 45px;
				min-width: auto;
				overflow: hidden;
				transition: 0s width;
				transition-delay: 2s;
			}

			#wpadminbar:hover {
				width: 100%;
				overflow: visible;
				transition-delay: 0s;
			}
		</style>
<?php  }
}


/**
 * Unique Column for Featured Image
 * 
 * @param string[] $columns - An associative array of column headings.
 */
add_filter('manage_posts_columns', 'unique_columns_head', 10);

function unique_columns_head($columns)
{
	$columns['img'] = 'Featured Image';
	return $columns;
}


/**
 * Unique Content for Featured Image
 * 
 * Add featured image for posts
 * 
 * @param string $column - The name of the column to display.
 * @param int $post_id - The current post ID.
 */
add_action('manage_posts_custom_column', 'unique_columns_content', 10, 2);

function unique_columns_content($column, $post_id)
{
	if ($column == 'img') {
		return the_post_thumbnail('thumbnail');
	}
}


/**
 * Team Picture
 * 
 * Add featured image for {Team} post type
 * 
 * @param string $column - The name of the column to display.
 * @param int $post_id - The current post ID.
 */
add_action('manage_team_posts_custom_column', 'team_custom_column_values', 10, 2);

function team_custom_column_values($column, $post_id)
{
	switch ($column) {
		case 'img':
			echo custom_image_size(get_field('profile_img'), 'thumbnail', get_the_title());
			break;
	}
}


/**
 * Testimonials Picture
 * 
 * Add featured image for {Testimonials} post type
 * 
 * @param string $column - The name of the column to display.
 * @param int $post_id - The current post ID.
 */
add_action('manage_resources_posts_custom_column', 'resources_custom_column_values', 10, 2);

function resources_custom_column_values($column, $post_id)
{
	switch ($column) {
		case 'img':

			// Vars
			$taxonomy = 'resource-type';
			$resource_img = get_field('resource_img', $post_id) ?: '';
			$placeholder = '';


			// ? Check image is exists
			if (!$resource_img) {
				// ? Demo Videos {5}
				if (has_term(5, $taxonomy)) {
					$placeholder = get_field('resources_video_bg', 'option') ?: '';
					echo custom_image_size($placeholder, 'thumbnail', get_the_title(), '', '', 'none');
				}
				// ? Fact Sheets {6}
				if (has_term(6, $taxonomy)) {
					$placeholder = get_field('resources_factsheet_bg', 'option') ?: '';
					echo custom_image_size($placeholder, 'thumbnail', get_the_title(), '', '', 'none');
				}
				// ? White Papers {7}
				if (has_term(7, $taxonomy)) {
					$placeholder = get_field('resource_whitepaper_bg', 'option') ?: '';
					echo custom_image_size($placeholder, 'thumbnail', get_the_title(), '', '', 'none');
				}
			} else {
				// Get the custom image: $resource_img
				echo custom_image_size($resource_img, 'thumbnail', get_the_title(), '', '', 'none');
			}
			break;
	}
}

/*
// Remove All / Mine / Draft from upload
add_filter('views_edit-upload', 'remove_draft_all_mine_upload');
function remove_draft_all_mine_upload($views) {
	//unset($views['all']);
	unset($views['draft']);
	unset($views['private']);
	unset($views['mine']);
	return $views;
}

// Remove action buttons
add_filter( 'post_row_actions', 'remove_row_actions', 10, 1 );

function remove_row_actions( $actions ){
	// Confirm post type
	if( get_post_type() === 'upload' ){
		unset( $actions['edit'] );
		unset( $actions['view'] );
		unset( $actions['trash'] );
		unset( $actions['inline hide-if-no-js'] );
	}    
	return $actions;
}


// Remove Theme Submenus
add_action('admin_menu', 'remove_upload_submenu', 100);

function remove_upload_submenu() {

	global $submenu;
	unset($submenu['edit.php?post_type=upload'][10]);

}


// Remove bulk edit for uploads
add_filter( 'bulk_actions-edit-upload', 'remove_upload_bulk_edit' );
function remove_upload_bulk_edit( $actions ){
	unset( $actions[ 'edit' ] );
	return $actions;
}


// Unique column for uploads
add_filter('manage_upload_posts_columns', 'custom_upload_columns', 10);

function custom_upload_columns($columns) {
	$columns = [
		'cb' 		  => $columns['cb'],
		'file_title'  => esc_html__('File'),
		'author' 	  => esc_html__('Author'),
		'upload_date' => esc_html__('Upload Date'),
		'download' 	  => esc_html__('Download'),
	];
	return $columns;
}

// Unique column values for uploads
add_action( 'manage_upload_posts_custom_column', 'custom_upload_column_values', 10, 2);
function custom_upload_column_values( $column, $post_id ) {

	// Vars
	$attach_id 	  = get_field( 'upload_file', $post_id );
	$attach_title = get_the_title($attach_id);

	// Upload date column
	if ( 'file_title' === $column ) {
		echo $attach_title;
	}
	if ( 'upload_date' === $column ) {
		echo get_the_date( 'H:i - m/d/Y', $post_id );
	}
	if ( 'download' === $column ) {
		echo '<a href="' . wp_get_attachment_url($attach_id) . '"  title="' . $attach_title . '" download>Download</a>';
	}
}*/


// EOF