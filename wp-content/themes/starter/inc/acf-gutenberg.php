<?php

/**
 * ACF | Gutenberg | TinyMCE
 * 
 * This file contains the following configurations:
 * 
 * * Show Only Parent Pages
 * * TinyMCE Color Settings
 * * ACF WYSIWYG Settings
 * * ACF Block Category
 * * ACF Allowed Block Types
 * * ACF Core Settings
 */


/**
 * Maintenance
 * 
 * This file contains the following configurations:
 * 
 * * Maintenance Message
 */


/**
 * ACF Post Object - Show only Parent Pages 
 */
add_filter('acf/fields/post_object/query/key=field_61320b4d9f14f', 'only_parents_show', 10, 1);
function only_parents_show($args)
{

	$excludes = [2, 242, 369, 1706];
	/*$german_excludes = lang_object_ids($excludes, 'page', 'de');

	foreach ($german_excludes as $german_exclude) {
		$excludes[] = $german_exclude;
	}*/

	$parent_not_in = [259, 363, 365];
	/*$german_posts = lang_object_ids($parent_not_in, 'page', 'de');

	foreach ($german_posts as $german_post) {
		$parent_not_in[] = $german_post;
	}*/

	if (ICL_LANGUAGE_CODE === 'de') {
		$excludes = lang_object_ids($excludes, 'page', 'de');
		$parent_not_in = lang_object_ids($parent_not_in, 'page', 'de');
	}

	$page_args = [
		'suppress_filters' => false,
		'post_type'    	   => 'page',
		'numberposts'  	   => -1,
		'post__not_in' 	   => $excludes
	];

	$parents = get_posts($page_args);
	$parent_ids = [];

	foreach ($parents as $parent) {
		if (!empty(get_children($parent->ID)) && !in_array($parent->post_parent, $parent_not_in)) {
			$parent_ids[] = $parent->ID;
		}
	}

	$args['post__in'] 	 = $parent_ids;
	$args['sort_order']  = 'ASC';
	$args['orderby'] 	 = 'title';
	$args['order'] 		 = 'ASC';
	$args['post_status'] = 'publish';

	return $args;
}

/**
 * TinyMCE — Remove Custom Color Row
 */
add_filter('tiny_mce_plugins', 'remove_tinymce_custom_colors');
function remove_tinymce_custom_colors($plugins)
{
	foreach ($plugins as $key => $plugin_name) {
		if ('colorpicker' === $plugin_name) {
			unset($plugins[$key]);
			return $plugins;
		}
	}
	return $plugins;
}


/**
 * TinyMCE > WYSIWYG — Add Custom Colors
 */
add_filter('tiny_mce_before_init', 'custom_mce_color_options');

function custom_mce_color_options($init)
{

	// Custom colors
	$custom_colours = '
			"005479", "LBlue",
			"00CDCC", "Green",
			"000E79", "Blue",
		';

	// Custom Font Sizes
	$custom_fontsizes = 'small=16px large=25px huge=30px';

	// $init['fontsize_formats'] = $custom_fontsizes;
	$init['textcolor_map'] = '[' . $custom_colours . ']';
	$init['toolbar1'] = 'formatselect,styleselect,forecolor,bold,italic,aligncenter,alignright,bullist,numlist,link,blockquote,wp_add_media';
	$init['toolbar2'] = '';
	$init['toolbar3'] = '';

	$init['block_formats'] = "Paragraph=p; Heading 1=h1; Heading 2=h2; Heading 3=h3; Heading 4=h4;";

	$style_formats = [
		[
			'title' 	=> 'Button Style',
			'selector'  => 'a',
			'classes' 	=> 'button red'
		],
		[
			'title' 	=> 'Button File Style',
			'selector'  => 'a',
			'classes' 	=> 'button red file'
		]
	];
	// Insert the array, JSON ENCODED, into 'style_formats'
	$init['style_formats'] = json_encode($style_formats);

	// 8 swatches per row
	$init['textcolor_rows'] = 1;

	return $init;
}

/**
 * ACF > WYSIWYG — Custom Toolbar
 * 
 * @param array $toolbars - Toolbar options
 * @return array $toolbars
 */
add_filter('acf/fields/wysiwyg/toolbars', function ($toolbars) {

	// Unset Basic Type Toolbar
	unset($toolbars['Basic']);

	// [1] formatselect. bold, italic, bullist, numlist, blockquote, alignleft, aligncenter, alignright, link, wp_more, spellchecker, fullscreen, wp_adv
	// [2] strikethrough, hr, forecolor, pastetext, removeformat, charmap, outdent, indent, undo, redo, wp_help

	// Register a basic toolbar with a single row of options
	$toolbars['Full'][1] = [
		'formatselect',
		'styleselect',
		//'fontsizeselect',
		'forecolor',
		'bold',
		'italic',
		'aligncenter',
		'alignright',
		'bullist',
		'numlist',
		'link',
		'blockquote'
	];
	$toolbars['Full'][2] = [];

	return $toolbars;
});


/**
 * Gutenberg | ACF — Custom Block Category
 * 
 * @param bool|array $block_categories - Array of block type slugs, or boolean to enable/disable all. Default true (all registered block types supported).
 * @param WP_Block_Editor_Context $block_categories_all - The current block editor context.
 * @return bool|array $block_categories
 */
add_filter('block_categories_all', 'custom_block_categories', 10, 2);
function custom_block_categories($block_categories, $block_editor_context)
{
	global $post_type;
	if ($post_type !== 'page') {
		return $block_categories;
	}
	return array_merge(
		$block_categories,
		[
			[
				'slug'  => sanitize_title('bs'),
				'title' => 'Bucher + Suter Blocks',
				'icon'  => 'block-default',
			],
		]
	);
}


/**
 * Gutenberg | ACF — Allowed Block Types
 * 
 * @param array $allowed_block_types - Array of categories for block types.
 * @param WP_Block_Editor_Context $block_categories_all - The current block editor context.
 * @return array $allowed_blocks
 */
add_filter('allowed_block_types_all', 'allowed_block_types', 10, 2);
function allowed_block_types($allowed_block_types, $block_editor_context)
{

	$theme = wp_get_theme();
	$files = $theme->get_files('php', 3, false);

	// Iterate over and ignore non-block templates
	foreach ($files as $filename => $filepath) {

		if (preg_match('#^parts/blocks/(components|core)s?/#', $filename, $matches) !== 1) {
			continue;
		}

		// Read the PHP comment meta data for the block
		$meta = get_file_data($filepath, [
			'name' => 'Block Name',
		]);

		// Skip template if a name is not provided
		if (empty($meta['name'])) {
			continue;
		}

		// Read acf block types automatically
		$acf_blocks[] = 'acf/' . $meta['name'];
	}


	global $post_type;

	$allowed_blocks = [
		'core/paragraph',
		'core/freeform'
	];

	//if ($post_type === 'post') {
	//return $block_categories;
	// Allowed Block Types
	//$allowed_blocks = [
	//'core/block', // For reusable block
	//'core/freeform', // For Post
	//'core/paragraph', // For section block
	//'core/separator',
	//];
	//}


	$allowed_blocks = array_merge($allowed_blocks, $acf_blocks);

	return $allowed_blocks;
}


/**
 * ACF — Init Settings
 */
add_action('acf/init', 'custom_acf_init');

function custom_acf_init()
{

	// ACF — Register ACF Blocks 
	if (function_exists('acf_register_block_type')) {

		$theme 		 = wp_get_theme();
		$theme_color = '#fff';
		$files 		 = $theme->get_files('php', 3, false);

		// Iterate over and ignore non-block templates
		foreach ($files as $filename => $filepath) {

			if (preg_match('#^parts/blocks/(components|core)s?/#', $filename, $matches) !== 1) {
				continue;
			}

			// Read the PHP comment meta data for the block
			$meta = get_file_data($filepath, [
				'name'        	=> 'Block Name',
				'title'       	=> 'Block Title',
				'description' 	=> 'Block Description',
				'post_types'  	=> 'Post Types',
				'mode_main'   	=> 'Block Mode Main',
				'mode'        	=> 'Block Mode',
				'align'       	=> 'Block Align',
				'align_text'  	=> 'Block Text',
				'align_content' => 'Block Content',
				'keywords'    	=> 'Block Keywords',
				'icon'    	  	=> 'Block Icon',
				'multiple'    	=> 'Block Multiple',
				'jsx'    	  	=> 'Block JSX',
				'script'    	=> 'Block Script',
				'anchor'	  	=> 'Block Anchor'
			]);

			// Skip template if a name is not provided
			if (empty($meta['name'])) {
				continue;
			}

			// Convert the post types to an [] (or use defaults)
			$post_types = array_filter(
				array_map('trim', explode(',', $meta['post_types']))
			);
			if (empty($post_types)) {
				$post_types = ['page', 'post'];
			}

			// Convert the keywords to an []
			$keywords = array_filter(
				array_map('trim', explode(',', $meta['post_types']))
			);

			// Register the ACF block using the meta data
			acf_register_block_type([
				'name'              	=> $meta['name'],
				'title'             	=> $meta['title'],
				'description'       	=> $meta['description'],
				'category'          	=> 'bs',
				'post_types'        	=> $post_types,
				'render_template'   	=> $filepath,
				'enqueue_script'		=> boolval($meta['script']) && !is_admin() ? THEMEPATH . '/assets/js/blocks/' . $meta['name'] . '.min.js' : '',
				'keywords'          	=> $keywords,
				'icon'              	=> [
					'background'		=> '#005479',
					'foreground' 		=> $theme_color,
					'src'		 		=> $meta['icon'],
				],
				'supports'          	=> [
					'anchor' 			=> $meta['anchor'] ? false : true,
					'customClassName' 	=> true,
					'align'				=> boolval($meta['align']) ? [$meta['align']] : false,
					'align_text'		=> boolval($meta['align_text']),
					'align_content'		=> boolval($meta['align_content']),
					'mode'            	=> false,
					'multiple'			=> boolval($meta['multiple']),
					'jsx'				=> boolval($meta['jsx'])
				],
				'mode'					=> $meta['mode_main'] ? $meta['mode_main'] : 'edit',
			]);
		}
	}

	// ACF — Options Page
	if (function_exists('acf_add_options_page')) {
		if (current_user_can('administrator')) {

			// Add parent page.
			$parent = acf_add_options_page([
				'page_title' 	=> __('Website Settings'),
				'menu_title' 	=> __('Website Settings'),
				'menu_slug' 	=> 'website-settings',
				'capability'	=> 'edit_posts',
				'autoload' 		=> true,
				'redirect' 		=> false
			]);

			// Add sub page.
			/*$child = acf_add_options_page(array(
				'page_title'  => __('Trusted By Logos'),
				'menu_title'  => __('Trusted By Logos'),
				'menu_slug'   => 'trusted-by-logos',
				'parent_slug' => $parent['menu_slug'],
			));*/
		}
	}
}


// EOF