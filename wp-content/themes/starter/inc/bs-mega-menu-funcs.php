<?php

function get_top_menu() {
  ob_start();

  wp_nav_menu([
    'theme_location'    => 'topmenu',
    'container'         => 'div',
    'container_class'   => 'top-menu-container',
    'menu'              => '311',
    'menu_class'        => 'top-menu',
  ]);

  $top_bar_menu_html = ob_get_contents();

  ob_end_clean();

  // replace id attribute (as this menu will be rendered twice)
  $top_bar_menu_html = str_replace(' id="', ' data-id="', $top_bar_menu_html);

  return $top_bar_menu_html;
}


// TOP MENU ICONS
add_filter('nav_menu_item_title', function ($title, $menu_item, $args, $depth) {
  if ($args->menu_class === 'top-menu') {
    $top_menu_icon = get_field('top_menu_icon', $menu_item);
    
    if ($top_menu_icon) {
      return "<span data-icon='$top_menu_icon'></span> $title";
    }

    // pr1($title);
    // return $title;

    if (get_bloginfo('language') === 'en-US' && strpos($title, 'lang="en"') !== false || 
        get_bloginfo('language') === 'de-DE' && strpos($title, 'lang="de"') !== false) {

      return "<span data-icon=''></span> $title <span data-icon=''></span>";

    };
  }

  return $title;
}, 10, 4);


function get_menu_side_links() {
  $output = '';

  $menu_side_links = wp_get_nav_menu_items(312);
  foreach ($menu_side_links as $menu_side_link) {
    $title = $menu_side_link->post_title;
    $icon = get_field('mega_sidebar_menu_icon', $menu_side_link);
    $icon_weight = get_field('icon_weight', $menu_side_link);

    $output .= "<a class='menu-side-link' href='" . $menu_side_link->url . "'>";
    $output .= "<span class='$icon_weight' data-icon='$icon'></span>";
    $output .= "<span>$title</span>";
    $output .= "</a>";
  }

  return $output;
}


class BS_Menu_Walker extends Walker_Nav_Menu {

  /**
   * Unique id for dropdowns
   */
  public $submenu_unique_id = '';

  /**
   * Starts the list before the elements are added.
   * @see Walker::start_lvl()
   */
  public function start_lvl( &$output, $depth = 0, $args = null ) {
    if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
      $t = '';
      $n = '';
    } else {
      $t = "\t";
      $n = "\n";
    }
    $indent = str_repeat( $t, $depth );

    // Default class.
    $classes = array( ($depth === 0) ? 'white-bc-menu' : 'sub-menu' );
  
    /**
     * Filters the CSS class(es) applied to a menu list element.
     *
     * @since 4.8.0
     *
     * @param string[] $classes Array of the CSS classes that are applied to the menu `<ul>` element.
     * @param stdClass $args    An object of `wp_nav_menu()` arguments.
     * @param int      $depth   Depth of menu item. Used for padding.
     */
    $class_names = implode( ' ', apply_filters( 'nav_menu_submenu_css_class', $classes, $args, $depth ) );
    $class_names = "$class_names depth-$depth";
    $class_names = $class_names ? ' class="' . esc_attr( $class_names ) . '"' : '';
  
    $output .= "{$n}{$indent}<div$class_names>{$n}";

    if ($depth === 0) {
      $output .= '<div class="white-bc-content"><div class="sub-menu-content"><span class="mobile-back" data-icon=""></span>';
    }
  }

  /**
   * Ends the list of after the elements are added.
   * @see Walker::end_lvl()
   */
  public function end_lvl( &$output, $depth = 0, $args = null ) {
    if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
      $t = '';
      $n = '';
    } else {
      $t = "\t";
      $n = "\n";
    }
    $indent  = str_repeat( $t, $depth );

    if ($depth === 0) {
      $output .= '</div>'; // /sub-menu-content

      $output .= '<div class="sub-menu-sidebar">';
      
      $output .= get_menu_side_links();

      $output .= '</div>'; // /sub-menu-sidebar

      $output .= '</div>'; // /white-bc-content
    }

    $output .= "$indent</div>{$n}";
  }

  /**
   * @see Walker::start_el()
   */
  public function start_el( &$output, $data_object, $depth = 0, $args = null, $current_object_id = 0 ) {
    $html_tag = ($depth === 0) ? 'li' : 'div';

    // CUSTOM FIELDS
    $mega_menu_item_type = get_field('mega_menu_item_type', $data_object);
    // $mega_menu_item_sub_text = get_field('text', $data_object);
    // $mega_menu_item_background = get_field('background', $data_object);
    $mega_menu_item_hidden = get_field('hidden', $data_object);
    $mega_menu_item_icon = get_field('menu_icon', $data_object);
    $mega_menu_item_icon_style = get_field('menu_icon_style', $data_object);
    $mega_menu_item_icon_type = get_field('menu_icon_type', $data_object);
    $mega_menu_item_icon_image = get_field('icon_image', $data_object);
    $mega_menu_item_icon_weight = get_field('menu_icon_weight', $data_object);

    // Restores the more descriptive, specific name for use within this method.
    $menu_item = $data_object;

    if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
      $t = '';
      $n = '';
    } else {
      $t = "\t";
      $n = "\n";
    }
    $indent = ( $depth ) ? str_repeat( $t, $depth ) : '';

    $classes   = empty( $menu_item->classes ) ? array() : (array) $menu_item->classes;
    $classes[] = 'menu-item-' . $menu_item->ID;

    /**
     * Getting custom fields for mega menu (only for depth > 0)
     */
    if ($depth > 0) {
      if ($mega_menu_item_type === 'title') {
        $classes[] = 'title';

        if ($mega_menu_item_hidden) {
          $classes[] = 'hidden-link';
        }
      }

      // $classes[] = $mega_menu_item_background;
    }

    /**
     * Filters the arguments for a single nav menu item.
     *
     * @since 4.4.0
     *
     * @param stdClass $args      An object of wp_nav_menu() arguments.
     * @param WP_Post  $menu_item Menu item data object.
     * @param int      $depth     Depth of menu item. Used for padding.
     */
    $args = apply_filters( 'nav_menu_item_args', $args, $menu_item, $depth );

    /**
     * Filters the CSS classes applied to a menu item's list item element.
     *
     * @since 3.0.0
     * @since 4.1.0 The `$depth` parameter was added.
     *
     * @param string[] $classes   Array of the CSS classes that are applied to the menu item's `<li>` element.
     * @param WP_Post  $menu_item The current menu item object.
     * @param stdClass $args      An object of wp_nav_menu() arguments.
     * @param int      $depth     Depth of menu item. Used for padding.
     */
    $class_names = implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $menu_item, $args, $depth ) );
    $class_names = $class_names ? "$class_names depth-$depth" : "depth-$depth";
    $class_names = ' class="' . esc_attr( $class_names ) . '"';

    /**
     * Filters the ID attribute applied to a menu item's list item element.
     *
     * @since 3.0.1
     * @since 4.1.0 The `$depth` parameter was added.
     *
     * @param string   $menu_item_id The ID attribute applied to the menu item's `<li>` element.
     * @param WP_Post  $menu_item    The current menu item.
     * @param stdClass $args         An object of wp_nav_menu() arguments.
     * @param int      $depth        Depth of menu item. Used for padding.
     */
    $id = apply_filters( 'nav_menu_item_id', 'menu-item-' . $menu_item->ID, $menu_item, $args, $depth );
    $id = $id ? ' id="' . esc_attr( $id ) . '"' : '';

    $output .= $indent . "<{$html_tag}" . $id . $class_names . '>';

    $atts           = array();
    $atts['title']  = ! empty( $menu_item->attr_title ) ? $menu_item->attr_title : '';
    $atts['target'] = ! empty( $menu_item->target ) ? $menu_item->target : '';
    if ( '_blank' === $menu_item->target && empty( $menu_item->xfn ) ) {
      $atts['rel'] = 'noopener';
    } else {
      $atts['rel'] = $menu_item->xfn;
    }
    $atts['href']         = ! empty( $menu_item->url ) ? $menu_item->url : '';
    $atts['aria-current'] = $menu_item->current ? 'page' : '';

    /**
     * Filters the HTML attributes applied to a menu item's anchor element.
     *
     * @since 3.6.0
     * @since 4.1.0 The `$depth` parameter was added.
     *
     * @param array $atts {
     *     The HTML attributes applied to the menu item's `<a>` element, empty strings are ignored.
     *
     *     @type string $title        Title attribute.
     *     @type string $target       Target attribute.
     *     @type string $rel          The rel attribute.
     *     @type string $href         The href attribute.
     *     @type string $aria-current The aria-current attribute.
     * }
     * @param WP_Post  $menu_item The current menu item object.
     * @param stdClass $args      An object of wp_nav_menu() arguments.
     * @param int      $depth     Depth of menu item. Used for padding.
     */
    $atts = apply_filters( 'nav_menu_link_attributes', $atts, $menu_item, $args, $depth );

    $attributes = '';
    foreach ( $atts as $attr => $value ) {
      if ( is_scalar( $value ) && '' !== $value && false !== $value ) {
        $value       = ( 'href' === $attr ) ? esc_url( $value ) : esc_attr( $value );
        $attributes .= ' ' . $attr . '="' . $value . '"';
      }
    }

    /** This filter is documented in wp-includes/post-template.php */
    $title = apply_filters( 'the_title', $menu_item->title, $menu_item->ID );

    /**
     * Filters a menu item's title.
     *
     * @since 4.4.0
     *
     * @param string   $title     The menu item's title.
     * @param WP_Post  $menu_item The current menu item object.
     * @param stdClass $args      An object of wp_nav_menu() arguments.
     * @param int      $depth     Depth of menu item. Used for padding.
     */
    $title = apply_filters( 'nav_menu_item_title', $title, $menu_item, $args, $depth );

    $item_output  = $args->before;
    $item_output .= '<a' . $attributes . " class='depth-$depth'>";

    if ($mega_menu_item_type !== 'title' && $depth > 1) {
      $item_output .= "<span class='menu-icon $mega_menu_item_icon_style $mega_menu_item_icon_type $mega_menu_item_icon_weight' data-icon='$mega_menu_item_icon'>";

      if ($mega_menu_item_icon_type === 'image') {
        $item_output .= "<img src='" . $mega_menu_item_icon_image['url'] . "' alt='$title'/>";
      }

      $item_output .= "</span>";
      $item_output .= "<span class='label'>";
      $item_output .= "$title";
      // $item_output .= ($mega_menu_item_sub_text) ? "<span>$mega_menu_item_sub_text</span>" : "";
      $item_output .= "</span>";
    } elseif ($depth === 0) {
      $item_output .= $args->link_before . $title . ' <span data-icon=""></span>' . $args->link_after;
    } else {
      $item_output .= $args->link_before . $title . $args->link_after;
    }

    $item_output .= '</a>';
    $item_output .= $args->after;

    /**
     * Filters a menu item's starting output.
     *
     * The menu item's starting output only includes `$args->before`, the opening `<a>`,
     * the menu item's title, the closing `</a>`, and `$args->after`. Currently, there is
     * no filter for modifying the opening and closing `<li>` for a menu item.
     *
     * @since 3.0.0
     *
     * @param string   $item_output The menu item's starting HTML output.
     * @param WP_Post  $menu_item   Menu item data object.
     * @param int      $depth       Depth of menu item. Used for padding.
     * @param stdClass $args        An object of wp_nav_menu() arguments.
     */
    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $menu_item, $depth, $args );
  }

  /**
   * Ends the element output, if needed.
   *
   */
  public function end_el( &$output, $data_object, $depth = 0, $args = null ) {
    $html_tag = $depth === 0 ? 'li' : 'div';

    if ( isset( $args->item_spacing ) && 'discard' === $args->item_spacing ) {
      $t = '';
      $n = '';
    } else {
      $t = "\t";
      $n = "\n";
    }
    $output .= "</{$html_tag}>{$n}";
  }

} //
