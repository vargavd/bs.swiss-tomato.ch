<?php

/**
 * Custom Post Types — Customization
 * 
 * This file contains the following configurations:
 * 
 * * Costum Post Types Settings
 */

// Register Custom Post Type
add_action('init', 'custom_post_types');

function custom_post_types()
{

	// Remove excerpt from Post
	remove_post_type_support('post', 'excerpt');

	$taxonomies = ['post_tag'];
	foreach ($taxonomies as $tax) {
		// Register taxonomies: attachment
		// register_taxonomy_for_object_type( $tax, 'attachment' );
		// Unregister taxonomies: tags
		unregister_taxonomy_for_object_type($tax, 'post');
	}


	// Post Template Settings
	$post_type_object_post = get_post_type_object('post');
	$post_type_object_post->template = [
		['core/freeform'],
		['acf/video-list'],
		['core/freeform']
	];
	//$post_type_object_post->template_lock = 'insert';


	// Jobs
	$slug_job = 'jobs';

	register_post_type('jobs', [
		'labels' => [
			'name' 				 => esc_html__('Jobs', 'bs'),
			'singular_name' 	 => esc_html__('Job', 'bs'),
			'add_new' 			 => 'New Job',
			'add_new_item' 		 => 'Add Job Job',
			'edit_item' 		 => 'Edit Job',
			'new_item'		 	 => 'New Job',
			'view_item' 		 => 'View Job',
			'view_items' 		 => 'View Job',
			'all_items' 		 => 'All Job',
			'search_items' 		 => 'Search Job',
			'not_found' 		 => 'No Job found!',
			'not_found_in_trash' => 'No Job found in Trash.',
			'parent_item_colon'  => '',
			'menu_name' 		 => 'Jobs'
		],
		'public' 				 => true,
		'publicly_queryable' 	 => true,
		'show_ui' 				 => true,
		'show_in_menu' 			 => true,
		'exclude_from_search' 	 => true,
		'query_var' 			 => true,
		'taxonomies'  			 => [],
		'capability_type' 		 => 'post',
		'rewrite' 				 => ['slug' => $slug_job],
		'can_export' 			 => true,
		'has_archive'			 => false,
		'hierarchical'			 => false,
		'menu_position' 		 => 24,
		'menu_icon' 			 => 'dashicons-portfolio',
		'show_in_rest' 			 => false,
		'supports' 				 => ['title'],
	]);


	// Resources
	$slug_resource = 'resources';

	register_post_type('resources', [
		'labels' => [
			'name' 				 => esc_html__('Resources', 'bs'),
			'singular_name' 	 => esc_html__('Resource', 'bs'),
			'add_new' 			 => 'New Resource',
			'add_new_item' 		 => 'Add New Resource',
			'edit_item' 		 => 'Edit Resource',
			'new_item'		 	 => 'New Resource',
			'view_item' 		 => 'View Resource',
			'view_items' 		 => 'View Resources',
			'all_items' 		 => 'All Resource',
			'search_items' 		 => 'Search Resource',
			'not_found' 		 => 'No Resource found!',
			'not_found_in_trash' => 'No Resource found in Trash.',
			'parent_item_colon'  => '',
			'menu_name' 		 => 'Resources'
		],
		'public' 				 => false,
		'publicly_queryable' 	 => true,
		'show_ui' 				 => true,
		'show_in_menu' 			 => true,
		'exclude_from_search' 	 => true,
		'query_var' 			 => true,
		'taxonomies'  			 => [],
		'capability_type' 		 => 'post',
		'rewrite' 				 => ['slug' => $slug_resource],
		'can_export' 			 => true,
		'has_archive'			 => false,
		'hierarchical'			 => false,
		'menu_position' 		 => 23,
		'menu_icon' 			 => 'dashicons-open-folder',
		'show_in_rest' 			 => false,
		'supports' 				 => ['title']
	]);

	// Products
	$slug_product = 'product';

	$product = [
		'name' 						 => esc_html__('Products', 'bs'),
		'singular_name' 			 => esc_html__('Product', 'bs'),
		'search_items' 				 => 'Search Product',
		'popular_items' 			 => 'Often used Product',
		'all_items' 				 => 'All Product',
		'parent_item' 				 => null,
		'parent_item_colon' 		 => null,
		'edit_item' 				 => 'Edit Product',
		'update_item' 				 => 'Update Product',
		'add_new_item' 				 => 'Add Product',
		'new_item_name' 			 => 'New Product',
		'separate_items_with_commas' => 'Separate with commas or the Enter key.',
		'add_or_remove_items' 		 => 'Add / Remove Product ',
		'choose_from_most_used'		 => 'Most used',
		'menu_name' 				 => 'Products',
	];

	register_taxonomy(
		$slug_product,
		['resources'],
		[
			'hierarchical' 			=> true,
			'labels' 				=> $product,
			'show_ui' 				=> true,
			'show_admin_column' 	=> true,
			'update_count_callback' => '_update_post_term_count',
			'query_var' 			=> true,
			'show_in_rest' 			=> true,
			'rewrite' 				=> ['slug' => $slug_product],
		]
	);

	// Resource Types
	$slug_resource_type = 'resource-type';

	$resource_types = [
		'name' 						 => esc_html__('Resources Types', 'bs'),
		'singular_name' 			 => esc_html__('Resource Type', 'bs'),
		'search_items' 				 => 'Search Resource Types',
		'popular_items' 			 => 'Often used Resource Types',
		'all_items' 				 => 'All Resource Type',
		'parent_item' 				 => null,
		'parent_item_colon' 		 => null,
		'edit_item' 				 => 'Edit Resource Type',
		'update_item' 				 => 'Update Resource Type',
		'add_new_item' 				 => 'Add Resource Type',
		'new_item_name' 			 => 'New Resource Type',
		'separate_items_with_commas' => 'Separate with commas or the Enter key.',
		'add_or_remove_items' 		 => 'Add / Remove Resource Type',
		'choose_from_most_used'		 => 'Most used',
		'menu_name' 				 => 'Resource Types',
	];

	register_taxonomy(
		$slug_resource_type,
		['resources'],
		[
			'hierarchical' 			=> true,
			'labels' 				=> $resource_types,
			'show_ui' 				=> true,
			'show_admin_column' 	=> true,
			'update_count_callback' => '_update_post_term_count',
			'query_var' 			=> true,
			'show_in_rest' 			=> true,
			'rewrite' 				=> ['slug' => $slug_resource_type],
		]
	);
}


// EOF