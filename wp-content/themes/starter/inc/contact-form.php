<?php

/**
 * Contact Form — Customization
 * 
 * This file contains the following configurations:
 * 
 * * Custom Tag - [siteurl]
 */


// Custom Tag - [siteurl]
add_action('wpcf7_init', 'custom_add_form_tag_siteurl');

function custom_add_form_tag_siteurl()
{
	wpcf7_add_form_tag('siteurl', 'custom_siteurl_text'); // "siteurl" is the type of the form-tag
	wpcf7_add_form_tag(array('siteurl', 'siteurl*'), 'custom_siteurl_form_tag_handler', array('name-attr' => true));
}

function custom_siteurl_form_tag_handler($tag)
{

	$tag = new WPCF7_FormTag($tag);

	if (empty($tag->name)) {
		return '';
	}

	$validation_error = wpcf7_get_validation_error($tag->name);

	$class = wpcf7_form_controls_class($tag->type);

	if ($validation_error) {
		$class .= ' wpcf7-not-valid';
	}

	$atts = array();
	$atts['class'] = $tag->get_class_option($class);
	$atts['id'] = $tag->get_id_option();

	if ($tag->is_required()) {

		$atts['aria-required'] = 'true';
	}

	$atts['aria-invalid'] = $validation_error ? 'true' : 'false';
	$atts['name'] = $tag->name;
	$atts = wpcf7_format_atts($atts);

	$output = '';

	global $post;

	if ($post) {

		$post_id = $post->ID;

		$output = get_the_title($post_id) . ' - ' .  get_the_permalink($post_id) ?: '';

		$output = sprintf(
			'<span class="wpcf7-form-control-wrap %1$s"><input type="text" %2$s value="%3$s" readonly />%4$s</span>',
			sanitize_html_class($tag->name),
			$atts,
			$output,
			$validation_error
		);
	}

	return $output;
}


// EOF