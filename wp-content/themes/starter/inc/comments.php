<?php

/**
 * Comments — Customization
 * 
 * This file contains the following configurations:
 * 
 * * Disable Comments
 */


/**
 * Disable Comments
 */
add_action('admin_init', 'disable_comments');
function disable_comments()
{
	$post_types = get_post_types();
	foreach ($post_types as $post_type) {
		if (post_type_supports($post_type, 'comments')) {
			remove_post_type_support($post_type, 'comments');
			remove_post_type_support($post_type, 'trackbacks');
		}
	}
}


// EOF