<?php

/**
 * WPML - Customization
 * 
 * This file contains the following configurations:
 * 
 * * Custom Language Switcher
 */



// WPML is active?
if (function_exists('icl_object_id')) {

	// Define WPML Constants - Remove all of them
	define('ICL_DONT_LOAD_NAVIGATION_CSS', true);
	define('ICL_DONT_LOAD_LANGUAGE_SELECTOR_CSS', true);
	define('ICL_DONT_LOAD_LANGUAGES_JS', true);


	/**
	 * Add language code to body classes
	 *  
	 * @param string $classes
	 * @return string $classes
	 */
	add_filter('body_class', 'add_language_class');
	function add_language_class($classes)
	{
		$classes[] = 'lang__' . ICL_LANGUAGE_CODE;
		return $classes;
	}


	/**
	 * Get language native name | Usage: get_language_name($code)
	 * 
	 * @param string $code - language code
	 * @return string $language_name - language native name
	 */
	function get_language_name($code = '')
	{
		global $sitepress;
		$details = $sitepress->get_language_details($code);
		$language_name = $details['native_name'];
		return $language_name;
	}


	/**
	 * Custom language switcher | Usage: custom_languages_switcher('footer__class')
	 * 
	 * @param string $class - default ''
	 */
	function custom_languages_switcher($class = '')
	{

		$languages = apply_filters('wpml_active_languages', NULL, ['skip_missing' => 1, 'orderby' => 'code']);

		if (!empty($languages)) {
			echo '<ul class="language__switcher ' . $class . '" aria-labelledby="language">';
			if (defined('ICL_LANGUAGE_CODE')) {
				$language = ICL_LANGUAGE_CODE;
			}
			foreach ($languages as $l) {
				//if ($l['code'] === $language) continue;
				$active = true === (bool) $l['active'] ? 'active' : '';
				echo '<li class="language__item ' . $active . '">';
				if (!$l['active']) echo '<a href="' . $l['url'] . '" data-title="' . apply_filters('wpml_display_language_names', NULL, $l['translated_name']) . '">';
				echo  strtoupper(apply_filters('wpml_display_language_names', NULL, $l['code']));
				if (!$l['active']) echo '</a>';
				echo '</li>';
			}
			echo '</ul>';
		}
	}

	/**
	 * Translating [] of IDs | Usage: lang_object_ids([1,3,6], 'category')
	 * 
	 * @param array $object_id - Object ID
	 * @param string $type - (required) Can be post, page, {custom post type key}, nav_menu, nav_menu_item, category, post_tag, {custom taxonomy key}
	 * @return array - Returns an element’s ID in the current language or in another specified language.
	 */
	function lang_object_ids($object_id, $type, $lang_code = ICL_LANGUAGE_CODE)
	{
		if (is_array($object_id)) {
			$translated_object_ids = [];
			foreach ($object_id as $id) {
				$translated_object_ids[] = apply_filters('wpml_object_id', $id, $type, true, $lang_code);
			}
			return $translated_object_ids;
		} else {
			return apply_filters('wpml_object_id', $object_id, $type, true, $lang_code);
		}
	}
}


// EOF