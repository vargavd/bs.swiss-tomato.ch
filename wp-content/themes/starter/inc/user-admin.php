<?php

/**
 * User Restrictions
 * 
 * This file contains the following configurations:
 * 
 * * Custom Redirect
 * * Add Role Caps
 * * Hide Super Admin
 * * Remove Admin Menus
 * * Remove Admin Bars
 * * Disable Theme Notifications
 */


// Redirect to Posts
add_filter('login_redirect', 'custom_redirect', 10, 3);
function custom_redirect($url, $request, $user)
{
	// Is there a user to check?
	if (isset($user->roles) && is_array($user->roles)) {
		// Check for Super Admin
		if (in_array('administrator', $user->roles) && $user->user_login === SUPERADMIN) {
			return $url;
		} else {
			// If user != Super Admin go to another url
			return get_admin_url() . 'edit.php?post_type=page';
		}
	} else {
		return $url;
	}
}


// Add Themes Capabilty to Editor
add_action('admin_init', 'add_role_caps');

function add_role_caps()
{
	$role = get_role('editor');
	$role->add_cap('edit_theme_options');
}


// Get the current user role
$current_user = wp_get_current_user()->user_login ? wp_get_current_user()->user_login : '';

// If current user Editor
if ($current_user !== '' && $current_user !== SUPERADMIN && $current_user !== 'danielvarga') {


	// // Hide Super Administrator account
	// add_action('pre_user_query', 'site_pre_user_query');
	// function site_pre_user_query($user_search)
	// {
	// 	global $wpdb;
	// 	$user_search->query_where = str_replace(
	// 		'WHERE 1=1',
	// 		"WHERE 1=1 AND {$wpdb->users}.user_login != '" . SUPERADMIN . "'",
	// 		$user_search->query_where
	// 	);
	// }


	// Filter Admin Account
	// add_filter("views_users", "site_list_table_views");
	// function site_list_table_views($views)
	// {
	// 	$users 					= count_users();
	// 	$admins_num 			= $users['avail_roles']['administrator'] - 1;
	// 	$all_num 				= $users['total_users'] - 1;
	// 	$class_adm 				= (strpos($views['administrator'], 'current') === false) ? '' : 'current';
	// 	$class_all 				= (strpos($views['all'], 'current') === false) ? '' : 'current';
	// 	$views['administrator'] = '<a href="users.php?role=administrator" class="' . $class_adm . '">' . translate_user_role('Administrator') . ' <span class="count">(' . $admins_num . ')</span></a>';
	// 	$views['all'] 			= '<a href="users.php" class="' . $class_all . '">' . esc_html__('All') . ' <span class="count">(' . $all_num . ')</span></a>';
	// 	return $views;
	// }


	// Remove Theme Submenus
	add_action('admin_menu', 'remove_theme_submenu', 999);

	function remove_theme_submenu()
	{

		global $submenu;

		foreach ($submenu['themes.php'] as $menu_index => $theme_menu) {

			if (
				$theme_menu[0] == esc_html__('Header')
				|| $theme_menu[0] == esc_html__('Background')
				//	|| $theme_menu[0] == esc_html__('Customize')
			)
				unset($submenu['themes.php'][$menu_index]);
		}

		$customizer_url = add_query_arg('return', urlencode(remove_query_arg(wp_removable_query_args(), wp_unslash($_SERVER['REQUEST_URI']))), 'customize.php');

		remove_submenu_page('themes.php', $customizer_url);
		remove_submenu_page('themes.php', 'themes.php');
		remove_submenu_page('themes.php', 'theme-editor.php');

		//remove_menu_page('tm/menu/main.php');
	}


	// Remove Admin Menus
	add_action('admin_menu', 'remove_menus', 999);

	function remove_menus()
	{

		remove_menu_page('index.php');                  			// Dashboard
		//remove_menu_page( 'edit-comments.php' );          			// Comments
		//remove_menu_page( 'edit.php' ); 							// Posts
		//remove_menu_page( 'edit.php?post_type=page' );   			// Pages
		//remove_menu_page( 'themes.php' );                 		// Appearance
		//remove_menu_page( 'profile.php' );                		// Profile
		//remove_menu_page( 'upload.php' );                 		// Media
    if ($current_user !== 'daniel') {
		  remove_menu_page('plugins.php');                			// Plugins
    }
		//remove_menu_page( 'users.php' );                  		// Users
		remove_menu_page('options-general.php');        			// Settings
		remove_menu_page('tools.php');                  			// Tools
		remove_menu_page('rank-math');                  			// Rank Math
		remove_menu_page('edit.php?post_type=acf-field-group');   //ACF
		remove_menu_page('duplicator-pro');    					// Duplicator Pro
		remove_menu_page('postman');								// Postman
		remove_menu_page('maintance');							// Maintance
	}


	// Custom Admin Bar
	add_action('wp_before_admin_bar_render', 'my_admin_bar_render', 999);

	function my_admin_bar_render()
	{
		global $wp_admin_bar;

		// remove update notices
		remove_action('admin_notices', 'update_nag', 3);

		// remove items from admin bar
		$wp_admin_bar->remove_menu('archive');
		$wp_admin_bar->remove_menu('wp-logo');
		//$wp_admin_bar->remove_menu('site-name');
		$wp_admin_bar->remove_menu('edit-profile');
		$wp_admin_bar->remove_menu('updates');
		$wp_admin_bar->remove_menu('wpseo-menu');
		$wp_admin_bar->remove_menu('comments');
		$wp_admin_bar->remove_menu('new-content');
		$wp_admin_bar->remove_node('updraft_admin_node');
		$wp_admin_bar->remove_menu('wpfc-toolbar-parent');
	}
}

// Remove update notifications
add_filter('auto_core_update_send_email', 'wpb_stop_auto_update_emails', 10, 4);

function wpb_stop_update_emails($send, $type, $core_update, $result)
{
	if (!empty($type) && $type == 'success') {
		return false;
	}
	return true;
}

add_filter('auto_plugin_update_send_email', '__return_false');
add_filter('auto_theme_update_send_email', '__return_false');


// EOF