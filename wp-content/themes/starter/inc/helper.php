<?php

/** 	
 * Helper functions
 * 
 * This file contains the following configurations:
 * 
 * * Print / Var dump + by IP
 * * Custom Image Size
 * * Display ACF Block data + Recursive
 * * Is Block Exists 
 * * Custom Archive's Title
 * * Custom Taxonomy Term's Link
 * * Show Time Differences
 * * Show Tags
 * * Custom Excerpt
 * * Attachment's Meta Data
 * * BR to SPAN
 * * Random String
 * * Clean Phone Number
 * * Remove Empty P
 * * Truncate Advanced
 */


// Custom print_r()
function pr1($var)
{
	echo "\n<pre style=\"background: #FFFF99; font-size: 14px; color: black;\">\n";
	print_r($var);
	echo "\n</pre>\n";
}


// IP restricted print_r()
function ip_pr1($ip, $var)
{
	// $ip = https://www.google.com/search?q=whats+my+ip
	if ($_SERVER['REMOTE_ADDR'] === $ip) {
		return pr1($var);
	}
}


function pr2($var1, $var2) {
  echo "\n<pre style=\"background: #FFFF99; font-size: 14px; color: black;\">\n";
  print_r($var1);
  echo "\n";
  print_r($var2);
  echo "\n</pre>\n";
}
function pr3($var1, $var2, $var3) {
  echo "\n<pre style=\"background: #FFFF99; font-size: 14px; color: black;\">\n";
  print_r($var1);
  echo "\n";
  print_r($var2);
  echo "\n";
  print_r($var3);
  echo "\n</pre>\n";
}
function pr4($var1, $var2, $var3, $var4) {
  echo "\n<pre style=\"background: #FFFF99; font-size: 14px; color: black;\">\n";
  print_r($var1);
  echo "\n";
  print_r($var2);
  echo "\n";
  print_r($var3);
  echo "\n";
  print_r($var4);
  echo "\n</pre>\n";
}


// Custom var_dump()
function vd1($var)
{
	echo "\n<pre style=\"background: #FFFF99; font-size: 14px; color: black;\">\n";
	var_dump($var);
	echo "\n</pre>\n";
}


// IP restricted var_dump()
function ip_vd1($ip, $var)
{
	// $ip = https://www.google.com/search?q=whats+my+ip
	if ($_SERVER['REMOTE_ADDR'] === $ip) {
		return vd1($var);
	}
}


// Custom Image Size
function custom_image_size($img, $size, $title, $class = '', $lazy = 'lazy', $srcset = '')
{

	// Image is not empty?
	if (!$img) {
		return false;
	}

	// Convert to ID from url
	$img_id = attachment_url_to_postid($img);

	// Already in attachment ID format?
	if (is_numeric($img)) {
		$img_id = $img;
	}

	// Proper size url
	$img_url = wp_get_attachment_image_src($img_id, $size)[0];

	// Has desired size?
	if (!$img_url) {
		$size	 = 'full';
		$img_url = wp_get_attachment_image_src($img_id, $size)[0];
	}

	// Width
	$img_width 	= wp_get_attachment_image_src($img_id, $size)[1];

	// Height
	$img_height = wp_get_attachment_image_src($img_id, $size)[2];

	// Lazy
	$lazy = $lazy !== 'none' ? 'loading="' . esc_attr($lazy !== '' ? $lazy : 'lazy') . '"' : '';

	// Srcset
	$srcset = $srcset !== 'none' ? 'srcset="' . esc_attr(wp_get_attachment_image_srcset($img_id)) . '"' : '';

	// Class
	$class = $class ? 'class="' . esc_attr($class) . '"' : '';


	return '<img ' . $lazy . ' src="' . esc_url($img_url) . '"' . $srcset . '" width="' . (int) $img_width . '" height="' . (int) $img_height . '" alt="' . sanitize_text_field($title) . '" ' . $class  . '>';
}


// Display Block Data
function display_block_data()
{
	global $post;
	$blocks = parse_blocks($post->post_content);
	foreach ($blocks as $block) {
		if ('acf/your-block' === $block['blockName']) {
			pr1($block);
			break;
		}
	}
}


// Display Recursive Block Data
function display_recursive_block_data($block)
{
	if ($block['blockName'] === 'acf/your-block-name') {
		return $block;
	}
	if (!empty($block['innerBlocks'])) {
		foreach ($block['innerBlocks'] as $innerBlock) {
			if ($innerBlock) {
				return $innerBlock;
			}
		}
	}
	return false;
}


// Is Block Exist?
function is_block_exists($block_name = '')
{
	global $post;

	// If !post = stop
	if (!$post) {
		return false;
	}

	// Parse Post's content
	$blocks = parse_blocks($post->post_content);
	foreach ($blocks as $block) {
		if ($block_name === $block['blockName']) {
			if ($block_name === $block['blockName']) {
				return true;
				break;
			}
		}
		// Check innerBlocks
		if ($block['innerBlocks']) {
			foreach ($block['innerBlocks'] as $innerBlock) {
				$inner_block_name = $innerBlock['innerBlocks'][0]['blockName'];
				if ($block_name === $inner_block_name) {
					return true;
					break;
				}
			}
		}
	}
	return false;
}


// Custom Archive Title
add_filter('get_the_archive_title', function ($title) {
	if (is_category()) {
		$title = single_cat_title('', false);
	} elseif (is_tag()) {
		$title = single_tag_title('', false);
	} elseif (is_author()) {
		$title = '<span class="vcard">' . get_the_author() . '</span>';
	} elseif (is_tax()) { //for custom post types
		$title = sprintf(__('%1$s'), single_term_title('', false));
	} elseif (is_post_type_archive()) {
		$title = post_type_archive_title('', false);
	}
	return $title;
});


// Custom Taxonomy Terms Links
function custom_taxonomies_terms_links($link = 1)
{
	// Get post by post ID.
	if (!$post = get_post()) {
		return '';
	}

	// Get post type by post.
	$post_type = $post->post_type;

	// Get post type taxonomies.
	$taxonomies = get_object_taxonomies($post_type, 'objects');

	$out = [];

	foreach ($taxonomies as $taxonomy_slug => $taxonomy) {

		// Get the terms related to post.
		$terms = get_the_terms($post->ID, $taxonomy_slug);

		if (!empty($terms)) {
			//$out[] = "<h2>" . $taxonomy->label . "</h2>\n<ul>";
			foreach ($terms as $term) {
				if ($link === 1) {
					$out[] = sprintf(
						'<a href="%1$s" class="category">%2$s</a>',
						esc_url(get_term_link($term->slug, $taxonomy_slug)),
						esc_html($term->name)
					);
				} else {
					$out[] = esc_html($term->name);
				}
			}
			//$out[] = "\n</ul>\n";
		}
	}
	return implode(' ', $out);
}


// Date diff
function pluralize($count, $text)
{
	return $count . (($count === 1) ? (" $text") : (" ${text}s"));
}

function ago($datetime)
{
	$interval = date_create('now')->diff($datetime);
	$suffix = ($interval->invert ? ' ago' : '');
	if ($v = $interval->y >= 1) return pluralize($interval->y, 'year') . $suffix;
	if ($v = $interval->m >= 1) return pluralize($interval->m, 'month') . $suffix;
	if ($v = $interval->d >= 1) return pluralize($interval->d, 'day') . $suffix;
	if ($v = $interval->h >= 1) return pluralize($interval->h, 'hour') . $suffix;
	if ($v = $interval->i >= 1) return pluralize($interval->i, 'minute') . $suffix;
	return pluralize($interval->s, 'second') . $suffix;
}


// Custom Tags
if (!function_exists('show_tags')) {
	function show_tags()
	{
		$post_tags = get_the_tags();
		$separator = ' ';
		$output = '';
		if (!empty($post_tags)) { ?>
			<div class="post__tag__list">
				<span class="post__tag__title"><?= esc_html__('Tags:', 'bs') ?></span>
				<?php foreach ($post_tags as $tag) {
					$output .= '<h4><a href="' . get_tag_link($tag->term_id) . '" class="post__tag__name">' . ucfirst($tag->name) . '</a></h4>' . $separator;
				}
				return trim($output, $separator); ?>
			</div>
		<?php }
	}
}

// Custom Tags
if (!function_exists('show_tag_list')) {
	function show_tag_list()
	{
		$post_tags = get_the_tags();
		$separator = ' ';
		$output = '';
		if (!empty($post_tags)) { ?>
			<?php foreach ($post_tags as $tag) {
				$output .= '<span><a href="' . get_tag_link($tag->term_id) . '" class="post__tag__name">' . ucfirst($tag->name) . '</a></span>' . $separator;
			}
			return trim($output, $separator); ?>
<?php }
	}
}


/**
 * Get excerpt
 * 
 * $limit int content limit size e.g. 100
 */
function get_excerpt($limit = 50)
{
	$excerpt = get_the_content();
	$excerpt = preg_replace(" ([.*?])", '', $excerpt);
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, $limit);
	$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	$excerpt = trim(preg_replace('/\s+/', ' ', $excerpt));
	$excerpt = $excerpt . '...';
	return $excerpt;
}


// Get Archive CPT's years in []
function get_cpt_year_archive_array($cpt = 'post')
{

	// Vars
	$years = [];
	$years_args = [
		'type' => 'monthly',
		'format' => 'custom',
		'before' => '',
		'after' => '|',
		'echo' => false,
		'post_type' => $cpt,
		'order' => 'DESC'
	];

	// Get Years
	$years_content = wp_get_archives($years_args);

	if (!empty($years_content)) {
		$years_arr = explode('|', $years_content);
		$years_arr = array_filter($years_arr, function ($item) {
			return trim($item) !== '';
		});

		// Remove empty whitespace item from array
		foreach ($years_arr as $year_item) {
			$year_row = trim($year_item);
			preg_match('/href=["\']?([^"\'>]+)["\']>(.+)<\/a>/', $year_row, $year_vars);

			if (!empty($year_vars)) {
				$years[] = [
					'name' => $year_vars[2],
					'value' => date('m-Y', strtotime($year_vars[2]))
				];
			}
		}
	}
	return $years;
}


// Get Attachment's Meta Data
function get_attachment_data($attachment_id)
{

	$attachment = get_post($attachment_id);
	return [
		'alt' => get_post_meta($attachment->ID, '_wp_attachment_image_alt', true),
		'caption' => $attachment->post_excerpt,
		'description' => $attachment->post_content,
		'href' => get_permalink($attachment->ID),
		'src' => $attachment->guid,
		'title' => $attachment->post_title
	];
}


// br to span
function br2span($str, $class = '')
{
	return '<span class="' . $class . '">' . implode('</span><span class="' . $class . '">', explode("\r\n", $str)) . '</span>';
}


/**
 * Generate a random string, using a cryptographically secure 
 * pseudorandom number generator (random_int)
 * 
 * For PHP 7, random_int is a PHP core function
 * 
 * @param int $length      How many characters do we want?
 * @param string $keyspace A string of all possible characters
 * @return string
 */
function random_str(
	int $length = 64,
	string $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
): string {
	if ($length < 1) {
		throw new \RangeException("Length must be a positive integer");
	}
	$pieces = [];
	$max = mb_strlen($keyspace, '8bit') - 1;
	for ($i = 0; $i < $length; ++$i) {
		$pieces[] = $keyspace[random_int(0, $max)];
	}
	return implode('', $pieces);
}


/**
 * Clean Phone Number
 * 
 * @param string $string - Phone number
 * @return string $string - Cleaned Phone number
 */
function clean_phone($string)
{
	// Removes special chars
	return preg_replace('/[^0-9\+]/', '', $string);
}


/**
 * Remove empty paragraphs created by wpautop()
 * 
 * @param string|mixed $content
 * @return string|mixed $content
 */
function remove_empty_p($content)
{
	$content = force_balance_tags($content);
	$content = preg_replace('#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content);
	$content = preg_replace('~\s?<p>(\s|&nbsp;)+</p>\s?~', '', $content);
	return $content;
}


/** 
 * Truncates text.
 *
 * Cuts a string to the length of $length and replaces the last characters
 * with the ending if the text is longer than length.
 *
 * @param string $text String to truncate.
 * @param integer $length Length of returned string, including ellipsis.
 * @param string $ending Ending to be appended to the trimmed string.
 * @param boolean $exact If false, $text will not be cut mid-word
 * @param boolean $considerHtml If true, HTML tags would be handled correctly
 * @return string Trimmed string.
 */
function truncate($text, $length = 100, $ending = '...', $exact = true, $considerHtml = false)
{
	if ($considerHtml) {
		// if the plain text is shorter than the maximum length, return the whole text
		if (strlen(preg_replace('/<.*?>/', '', $text)) <= $length) {
			return $text;
		}

		// splits all html-tags to scanable lines
		preg_match_all('/(<.+?>)?([^<>]*)/s', $text, $lines, PREG_SET_ORDER);

		$total_length = strlen($ending);
		$open_tags = array();
		$truncate = '';

		foreach ($lines as $line_matchings) {
			// if there is any html-tag in this line, handle it and add it (uncounted) to the output
			if (!empty($line_matchings[1])) {
				// if it’s an “empty element” with or without xhtml-conform closing slash (f.e.)
				if (preg_match('/^<(\s*.+?\/\s*|\s*(img|br|input|hr|area|base|basefont|col|frame|isindex|link|meta|param)(\s.+?)?)>$/is', $line_matchings[1])) {
					// do nothing
					// if tag is a closing tag (f.e.)
				} else if (preg_match('/^<\s*\/([^\s]+?)\s*>$/s', $line_matchings[1], $tag_matchings)) {
					// delete tag from $open_tags list
					$pos = array_search($tag_matchings[1], $open_tags);
					if ($pos !== false) {
						unset($open_tags[$pos]);
					}
					// if tag is an opening tag (f.e. )
				} else if (preg_match('/^<\s*([^\s>!]+).*?>$/s', $line_matchings[1], $tag_matchings)) {
					// add tag to the beginning of $open_tags list
					array_unshift($open_tags, strtolower($tag_matchings[1]));
				}
				// add html-tag to $truncate’d text
				$truncate .= $line_matchings[1];
			}

			// calculate the length of the plain text part of the line; handle entities as one character
			$content_length = strlen(preg_replace('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', ' ', $line_matchings[2]));
			if ($total_length + $content_length > $length) {
				// the number of characters which are left
				$left = $length - $total_length;
				$entities_length = 0;
				// search for html entities
				if (preg_match_all('/&[0-9a-z]{2,8};|&#[0-9]{1,7};|&#x[0-9a-f]{1,6};/i', $line_matchings[2], $entities, PREG_OFFSET_CAPTURE)) {
					// calculate the real length of all entities in the legal range
					foreach ($entities[0] as $entity) {
						if ($entity[1] + 1 - $entities_length <= $left) {
							$left--;
							$entities_length += strlen($entity[0]);
						} else {
							// no more characters left
							break;
						}
					}
				}
				$truncate .= substr($line_matchings[2], 0, $left + $entities_length);
				// maximum lenght is reached, so get off the loop
				break;
			} else {
				$truncate .= $line_matchings[2];
				$total_length += $content_length;
			}

			// if the maximum length is reached, get off the loop
			if ($total_length >= $length) {
				break;
			}
		}
	} else {
		if (strlen($text) <= $length) {
			return $text;
		} else {
			$truncate = substr($text, 0, $length - strlen($ending));
		}
	}

	// if the words shouldn't be cut in the middle...
	if (!$exact) {
		// ...search the last occurance of a space...
		$spacepos = strrpos($truncate, ' ');
		if (isset($spacepos)) {
			// ...and cut the text in this position
			$truncate = substr($truncate, 0, $spacepos);
		}
	}

	// add the defined ending to the text
	$truncate .= $ending;

	if ($considerHtml) {
		// close all unclosed html-tags
		foreach ($open_tags as $tag) {
			$truncate .= '';
		}
	}

	return $truncate;
}


function bs_swisst_enqueue_style_with_filetime($name, $filePath, $deps) {
  wp_enqueue_style($name, get_stylesheet_directory_uri() . $filePath, $deps, filemtime(get_stylesheet_directory() . $filePath));
}
function bs_swisst_register_style_with_filetime($name, $filePath, $deps) {
  wp_register_style($name, get_stylesheet_directory_uri() . $filePath, $deps, filemtime(get_stylesheet_directory() . $filePath));
}
function bs_swisst_enqueue_script_with_filetime($name, $filePath, $deps, $in_footer = true) {
  wp_enqueue_script($name, get_stylesheet_directory_uri() . $filePath, $deps, filemtime(get_stylesheet_directory() . $filePath), $in_footer);
}
function bs_swisst_register_script_with_filetime($name, $filePath, $deps, $in_footer = true) {
  wp_register_script($name, get_stylesheet_directory_uri() . $filePath, $deps, filemtime(get_stylesheet_directory() . $filePath), $in_footer);
}


// EOF