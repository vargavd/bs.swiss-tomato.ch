<?php

/**
 * Shortcodes
 * 
 * This file contains the following configurations:
 * 
 * * Read More Shortcode - [more]
 * * Sitemap Shortcode - [sitemap]
 * * Auth Shortcodes - [login] [register] [lost_pw] [reset_pw]
 */


// More — Read More 
add_shortcode('more', 'read_more');
function read_more($atts)
{
	/*
	// example of multiple attributes
	$a = shortcode_atts( [
		'link' => '#',
		'id' => 'salcodes',
		'color' => 'blue',
		'size' => '',
		'label' => 'Button',
		'target' => '_self'
	], $atts );
	$output = '<p><a href="' . esc_url( $a['link'] ) . '" id="' . esc_attr( $a['id'] ) . '" class="button ' . esc_attr( $a['color'] ) . ' ' . esc_attr( $a['size'] ) . '" target="' . esc_attr($a['target']) . '">' . esc_attr( $a['label'] ) . '</a></p>';
	*/
	$output = '<button class="read__more__button">Continue reading</button><span class="hidden stay">';
	return $output;
}


// Sitemap Shortcode
add_shortcode('sitemap', 'sitemap');
function sitemap($attributes, $content = null)
{
	ob_start();
	get_template_part('parts/components/sitemap');
	return ob_get_clean();
}


// Login Form Shortcode
add_shortcode('login', 'login_form');
function login_form($attributes, $content = null)
{

	if (!is_user_logged_in()) {
		ob_start();
		get_template_part('parts/auth/login');
		return ob_get_clean();
	}
}

// Register Form Shortcode
add_shortcode('register', 'register_form');
function register_form($attributes, $content = null)
{
	ob_start();
	get_template_part('parts/auth/register');
	return ob_get_clean();
}


// Lost Password Form Shortcode
add_shortcode('lost_pw', 'lost_pw');
function lost_pw($attributes, $content = null)
{
	ob_start();
	get_template_part('parts/auth/lost-password');
	return ob_get_clean();
}


// Reset Password Form Shortcode
add_shortcode('reset_pw', 'reset_pw');
function reset_pw($attributes, $content = null)
{
	ob_start();
	get_template_part('parts/auth/reset-password');
	return ob_get_clean();
}


// EOF