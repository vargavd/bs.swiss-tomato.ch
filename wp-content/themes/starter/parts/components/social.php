<?php if (trim(have_rows('social_media', 'option'))) : ?>
	<!-- Social Media -->

	<div class="social__link">
		<div class="social__link-icon">
			<?php while (have_rows('social_media', 'option')) : the_row(); ?>
				<a href="<?= esc_url(get_sub_field('social_url', 'option')); ?>" data-icon="<?= get_sub_field('social_icon', 'option'); ?>" class="icon" target="_blank">
					<span class="screen-reader-text"><?= get_sub_field('social_name', 'option'); ?></span>
				</a>
			<?php endwhile; ?>
		</div>
	</div>

<?php

endif;

// EOF