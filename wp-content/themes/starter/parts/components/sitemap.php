<!-- Sitemap -->
<div id="sitemap" class="sitemap">

	<?php

	// Check existance
	if (get_pages()) : ?>
		<section class="sitemap__pages">
			<h2 class="sitemap__title"><?= esc_html__('Pages', 'bs') ?></h2>
			<ul class="sitemap__list">
				<?php

				// List Pages
				wp_list_pages([
					'title_li' => '',
					'exclude'  => get_the_ID(),
				]); ?>
			</ul>
		</section>
	<?php endif; ?>


	<?php

	// Blog's Query
	$blog = new WP_Query([
		'post_type'			=> 'post',
		'posts_per_page'	=> -1,
		'post_status'		=> 'publish',
		'has_password'		=> false,
		'orderby'			=> 'title',
		'order'				=> 'ASC'
	]);

	// Check existance
	if ($blog->have_posts()) : ?>

		<section class="sitemap__posts">

			<h2 class="sitemap__title"><?= esc_html__('News & Events', 'bs') ?></h2>
			<ul class="sitemap__list">
				<?php

				// List Posts
				while ($blog->have_posts()) : $blog->the_post(); ?>
					<li class="sitemap__item"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
				<?php endwhile; ?>
			</ul>

		</section>

	<?php

	endif;
	wp_reset_postdata(); ?>

</div>