<?php

/**
 * 
 * Parts - Filter Result
 *
 */

// Vars
$post_id 	 = $args['post_id'];
$cpt	 	 = $args['post_type'];
$title 	 	 = get_the_title($post_id);
$url	 	 = get_the_permalink($post_id);
$img 	 	 = '';
$cat 	 	 = '';
$video	 	 = '';
$event_start = '';
$event_end 	 = '';
$target	 	 = '_self';
$today		 = date('Y-m-d');
$future 	 = '';

if ($cpt === 'post') {

	// Vars
	$cat 		 = has_category('', $post_id) ? get_the_terms($post_id, 'category')[0] : '';
	$img 		 = get_the_post_thumbnail_url($post_id, 'full') ?: '';
	$event_start = trim(get_field('event_start', $post_id)) ?: '';
	$event_end 	 = trim(get_field('event_end', $post_id)) ?: '';
	$date		 = get_the_date('F j, Y', $post_id);

	if (ICL_LANGUAGE_CODE === 'de') {
		$date = get_the_date('j. F Y', $post_id);
	}

	// ? Event Start Date Exists
	if ($event_start) {

		if (!$event_end) {
			$event_start = date_i18n('F j, Y', strtotime($event_start));
			if (ICL_LANGUAGE_CODE === 'de') {
				$event_start = date_i18n('j. F Y', strtotime($event_start));
			}
		}

		// ? Event End Date Exists
		if ($event_end) {

			$event_start_date = date_i18n('Y-m-d', strtotime($event_start));
			$future = $today < $event_start_date ? true : false;

			// Event Start Split
			$start_year  = date_i18n('Y', strtotime($event_start));
			$start_month = date_i18n('m', strtotime($event_start));
			$start_day   = date_i18n('d', strtotime($event_start));

			// Event End Split
			$end_year  = date_i18n('Y', strtotime($event_end));
			$end_month = date_i18n('m', strtotime($event_end));
			$end_day   = date_i18n('d', strtotime($event_end));

			// Year ++
			if ($start_year < $end_year) {
				$event_start = date_i18n('F j, Y', strtotime($event_start)) . ' - ' . date_i18n('F j, Y', strtotime($event_end));
				if (ICL_LANGUAGE_CODE === 'de') {
					$event_start = date_i18n('j. F Y', strtotime($event_start)) . ' - ' . date_i18n('j. F Y', strtotime($event_end));
				}
			}
			// Month ++
			if ($start_year === $end_year && $start_month < $end_month) {
				$event_start = date_i18n('F j', strtotime($event_start)) . ' - ' . date_i18n('F j', strtotime($event_end)) . ', ' . $start_year;
				if (ICL_LANGUAGE_CODE === 'de') {
					$event_start = date_i18n('j. F', strtotime($event_start)) . ' - ' . date_i18n('j F', strtotime($event_end)) . ', ' . $start_year;
				}
			}
			// Day ++
			if ($start_year === $end_year && $start_month === $end_month && $start_day < $end_day) {
				$event_start = date_i18n('F j', strtotime($event_start)) . ' - ' . date_i18n('j', strtotime($event_end)) . ', ' . $start_year;
				if (ICL_LANGUAGE_CODE === 'de') {
					$event_start = date_i18n('j.', strtotime($event_start)) . ' - ' . date_i18n('j. F', strtotime($event_end)) . ' ' . $start_year;
				}
			}
		}
	}
} elseif ($cpt === 'resources') {
  $cats = wp_get_post_terms($post_id, 'resource-type');
  
  $cat = (is_array($cats) && !empty($cats)) ? $cats[0] : '';

	$placeholder = '';

	// Videos
	if ($cat) {
		if ($cat->term_id === apply_filters('wpml_object_id', 5, 'resource-type')) {
			$placeholder = get_field('resources_video_bg', 'option') ?: '';
			// Fact Sheets
		} elseif ($cat->term_id === apply_filters('wpml_object_id', 6, 'resource-type')) {
			$placeholder = get_field('resources_factsheet_bg', 'option') ?: '';
			// White Papers
		} elseif ($cat->term_id === apply_filters('wpml_object_id', 7, 'resource-type')) {
			$placeholder = get_field('resource_whitepaper_bg', 'option') ?: '';
		}
	}

	$img = get_field('resource_img', $post_id) ?: $placeholder;

	// ? Has Resource Type {$cat}
	if ($cat) {

		// Vars
		$resource_type	  = get_field('resource_type', $post_id) ?: 'file';
		$resource_file	  = (int) get_field('resource_file', $post_id) ?: '';
		$resource_youtube = trim(get_field('resource_youtube', $post_id)) ?: '';

		// If Category !== Video
		if ($cat->term_id !== apply_filters('wpml_object_id', 5, 'resource-type')) {
			$target = '_blank';
			$url 	= wp_get_attachment_url($resource_file);
		}

		// If Video Category
		if ($cat->term_id === apply_filters('wpml_object_id', 5, 'resource-type')) {

			if ($resource_file || $resource_youtube) {
				$video = $resource_type === 'file' ? 'data-lity data-lity-target="' . esc_url(wp_get_attachment_url($resource_file)) . '"' :  'data-lity data-lity-target="//youtube.com/watch?v=' . $resource_youtube . '"';
			}
		}
	}
}

?>
<!-- Post Item -->
<article class="<?= $cpt ?>__item <?= $video ? 'video' : '' ?>" data-sal="slide-up" data-sal-duration="800" data-sal-delay="100" data-sal-easing="ease-out-cubic" <?= $video ?>>

	<?php if ($cpt === 'post' || ($cpt === 'resources' && $cat && $cat->term_id !== apply_filters('wpml_object_id', 5, 'resource-type'))) : ?>
		<a href="<?= esc_url($url) ?>" target="<?= $target ?>" class="absolute"><span class="screen-reader-text"><?= get_the_title() ?></span></a>
	<?php endif; ?>

	<!-- Post Content -->
	<div class="<?= $cpt ?>__content">
		<!-- Post Meta -->
		<div class="<?= $cpt ?>__meta">
			<!-- Post Category -->
			<?php if ($cat) : ?>
				<small class="<?= $cpt ?>__category"><?= $cat->name ?></small>
			<?php endif; ?>
			<?php if ($cpt === 'post') : ?>
				<!-- Post Date -->
				<small class="<?= $cpt ?>__date"><?= $event_start ?: $date ?></small>
				<?php

				// ? Is Post | Is Event Category {20} | Is Administrator / Editor
				if ($cpt === 'post' && $cat && $cat->term_id === apply_filters('wpml_object_id', 20, 'category') && $future && current_user_can('manage_options')) : ?>
					<small class="<?= $cpt ?>__date admin">Admin only: <?= get_the_date("F j, Y", $post_id) ?></small>
				<?php endif; ?>
			<?php endif ?>
		</div>
		<!-- Post Title -->
		<h2 class="<?= $cpt ?>__title"><?= get_the_title() ?></h2>
		<?php if ($cpt === 'post') : ?>
			<!-- Post Text -->
			<p class="<?= $cpt ?>__text"><?= get_excerpt(160) ?></p>
		<?php endif ?>
	</div>
	<!-- Post Image -->
	<div class="<?= $cpt ?>__image">
		<?= $img ? custom_image_size($img, 'medium', $title, $cpt . '__img', '', 'none') : '' ?>
	</div>
</article>