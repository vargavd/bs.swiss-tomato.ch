<?php

/**
 * * Block Name: testimonials
 * * Block Title: Testimonials
 * * Block Description: Testimonials Block
 * * Block Icon: slides
 * * Block Multiple: true
 * * Block Script: true
 * * Block Keywords: testimonial sliders, testimonial slide, testimonials
 * * Post Types: page
 */

// Basic Settings
$id	   = !empty($block['anchor']) ? $block['anchor'] : '';
$class = !empty($block['className']) ? 'testimonials ' . $block['className'] : 'testimonials';

// Advanced Settings
$testimonial_title = trim(get_field('testimonial_title')) ?: '';
$is_history		   = (bool) get_field('is_history');

if (trim(have_rows('testimonial_slider'))) : ?>

	<!-- Testimonials -->
	<section <?= $id ? 'id="' . esc_attr($id) . '"' : '' ?> class="<?= esc_attr($class) ?>">

		<div class="inner">

			<?php if ($testimonial_title) : ?>
				<header class="testimonial__header">
					<h2 class="testimonial__title small__title"><?= $testimonial_title ?></h2>
				</header>
			<?php endif; ?>

			<div class="swiper-container testimonial__container <?= true === $is_history ? 'history' : '' ?>">

				<div class="swiper-wrapper testimonial__wrapper">

					<?php

					$x = 0;
					// Loop Testimonials
					while (have_rows('testimonial_slider')) : the_row();
						$x++;

						// Vars
						$testimonial_text   = trim(get_sub_field('testimonial_text')) ?: '';
						$testimonial_image  = get_sub_field('testimonial_image') ?: '';
						$testimonial_year 	= trim(get_sub_field('testimonial_number')) ?: '';
						$testimonial_credit = trim(get_sub_field('testimonial_credit')) ?: '';
						$testimonial_link 	= get_sub_field('testimonial_link') ?: '';
						$testimonial_url	= '';
						$testimonial_title	= '';
						$testimonial_target	= '';

						if ($testimonial_link) {
							$testimonial_url	= $testimonial_link['url'] ?: '';
							$testimonial_title	= $testimonial_link['title'] ?: esc_html__('Learn more', 'bs');
							$testimonial_target	= $testimonial_link['target'] ?: '_self';
						}
					?>

						<div class="swiper-slide testimonial__slide">
							<!-- Content -->
							<div class="testimonial__content">
								<?php if (true === $is_history) : ?>
									<?php if ($testimonial_year) : ?>
										<!-- History Number -->
										<div class="testimonial__year"><?= $testimonial_year; ?></div>
									<?php endif; ?>
									<?php if ($testimonial_credit) : ?>
										<!-- History Credit -->
										<small class="testimonial__subtitle"><?= $testimonial_credit; ?></small>
									<?php endif; ?>
								<?php endif; ?>
                <div class="testimonial__text_and_image">
                  <?php if ($testimonial_image): ?>
                    <!-- Testimonial Image -->
                    <img src="<?= $testimonial_image['url'] ?>" class="testimonial__image" />
                  <?php endif; ?>
                  
                  <?php if ($testimonial_text) : ?>
                    <!-- Testimonial Text -->
                    <h3 class="testimonial__text <?= true === $is_history ? 'history' : '' ?>">
                      <?= $testimonial_text ?>
                    </h3>
                  <?php endif; ?>
                </div>
								<?php if ($testimonial_url) : ?>
									<a href="<?= $testimonial_url ?>" target="<?= $testimonial_target ?>" class="testimonial__link read__more"><?= $testimonial_title ?></a>
								<?php endif; ?>
								<?php if (false === $is_history	&& $testimonial_credit) : ?>
									<!-- Testimonial Credit -->
									<small class="testimonial__credit <?= $testimonial_url ? 'to__right' : '' ?>"><?= $testimonial_credit; ?></small>
								<?php endif; ?>
							</div>
						</div>

					<?php endwhile; ?>

				</div>

				<?php if ($x > 1) : ?>
					<?php if (true !== $is_history) : ?>
						<div class="navigation__area">
							<div class="pagination__<?= true !== $is_history ? 'bullets' : 'scrollbar swiper-scrollbar' ?>"></div>
						</div>
					<?php else : ?>
						<div class="swiper-scrollbar"></div>
					<?php endif; ?>
				<?php endif; ?>

			</div>

		</div>

	</section>


<?php

endif;

// EOF