<?php

/**
 * * Block Name: imagecontent
 * * Block Title: Image + Text
 * * Block Description: Image + Text Block
 * * Block Icon: align-left
 * * Block Multiple: true
 * * Block Keywords: image, content
 * * Post Types: page
 */

// Basic Settings
$id 		= !empty($block['anchor']) ? $block['anchor'] : '';
$id_comment = ucwords(preg_replace('!\s+!', ' ', str_replace('_', ' ', $id))) ?: 'Image + Content';
$class		= !empty($block['className']) ? 'imagecontent ' . $block['className'] : 'imagecontent';

// Advanced Settings
$start_reverse = (bool)get_field('start_reverse'); ?>

<?php if (trim(have_rows('image_content'))) : ?>

	<?php if ($id_comment) : ?>
		<!-- <?= $id_comment ?> -->
	<?php endif; ?>
	<div <?= $id ? 'id="' . esc_attr($id) . '"' : '' ?> class="<?= esc_attr($class) ?>">

		<?php
		$x = 0;
		while (have_rows('image_content')) : the_row();
			$x++;

			// Vars
			$image_container = get_sub_field('img_container');
			$image_or_video  = $image_container['image_or_video'] ?: '';
			$img 			 = '';
			$video 			 = '';
			$video_bg 		 = '';
			$youtube 		 = '';
			$youtube_bg		 = '';

			// Image Container
			if ($image_container) {


				// Is Image?
				if ($image_or_video === 'image') {
					$img = $image_container['img'] ?: '';
				}

				// Is Video?
				if ($image_or_video === 'video') {
					$video 	  = $image_container['video'] ?: '';
					$video_bg = $image_container['video_bg'] ?: '';
				}

				// Is Youtube?
				if ($image_or_video === 'youtube') {
					$youtube	= $image_container['youtube'] ?: '';
					$youtube_bg = $image_container['youtube_background'] !== 'gray' ? 'white' : '';
					$video_bg 	= $image_container['video_bg'] ?: '';
				}
			}

			// Vars
			$content_container  = get_sub_field('content_container');

			// Content Container
			if ($content_container) {
				$content_title  = trim($content_container['title']) ?: '';
				$alt 			= $content_title ?: 'image for block';
				$text  			= trim($content_container['text']) ?: '';
				$url  			= $content_container['url'] ?: '';
				$icon 			= trim($content_container['button_icon']) ?: '';
				$subtitle 		= trim($content_container['subtitle']) ?: '';

				// Has Url
				if ($url) {
					$url 		= $content_container['url']['url'] ?: '';
					$url_title  = $content_container['url']['title'] ?: 'Read more';
					$url_target = $content_container['url']['target'] ?: '_self';
				}
				$button_or_text = $content_container['button_or_text'] ?: 'text';
			}

			$reverse = '';

			if (true !== $start_reverse) {
				$reverse = $x % 2 ? '' : ' reverse';
			} else {
				$reverse = $x % 2 ? ' reverse' : '';
			}

			if ($img || $video || $youtube && ($content_title || $text || $url)) : ?>

				<?php

				if ($img) {
					$attachment_id = attachment_url_to_postid($img);
					$description   = trim(get_attachment_data($attachment_id)['description']) ?: '';
				}

				?>

				<article class="imagecontent__item grid<?= $reverse  ?><?= $youtube && !$video_bg ? ' youtube' : '' ?> <?= $youtube_bg ?>">

					<?php if ($youtube && !$video_bg) : ?>
						<div class="imagecontent__helper inner narrow">
						<?php endif; ?>

						<!-- Content -->
						<div class="imagecontent__content__container">
							<div class="helper">
								<?php if ($content_title) : ?>
									<h2 class="imagecontent__title small__title" data-sal="slide-up"><?= $content_title ?></h2>
								<?php endif; ?>
								<?php if ($text || $subtitle) : ?>
									<div class="imagecontent__text" data-sal="slide-up" data-sal-duration="500" data-sal-delay="100">
										<?php if ($subtitle) : ?>
											<h3 class="imagecontent__subtitle"><?= $subtitle ?></h3>
										<?php endif; ?>
										<?= $text ?>
										<?php if ($button_or_text === 'text' && $url) : ?>
											<a href="<?= esc_url($url) ?>" target="<?= $url_target ?>" class="read__more"><?= $url_title ?></a>
										<?php endif; ?>
										<?php if ($button_or_text === 'button' && $url) : ?>
											<a href="<?= esc_url($url) ?>" target="<?= $url_target ?>" <?= $icon ? 'data-icon="' . $icon . '"' : '' ?> class="button"><?= $url_title ?></a>
										<?php endif; ?>
									</div>
								<?php endif; ?>
							</div>
						</div>

						<!-- Image -->
						<div class="imagecontent__image__container">
							<?php if ($img && $description) : ?>
								<div class="helper">
								<?php endif; ?>
								<?php if ($image_or_video === 'image' && $img) : ?>
									<div class="imagecontent__image__wrapper" data-sal="zoom-out" data-sal-duration="2000">
										<?= custom_image_size($img, 'medium_large', $alt) ?>
									</div>
								<?php endif; ?>
								<?php if ($image_or_video === 'video' && $video) : ?>
									<div class="imagecontent__video__wrapper" data-sal="zoom-out" data-sal-duration="2000" data-lity data-lity-target="<?= $video ?>">
										<?php if (!wp_is_mobile()) : ?>
											<video playsinline preload autoplay muted loop muted>
												<source type="video/mp4" src="<?= $video ?>">
											</video>
										<?php else : ?>
											<?= custom_image_size($video_bg, 'full', esc_html__('Video poster image', 'bs'), 'imagecontent__video__bg', '', 'none') ?>
										<?php endif; ?>
									</div>
								<?php endif; ?>
								<?php if ($image_or_video === 'youtube' && $youtube && $video_bg) : ?>
									<div class="imagecontent__video__wrapper" data-sal="zoom-out" data-sal-duration="2000" data-lity data-lity-target="//youtube.com/watch?v=<?= $youtube ?>">
										<?= custom_image_size($video_bg, 'full', esc_html__('Video poster image', 'bs'), 'imagecontent__video__bg', '', 'none') ?>
									</div>
								<?php endif; ?>
								<?php if ($image_or_video === 'youtube' && $youtube && !$video_bg) : ?>
									<div class="imagecontent__youtube__wrapper">
										<iframe class="youtube__player" width="573" height="324" src="//youtube.com/embed/<?= $youtube ?>" frameborder="0" allowfullscreen></iframe>
									</div>
								<?php endif; ?>
								<?php if ($img && $description) : ?>
									<span class="credits"><?= $description ?></span>
								</div>
							<?php endif; ?>
						</div>

						<?php if ($youtube && !$video_bg) : ?>
						</div>
					<?php endif; ?>

				</article>

			<?php endif; ?>

		<?php endwhile; ?>

	</div>

<?php

endif;

// EOF