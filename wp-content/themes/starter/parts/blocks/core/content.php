<?php

/**
 * * Block Name: content
 * * Block Title: Content
 * * Block Description: Content Block
 * * Block Icon: text
 * * Block Multiple: true
 * * Block Content: true
 * * Block Keywords: content, text
 * * Post Types: page
 */

// Basic Settings
$id 		= !empty($block['anchor']) ? $block['anchor'] : '';
$id_comment = ucwords(preg_replace('!\s+!', ' ', str_replace('_', ' ', $id))) ?: 'Content';
$class		= !empty($block['className']) ? 'content__acf ' . $block['className'] : 'content__acf';
if (!empty($block['align_content'])) {
	$class .= ' ' . $block['align_content'];
}

// Advanced Settings
$content_title = trim(get_field('content_title')) ?: '';
$content_text  = trim(get_field('content_text')) ?: '';
$content_url   = get_field('content_url') ?: '';

$content_url_url = '';

if ($content_url) {
	$content_url_title = $content_url['title'] ?: 'Discover more';
	$content_url_url   = $content_url['url'] ?: '';
	$content_target    = $content_url['target'] ?: '_self';
}

$text_or_button = get_field('content_url_text_or_button') ?: 'text';

$link_type = $text_or_button === 'text' ? 'read__more' : 'button';

$tag = 'div';

if ($content_title) {
	$tag = 'section';
}

if ($content_title || $content_text) : ?>

	<?php if ($id_comment) : ?>
		<!-- <?= $id_comment ?> -->
	<?php endif; ?>
	<<?= $tag ?> <?= $id ? 'id="' . esc_attr($id) . '"' : '' ?> class="<?= esc_attr($class) ?> <?= $content_title ? '' : 'no__title' ?>">
		<div class="inner narrow">
			<?php if ($content_title) : ?>
				<header class="contet__header">
					<h2 class="content__title small__title"><?= $content_title ?></h2>
				</header>
			<?php endif; ?>
			<div class="content__align">
				<?php if ($content_text) : ?>
					<h2 class="content__text">
						<?= $content_text ?>
					</h2>
				<?php endif; ?>
				<?php if ($content_url_url) : ?>
					<a href="<?= esc_url($content_url_url) ?>" target="<?= $content_target ?>" class="content__url <?= $link_type ?>"><?= $content_url_title ?></a>
				<?php endif; ?>
			</div>
		</div>
	</<?= $tag ?>>

<?php

endif;

// EOF