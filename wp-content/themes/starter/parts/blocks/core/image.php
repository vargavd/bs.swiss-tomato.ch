<?php

/**
 * * Block Name: image
 * * Block Title: Image
 * * Block Description: Image Block
 * * Block Icon: cover-image
 * * Block Align: full
 * * Block Multiple: true
 * * Block Keywords: image, picture
 * * Post Types: page, events
 */

// Basic Settings
$id 		= !empty($block['anchor']) ? $block['anchor'] : '';
$id_comment = ucwords(preg_replace('!\s+!', ' ', str_replace('_', ' ', $id)));
$class		= !empty($block['className']) ? $block['className'] : 'image__block';
if (!empty($block['align'])) {
	$class .= ' ' . $block['align'];
}

// Advanced Settings
$title  	  = trim(get_field('image_title')) ?:  '';
$title_alt    = $title ?: esc_html__('Image description', 'bs');
$img		  = get_field('image') ?: '';
$image_link   = get_field('image_link') ?: '';
$image_url	  = '';
$image_title  = '';
$image_target = '';

if ($image_link) {
	$image_url		= $image_link['url'] ?: '';
	$image_title	= $image_link['title'] ?: '';
	$image_target	= $image_link['target'] ?: '_self';
}

?>

<?php if ($img) : ?>

	<?php if ($id_comment) : ?>
		<!-- <?= $id_comment ?> -->
	<?php endif; ?>
	<section <?= $id ? 'id="' . esc_attr($id) . '"' : '' ?> class="<?= esc_attr($class) ?>">
		<div class="grid image__grid">
			<div class="image__holder">
				<?php if ($title) : ?>
					<header class="image__header">
						<h2 class="image__title"><?= $title ?></h2>
					</header>
				<?php endif; ?>
				<?php if ($image_url) : ?>
					<a href="<?= $image_url ?>" target="<?= $image_target ?>">
					<?php endif; ?>
					<?= custom_image_size($img, 'full', sanitize_text_field($title_alt), 'image__img') ?>
					<?php if ($image_url) : ?>
					</a>
				<?php endif; ?>
			</div>
		</div>
	</section>

<?php

endif;

// EOF