<?php

/**
 * * Block Name: resources
 * * Block Title: Resources
 * * Block Description: Resources Block
 * * Block Icon: open-folder
 * * Block Keywords: resources, fact sheets, white papers, videos
 * * Post Types: page
 */

// Basic Settings
$id 		= !empty($block['anchor']) ? $block['anchor'] : '';
$id_comment = ucwords(preg_replace('!\s+!', ' ', str_replace('_', ' ', $id))) ?: 'Resources';
$class		= !empty($block['className']) ? 'resources ' . $block['className'] : 'resources';

// Advanced Settings
$resources_title = trim(get_field('resources_title')) ?: '';

if (have_rows('fact_sheets') || have_rows('white_papers') || have_rows('demo_videos')) : ?>

	<?php if ($id_comment) : ?>
		<!-- <?= $id_comment ?> -->
	<?php endif; ?>
	<section <?= $id ? 'id="' . esc_attr($id) . '"' : '' ?> class="<?= esc_attr($class) ?>">

		<div class="inner narrow">
			<?php if ($resources_title) : ?>
				<header class="resources__header">
					<h2 class="resources__title small__title"><?= $resources_title ?></h2>
				</header>
			<?php endif; ?>

			<div class="resources__grid grid col__3 gap__2">

				<?php if (have_rows('fact_sheets')) : ?>

					<div class="resources__item">

						<p class="resources__smalltitle"><?= esc_html__('Fact Sheets', 'bs') ?></p>

						<ul class="resources__list">

							<?php

							$i = 0;
							// Loop Fact Sheets
							while (have_rows('fact_sheets')) : the_row();
								$i++;

								// Vars
								$fact_sheet 	  = get_sub_field('fact_sheet') ?: '';
								$fact_sheet_id 	  = '';
								$fact_sheet_title = '';

								if ($fact_sheet) {
									$fact_sheet_id 	  = (int) $fact_sheet->ID ?: '';
									$fact_sheet_title = $fact_sheet->post_title ?: '';
								}

								$resource_file = (int) get_field('resource_file', $fact_sheet_id) ?: '';
							?>

								<?php if ($fact_sheet_id) : ?>
									<li class="resource__item" data-sal="slide-up" data-sal-duration="800" data-sal-delay="200" data-sal-easing="ease-out-cubic">
										<?php if ($resource_file) : ?>
											<a href="<?= esc_url(wp_get_attachment_url($resource_file)) ?>" target="_blank" rel="alternate" class="resource__link">
											<?php endif; ?>
											<h3 class="resource__title"><?= $fact_sheet_title ?></h3>
											<?php if ($resource_file) : ?>
											</a>
										<?php endif; ?>
									</li>
								<?php endif; ?>

							<?php endwhile; ?>
						</ul>
					</div>
				<?php endif; ?>

				<?php if (have_rows('white_papers')) : ?>

					<div class="resources__item">

						<p class="resources__smalltitle"><?= esc_html__('White Papers', 'bs') ?></p>

						<ul class="resources__list">

							<?php

							$i = 0;
							// Loop Fact Sheets
							while (have_rows('white_papers')) : the_row();
								$i++;

								// Vars
								$white_paper 	  = get_sub_field('white_paper') ?: '';
								$white_paper_id 	  = '';
								$white_paper_title = '';

								if ($white_paper) {
									$white_paper_id    = (int) $white_paper->ID ?: '';
									$white_paper_title = $white_paper->post_title ?: '';
								}

								$resource_file = (int) get_field('resource_file', $white_paper_id) ?: '';
							?>

								<?php if ($white_paper_id) : ?>
									<li class="resource__item" data-sal="slide-up" data-sal-duration="800" data-sal-delay="200" data-sal-easing="ease-out-cubic">
										<?php if ($resource_file) : ?>
											<a href="<?= esc_url(wp_get_attachment_url($resource_file)) ?>" target="_blank" rel="alternate" class="resource__link">
											<?php endif; ?>
											<h3 class="resource__title"><?= $white_paper_title ?></h3>
											<?php if ($resource_file) : ?>
											</a>
										<?php endif; ?>
									</li>
								<?php endif; ?>

							<?php endwhile; ?>
						</ul>
					</div>
				<?php endif; ?>

				<?php if (have_rows('videos')) : ?>

					<div class="resources__item">

						<p class="resources__smalltitle"><?= esc_html__('Videos', 'bs') ?></p>

						<ul class="resources__list">

							<?php

							$i = 0;
							// Loop Fact Sheets
							while (have_rows('videos')) : the_row();
								$i++;

								// Vars
								$video 	  	 = get_sub_field('video') ?: '';
								$video_id 	 = '';
								$video_title = '';

								if ($video) {
									$video_id 	 = (int) $video->ID ?: '';
									$video_title = $video->post_title ?: '';
								}

								$resource_type	  = get_field('resource_type', $video_id) ?: 'file';
								$resource_file	  = (int) get_field('resource_file', $video_id) ?: '';
								$resource_youtube = trim(get_field('resource_youtube', $video_id)) ?: '';

							?>

								<?php if ($video_id) : ?>
									<li class="resource__item video" data-sal="slide-up" data-sal-duration="800" data-sal-delay="200" data-sal-easing="ease-out-cubic" data-lity data-lity-target="<?= $resource_type === 'file' ? esc_url(wp_get_attachment_url($resource_file)) : '//youtube.com/watch?v=' . $resource_youtube ?>">
										<h3 class="resource__title"><?= $video_title ?></h3>
									</li>
								<?php endif; ?>

							<?php endwhile; ?>
						</ul>
					</div>
				<?php endif; ?>

			</div>
		</div>

	</section>

<?php

endif;

// EOF