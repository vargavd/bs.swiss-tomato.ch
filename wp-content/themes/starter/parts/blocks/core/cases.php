<?php

/**
 * * Block Name: cases
 * * Block Title: Case Slider
 * * Block Description: Case Slider Block
 * * Block Icon: slides
 * * Block Multiple: true
 * * Block Script: true
 * * Block Keywords: case sliders, case slide, case study
 * * Post Types: page
 */

// Basic Settings
$id	   = !empty($block['anchor']) ? $block['anchor'] : '';
$class = !empty($block['className']) ? 'cases ' . $block['className'] : 'cases';

// Advanced Settings
$case_title = trim(get_field('case_title')) ?: '';

if (trim(have_rows('case_slider'))) : ?>

	<!-- Case Studies -->
	<section <?= $id ? 'id="' . esc_attr($id) . '"' : '' ?> class="<?= esc_attr($class) ?>">

		<div class="inner">

			<?php if ($case_title) : ?>
				<header class="case__header">
					<h2 class="case__title small__title"><?= $case_title ?></h2>
				</header>
			<?php endif; ?>

			<div class="swiper-container case__container">

				<div class="swiper-wrapper case__wrapper">

					<?php

					$x = 0;
					// Loop Cases
					while (have_rows('case_slider')) : the_row();
						$x++;

						// Vars
						$case_text 	   = trim(get_sub_field('case_text')) ?: '';
						$case_logo 	   = get_sub_field('case_logo') ?: '';
						$case_customer = trim(get_sub_field('case_customer')) ?: '';
						$case_file 	   = get_sub_field('case_file') ?: ''; ?>

						<div class="swiper-slide case__slide">
							<!-- Content -->
							<div class="case__content">
								<?php if ($case_text) : ?>
									<!-- Case Text -->
									<h2 class="case__text"><?= $case_text; ?></h2>
								<?php endif; ?>
								<?php if ($case_file) : ?>
									<!-- Case File -->
									<a href="<?= esc_url($case_file) ?>" target="_blank" class="case__file read__more"><?= esc_html__('Read case study', 'bs') ?></a>
								<?php endif; ?>
							</div>
							<div class="case__image">
								<?php if ($case_logo) : ?>
									<!-- Case Logo -->
									<?= custom_image_size($case_logo, 'medium', $case_text ?: esc_html__('Placeholder Title', 'bs'), 'case__logo') ?>
								<?php endif; ?>
								<?php if ($case_customer) : ?>
									<!-- Case Cite -->
									<p class="case__cite"><?= $case_customer; ?></p>
								<?php endif; ?>

							</div>
						</div>

					<?php endwhile; ?>

				</div>

				<?php if ($x > 1) : ?>
					<div class="navigation__area">
						<div class="pagination__bullets"></div>
					</div>
				<?php endif; ?>

			</div>

		</div>

	</section>

<?php

endif;

// EOF