<?php

/**
 * * Block Name: lists
 * * Block Title: Lists
 * * Block Description: Lists Block
 * * Block Icon: table-row-after
 * * Block Multiple: true
 * * Block Script: true
 * * Block Keywords: lists, cols
 * * Post Types: page
 */

// Basic Settings
$id 		= !empty($block['anchor']) ? $block['anchor'] : '';
$id_comment = ucwords(preg_replace('!\s+!', ' ', str_replace('_', ' ', $id))) ?: 'Lists';
$class		= !empty($block['className']) ? 'lists ' . $block['className'] : 'lists';

// Advanced Settings
$lists_title		= trim(get_field('lists_title')) ?: '';
$lists_background   = get_field('lists_background') ?: 'gray';
$cols				= (int)get_field('lists_size') ?: 3;
$image_switch		= get_field('image_switch') ?: '';
$title_switch		= (bool)get_field('title_switch');
$text_switch		= (bool)get_field('text_switch');
$link_switch		= (bool)get_field('link_switch');
$list_icon_size		= get_field('list_icon_size') ?: 'big';

?>

<?php if ($id_comment) : ?>
	<!-- <?= $id_comment ?> -->
<?php endif; ?>
<section <?= $id ? 'id="' . esc_attr($id) . '"' : '' ?> class="<?= esc_attr($class) ?> <?= $lists_background ?> <?= $image_switch === 'number' ? 'number__animated' : '' ?>">

	<div class="inner narrow">

		<?php if ($lists_title) : ?>
			<header class="lists__header">
				<h2 class="lists__title small__title"><?= $lists_title ?></h2>
			</header>
		<?php endif; ?>

		<div class="grid lists__grid col__<?= $cols ?>">

			<?php

			if (trim(have_rows('lists'))) :
				$i = 0;
				while (have_rows('lists')) : the_row();
					foreach (range(1, $cols) as $i) :

						$list = get_sub_field('list_' . $i) ?: '';

						$image_size = $i > 2 ? 'medium' : 'medium';


						if ($list) {

							$icon = '';
							if ($image_switch === 'icon') {
								$icon = $list['list_icon_' . $i] ?: '';
							}

							$image = '';
							if ($image_switch === 'image') {
								$image = $list['list_image_' . $i] ?: '';
							}

							$number = '';
							if ($image_switch === 'number') {
								$number = number_format((float) $list['list_number_' . $i], 0, ',', '.') ?: '';
							}

							$number_after = trim($list['list_number_after_' . $i]) ?: '';

							$title = '';
							if (true === $title_switch) {
								$title = trim($list['list_title_' . $i]) ?: '';
							}

							$text = '';
							if (true === $text_switch) {
								$text = trim($list['list_text_' . $i]) ?: '';
							}

							$link 		 = '';
							$link_url 	 = '';
							$link_title  = '';
							$link_target = '';
							if (true === $link_switch) {
								$link = $list['list_link_' . $i] ?: '';

								if ($link) {
									$link_url 	 = $link['url'] ?: '';
									$link_title  = $link['title'] ?: esc_html__('Learn more', 'bs');
									$link_target = $link['target'] ?: '_self';
								}
							}
						}
			?>

						<div class="list__item" data-sal="slide-up" data-sal-duration="800" data-sal-delay="<?= $i++ ?>00" data-sal-easing="ease-out-cubic">
							<?php if ($icon || $image || $number) : ?>
								<div class="list__<?= $icon ? 'icon' : '' ?><?= $image ? 'image' : '' ?><?= $number ? 'number' : '' ?> <?= $list_icon_size ?>" <?= $number_after ? 'data-after="' . $number_after . '"' : '' ?> <?= $icon ? 'data-icon="' . $icon . '"' : '' ?>>
									<!-- Has Image? -->
									<?php if ($image) : ?>
										<?= custom_image_size($image, $image_size, $title) ?>
									<?php endif; ?>
									<?php if ($number) : ?>
										<?= $number ?>
									<?php endif; ?>
								</div>
							<?php endif; ?>
							<?php if ($title) : ?>
								<!-- Title -->
								<h3 class="list__title <?= !$text ?: 'extended' ?> <?= $link_url ? 'more' : '' ?>"><?= $title ?></h3>
							<?php endif; ?>
							<?php if ($text) : ?>
								<!-- Text -->
								<p class="list__text <?= !$text ?: 'extended' ?> <?= $link_url ? 'more' : '' ?>"><?= $text ?></p>
							<?php endif; ?>
							<?php if ($link_url) : ?>
								<a href="<?= $link_url ?>" target="<?= $link_target ?>" class="list__link read__more"><?= $link_title ?></a>
							<?php endif; ?>
						</div>

			<?php endforeach;
				endwhile;
			endif;
			?>

		</div>

</section>