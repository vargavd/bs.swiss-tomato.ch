<?php

/**
 * * Block Name: products
 * * Block Title: Products
 * * Block Description: Products Block
 * * Block Icon: plugins-checked
 * * Block Keywords: products, services
 * * Block Script: true
 * * Post Types: page
 */

// Basic Settings
$id 		= !empty($block['anchor']) ? $block['anchor'] : '';
$id_comment = ucwords(preg_replace('!\s+!', ' ', str_replace('_', ' ', $id))) ?: 'Products';
$class		= !empty($block['className']) ? 'products ' . $block['className'] : '';

// Advanced Settings
$products_title 	= trim(get_field('products_title')) ?: '';
$dynamic_or_static	= trim(get_field('dynamic_or_static')) ?: 'dynamic';

// ? Has Product Parents Row
if ($dynamic_or_static === 'dynamic' && trim(have_rows('product_parents'))) :
	$count = count(get_field('product_parents')); ?>

	<?php if ($id_comment) : ?>
		<!-- <?= $id_comment ?> -->
	<?php endif; ?>
	<section <?= $id ? 'id="' . esc_attr($id) . '"' : '' ?> class="<?= esc_attr($class) ?>">

		<div class="inner">
			<?php if ($products_title) : ?>
				<header class="products__header">
					<h2 class="entry__title"><?= $products_title ?></h2>
				</header>
			<?php endif; ?>

			<?php

			// ? Product Parents' Row bigger then 1
			if ($count > 1) : ?>

				<div class="products__tab">
					<?php

					$x = 0;
					while (have_rows('product_parents')) : the_row();
						$x++;

						// Vars
						$is_active 		= $x === 1 ? ' active' : '';
						$product_parent = get_the_title((int) get_sub_field('product_parent')) ?: ''; ?>

						<h2 class="products__links<?= $is_active ?>" onclick="openProductTab(event, 'products__<?= $x ?>')"><?= $product_parent ?></button>

						<?php endwhile; ?>
				</div>

			<?php endif; ?>

			<?php

			$x = 0;
			// * Loop Product Parents
			while (have_rows('product_parents')) : the_row();

				$x++;

				// Vars
				$product_background = get_sub_field('product_background') ?: THEMEPATH . '/assets/img/video_bg.jpg';
				$product_parent 	= (int) get_sub_field('product_parent') ?: '';
				$product_columns	= (int) get_sub_field('product_columns') ?: '';
				$is_active = $x === 1 ? ' active' : ''; ?>

				<ul data-product="products__<?= $x ?>" class="grid col__<?= $product_columns ?> products__container<?= $is_active ?> scroll-trigger-elem" style="--product-bg:url(<?= $product_background ?>)">

					<?php

					$exclude = [1706];

					if (ICL_LANGUAGE_CODE === 'de') {
						$exclude = lang_object_ids($exclude, 'page', 'de');
					}

					$pages = get_pages([
						'post_status' 	=> 'publish',
						'sort_column'	=> 'menu_order',
						'exclude'		=> $exclude, // exclude some childrens
					]);

					$childrens = get_page_children($product_parent, $pages);

					// * Loop Childrens
					foreach ($childrens as $children) :

						$page_id	   = $children->ID;
						$page_title    = $children->post_title;
						$child_logo	   = get_field('child_logo', $page_id) ?: '';
						$child_excerpt = trim(get_field('child_excerpt', $page_id)) ?: 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas varius tortor.';

            if ($product_parent != $children->post_parent) {
              // display only 1 level of children
              continue;
            }

					?>

						<li class="products__item">
							<a href="<?php the_permalink($page_id) ?>" class="absolute" title="<?=$page_title?>"></a>
							<div class="products__helper">
								<?php if ($child_logo) : ?>
									<?= custom_image_size($child_logo, 'logo', $page_title, 'products__logo', '', 'none') ?>
								<?php endif; ?>
								<div class="products__content">
									<h3 class="products__title"><?= $page_title; ?></h3>
									<?php if ($child_excerpt) : ?>
										<p class="products__text"><?= $child_excerpt ?></p>
									<?php endif; ?>
								</div>
							</div>
						</li>

					<?php

					// * End Loop
					endforeach; ?>
				</ul>
			<?php

			// * End Loop
			endwhile; ?>
		</div>

	</section>

<?php

elseif ($dynamic_or_static === 'static' && trim(have_rows('product_static_data'))) : ?>

	<section <?= $id ? 'id="' . esc_attr($id) . '"' : '' ?> class="<?= esc_attr($class) ?>">

		<div class="inner">
			<?php if ($products_title) : ?>
				<header class="products__header">
					<h2 class="entry__title"><?= $products_title ?></h2>
				</header>
			<?php endif; ?>

			<?php
			$product_static_background = get_field('product_static_background') ?: THEMEPATH . '/assets/img/video_bg.jpg';
			$product_static_columns    = (int) get_field('product_static_columns') ?: 3; ?>

			<ul class="grid col__<?= $product_static_columns ?> scroll-trigger-elem active products__container" style="--product-bg:url(<?= $product_static_background ?>)">

				<?php

				// Loop Product Static Data
				while (have_rows('product_static_data')) : the_row();

					// Vars
					$product_link  	  = get_sub_field('product_link') ?: '';
					$product_link_url = '';

					if ($product_link) {
						$product_link_url 	 = $product_link['url'] ?: '';
						$product_link_target = $product_link['target'] ?: '_self';
					}
					$product_logo  = get_sub_field('product_logo') ?: '';
					$product_title = trim(get_sub_field('product_title')) ?: '';
					$product_text = trim(get_sub_field('product_text')) ?: ''; ?>

					<li class="products__item">
						<?php if ($product_link_url) : ?>
							<a href="<?= esc_url($product_link_url) ?>" target="<?= $product_link_target ?>" class="absolute"></a>
						<?php endif; ?>
						<div class="products__helper">
							<?php if ($product_logo) : ?>
								<?= custom_image_size($product_logo, 'logo', $product_title ?: esc_html__('Logo without alt description', 'bs'), 'products__logo', '', 'none')  ?>
							<?php endif; ?>
							<?php if ($product_title || $product_text) : ?>
								<div class="products__content">
									<?php if ($product_title) : ?>
										<h3 class="products__title"><?= $product_title; ?></h3>
									<?php endif; ?>
									<?php if ($product_text) : ?>
										<p class="products__text"><?= $product_text ?></p>
									<?php endif; ?>
								</div>
							<?php endif; ?>
						</div>
					</li>
				<?php endwhile; ?>
			</ul>
		</div>

	</section>

<?php

endif;

// EOF