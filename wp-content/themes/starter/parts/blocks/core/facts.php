<?php

/**
 * * Block Name: facts
 * * Block Title: Facts
 * * Block Description: Facts Block
 * * Block Icon: marker
 * * Block Multiple: true
 * * Block Script: true
 * * Block Keywords: facts, numbers
 * * Post Types: page
 */

// Basic Settings
$id 		= !empty($block['anchor']) ? $block['anchor'] : '';
$id_comment = ucwords(preg_replace('!\s+!', ' ', str_replace('_', ' ', $id))) ?: 'Facts';
$class		= !empty($block['className']) ? 'facts ' . $block['className'] : 'facts';

// Advanced Settings
$facts_title = trim(get_field('facts_title')) ?: '';
$count 		 = count(get_field('facts_items')) ?: '';

if (trim(have_rows('facts_items'))) : ?>

	<?php if ($id_comment) : ?>
		<!-- <?= $id_comment ?> -->
	<?php endif; ?>
	<section <?= $id ? 'id="' . esc_attr($id) . '"' : '' ?> class="<?= esc_attr($class) ?>">
		<div class="inner">
			<?php if ($facts_title) : ?>
				<header class="entry__title">
					<h1><?= $facts_title ?></h1>
				</header>
			<?php endif; ?>

			<div class="facts__grid grid col__<?= $count ?>">

				<?php while (have_rows('facts_items')) : the_row();

					// Vars
					$number_or_image   = get_sub_field('number_or_image') ?: '';
					$fact_icon 		   = get_sub_field('fact_icon') ?: '';
					$fact_text	 	   = trim(get_sub_field('fact_text')) ?: '';
					$fact_number 	   = number_format((float) get_sub_field('fact_number'), 0, ',', '.') ?: '';
					$fact_number_after = trim(get_sub_field('fact_number_after')) ?: '';

				?>

					<div class="facts__item">
						<div class="helper">
							<?php if ('number' === $number_or_image && $fact_number) : ?>
								<span class="facts__number" <?= $fact_number_after ? 'data-after="' . $fact_number_after . '"' : '' ?> data-sal="fade-in"><?= $fact_number ?></span>
							<?php endif; ?>
							<?php if ('image' === $number_or_image && $fact_icon) : ?>
								<span class="facts__icon" data-sal="fade-in"><?= custom_image_size($fact_icon, 'full', $fact_text); ?></span>
							<?php endif; ?>
							<?php if ($fact_text) : ?>
								<p class="facts__text" data-sal="slide-up" data-sal-duration="500" data-sal-delay="0">
									<?= $fact_text ?>
								</p>
							<?php endif; ?>
						</div>
					</div>

				<?php endwhile; ?>

			</div>

		</div>

	</section>

<?php

endif;

// EOF