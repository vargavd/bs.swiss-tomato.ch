<?php

/**
 * * Block Name: videohero
 * * Block Title: Hero Video
 * * Block Description: Hero Video Block
 * * Block Icon: format-video
 * * Block Keywords: video hero, video header
 * * Post Types: page
 */

// Basic Settings
$id 		= 'video__hero__header';
$class		= !empty($block['className']) ? $block['className'] : '';

// Advanced Settings
$video_hero_title	   = trim(get_field('video_hero_title')) ?: '';
$video_hero_text	   = trim(get_field('video_hero_text')) ?: '';
$video_hero_video_mp4  = get_field('video_hero_video_mp4') ?: '';
$video_hero_button     = get_field('video_hero_button') ?: '';
$video_hero_button_url = '';

if ($video_hero_button) {
	$video_hero_button_title  = $video_hero_button['title'] ?: 'Discover more';
	$video_hero_button_url 	  = $video_hero_button['url'] ?: '';
	$video_hero_button_target = $video_hero_button['target'];
}

?>

<!-- Video Hero Header -->
<section id="<?= esc_attr($id) ?>" class="<?= esc_attr($class) ?> <?= $video_hero_button_url ? 'extra' : '' ?>">
	<?php if ($video_hero_video_mp4) : ?>
		<?php if (!wp_is_mobile()) : ?>
			<video playsinline preload autoplay muted loop muted>
				<source type="video/mp4" src="<?= $video_hero_video_mp4 ?>">
			</video>
		<?php else : ?>
			<img src="<?= THEMEPATH . '/assets/img/hero_video_bg_2.jpg' ?>" alt="<?= esc_html__('Great companies deserve exceptional customer service', 'bs') ?>" class="video__hero__bg" width="1024" height="640">
		<?php endif; ?>
	<?php endif; ?>
	<div class="inner">
		<header class="video__hero__header">
			<?php if ($video_hero_title) : ?>
				<h1 class="video__hero__title" data-splitting><?= br2span($video_hero_title, 'video__hero__split') ?></h1>
			<?php endif; ?>
			<?php if ($video_hero_text) : ?>
				<p class="video__hero__subtitle"><?= $video_hero_text ?></p>
			<?php endif; ?>
			<?php if ($video_hero_button_url) : ?>
				<div class="video__hero__button">
					<a href="<?= esc_url($video_hero_button_url) ?>" target="<?= $video_hero_button_target ?>" class="button green"><?= $video_hero_button_title ?></a>
				</div>
			<?php endif; ?>
		</header>
	</div>
</section>