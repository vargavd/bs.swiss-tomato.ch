<?php

/**
 * * Block Name: features
 * * Block Title: Features
 * * Block Description: Features Block
 * * Block Icon: star-empty
 * * Block Multiple: true
 * * Block Keywords: features, cols
 * * Post Types: page
 */

// Basic Settings
$id 		= !empty($block['anchor']) ? $block['anchor'] : '';
$id_comment = ucwords(preg_replace('!\s+!', ' ', str_replace('_', ' ', $id))) ?: 'Features';
$class		= !empty($block['className']) ? 'features ' . $block['className'] : 'features';

// Advanced Settings
$features_title	= trim(get_field('features_title')) ?: '';
$cols			= (int)get_field('features_size') ?: 1;
$features		= get_field('features') ?: '';
$random_str		= random_str(10);
?>

<?php if ($id_comment) : ?>
	<!-- <?= $id_comment ?> -->
<?php endif; ?>
<section <?= $id ? 'id="' . esc_attr($id) . '"' : '' ?> class="<?= esc_attr($class) ?><?= !$features_title ? ' no__title' : '' ?>">

	<div class="inner">

		<?php if ($features_title) : ?>
			<header class="features__header">
				<h2 class="features__title small__title"><?= $features_title ?></h2>
			</header>
		<?php endif; ?>

		<div class="grid features__grid col__<?= $cols ?>">

			<?php

			// ? Has Feature Row
			if (trim(have_rows('features'))) :

				$i = 0;
				$x = 0;
				// * Loop Features
				while (have_rows('features')) : the_row();
					$x++;

					// * Loop Features by Columns
					foreach (range(1, $cols) as $i) :

						$feature = get_sub_field('feature_' . $i) ?: '';

						if ($feature) {

							// Vars
							$feature_background = $feature['feature_background_' . $i] ?: '';
							$img_size 			= $cols < 3 ? 'medium_feature' : 'medium';
							$feature_bg_url 	= $feature['feature_background_' . $i] ? wp_get_attachment_image_src($feature['feature_background_' . $i], 'full')[0] : '';
							$feature_title 		= trim($feature['feature_title_' . $i]) ?: '';
							$feature_text  		= trim($feature['feature_text_' . $i]) ?: '';
							$feature_url  		= $feature['feature_url_' . $i];
							$feature_url_url 	= '';

							if ($feature_url) {
								$feature_url_title  = trim($feature_url['title']) ?: esc_html__('Show more', 'bs');
								$feature_url_url 	= trim($feature_url['url']) ?: '';
								$feature_url_target = trim($feature_url['target']) ?: '_self';
							}

							// Clean text from empty break lines
							$cleaned_feature_text = remove_empty_p($feature_text);
							// Count break lines
							$break_lines = (int) substr_count($cleaned_feature_text, "\n");
							// Strip ot html tags
							$stripped_feature_text = strip_tags($cleaned_feature_text);
							// Count characters
							$character_count = (int) mb_strlen($stripped_feature_text, "UTF-8") - $break_lines;
							// Start with ul?
							$start_with_ul = str_starts_with($cleaned_feature_text, '<ul>');
							// Allowed rows 
							$allowed_rows = $start_with_ul ? 6 : 5;
							$allowed_characters = 200;
							// Show More Rules
							$showmore_rules = ($break_lines > $allowed_rows || $character_count > $allowed_characters);

							// Test Rules
							//pr1($break_lines);
							//pr1($character_count);
							//pr1($start_with_ul);
						}
			?>

						<div class="feature__item <?= !$feature_title && !$feature_text && !$feature_background ? 'empty' : '' ?>" data-sal="slide-up" data-sal-duration="800" data-sal-delay="<?= $i ?>00" data-sal-easing="ease-out-cubic">

							<?php if ($feature_title && $feature_text && $feature_background) : ?>
								<?= custom_image_size($feature_bg_url, $img_size, $feature_title, 'feature__img') ?>
								<?php if ($showmore_rules) : ?>
									<!-- Show More -->
									<a href="#feature__item__<?= $x . $i . $random_str ?>" class="absolute" data-lity><span class="screen-reader-text"><?= $feature_title ?></span></a>
								<?php elseif ($feature_url_url && !$showmore_rules) : ?>
									<!-- Real Link -->
									<a href="<?= esc_url($feature_url_url) ?>" target="<?= $feature_url_target ?>" class="absolute"><span class="screen-reader-text"><?= $feature_title ?></span></a>
								<?php endif; ?>
								<div class="feature__container">
									<?php if ($feature_title) : ?>
										<!-- Title -->
										<h2 class="feature__title"><?= $feature_title ?></h2>
									<?php endif; ?>
									<?php if ($feature_text) : ?>
										<!-- Text -->
										<div class="feature__text <?= !$showmore_rules && !$feature_url_url ? 'mb__45' : '' ?>"><?= $character_count > $allowed_characters ? truncate($feature_text, $allowed_characters, '...', true, true) : $feature_text ?></div>
									<?php endif; ?>
									<?php if ($showmore_rules) : ?>
										<!-- Show More -->
										<a href="#feature__item__<?= $x . $i . $random_str ?>" class="feature__url read__more" data-lity><?= esc_html__('Show more', 'bs') ?></a>
									<?php endif; ?>
									<?php if ($feature_url_url && !$showmore_rules) : ?>
										<!-- Real Link -->
										<a href="<?= esc_url($feature_url_url) ?>" target="<?= $feature_url_target ?>" class="feature__url read__more"><?= $feature_url_title ?></a>
									<?php endif; ?>
								</div>

								<?php if ($showmore_rules) : ?>
									<!-- Image -->
									<div id="feature__item__<?= $x . $i . $random_str ?>" class="feature__popup lity-hide">
										<?php if ($feature_background) : ?>
											<?= custom_image_size($feature_background, 'medium_large', $feature_title ?: esc_html__('Feature without title' . 'bs'), 'feature__popup__background') ?>
										<?php endif; ?>
										<?php if ($feature_title) : ?>
											<!-- Title -->
											<h2 class="feature__popup__title"><?= $feature_title ?></h2>
										<?php endif; ?>
										<?php if ($feature_text) : ?>
											<!-- Text -->
											<div class="feature__popup__text">
												<?= $feature_text ?>

												<?php if ($feature_url_url) : ?>
													<!-- Show more -->
													<a href="<?= esc_url($feature_url_url) ?>" target="<?= $feature_url_target ?>" class="feature__url button"><?= $feature_url_title ?></a>
												<?php endif; ?>
											</div>
										<?php endif; ?>
									</div>
								<?php endif; ?>
							<?php endif; ?>
						</div>

			<?php
					// * End Loops
					endforeach;
				endwhile;
			endif;
			?>

		</div>

</section>