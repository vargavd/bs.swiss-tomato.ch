<?php

/**
 * * Block Name: gadgets
 * * Block Title: Gadgets
 * * Block Description: Gadgets Block
 * * Block Icon: slides
 * * Block Multiple: true
 * * Block Script: true
 * * Block Keywords: gadgets, gadget, gallery
 * * Post Types: page
 */

// Basic Settings
$id	   = !empty($block['anchor']) ? $block['anchor'] : '';
$class = !empty($block['className']) ? 'gadgets ' . $block['className'] : 'gadgets';

// Advanced Settings
$gadgets_title	    = trim(get_field('gadgets_title')) ?: '';
$gadgets_background = get_field('gadgets_background') ?: 'gray';
$gadgets_big_title  = trim(get_field('gadgets_big_title')) ?: '';
$count 				= count(get_field('gadgets_images')) > 2 ? 'small' : '';

if (trim(have_rows('gadgets_images'))) : ?>

	<!-- Gadgets -->
	<section <?= $id ? 'id="' . esc_attr($id) . '"' : '' ?> class="<?= esc_attr($class) . ' ' . $gadgets_background ?>">

		<div class="inner wide">

			<?php if ($gadgets_title) : ?>
				<header class="gadgets__header inner narrow">
					<h2 class="gadgets__title small__title"><?= $gadgets_title ?></h2>
				</header>
			<?php endif; ?>

			<div class="swiper-container gadgets__container">

				<div class="swiper-wrapper gadgets__wrapper">

					<?php

					$i = 0;
					// Loop gadgets
					while (trim(have_rows('gadgets_images'))) : the_row();
						$i++;

						// Vars
						$gadget_image   = (int) get_sub_field('gadget_image') ?: '';
						$gadget_lg_url  = $gadget_image ? wp_get_attachment_image_src(get_sub_field('gadget_image'), 'full')[0] : '';
						$gadget_caption = trim(get_sub_field('gadget_caption')) ?: '';

						if ($gadget_image) : ?>
							<div class="swiper-slide gadgets__slide <?= $count ?>" data-src="<?= $gadget_lg_url ?>" <?= $gadget_caption ? ' data-caption="' . $gadget_caption . '"' : '' ?>>
								<?php if ($gadget_image) : ?>
									<!-- Gadget Image -->
									<div class="gadget__image <?= $gadget_caption ? 'has__caption' : '' ?>" <?= $gadget_caption ? ' data-caption="' . $gadget_caption . '"' : '' ?>>
										<?= custom_image_size($gadget_image, count(get_field('gadgets_images')) > 2 ? 'medium_feature' : 'large', $gadget_caption ?: '', '', '', 'none')
										?>
									</div>
								<?php endif; ?>
							</div>
						<?php endif; ?>

					<?php endwhile; ?>

				</div>

			</div>

			<?php if ($gadgets_big_title) : ?>
				<header class="gadgets__big__header inner narrow">
					<h2 class="gadgets__big__title h1"><?= $gadgets_big_title ?></h2>
				</header>
			<?php endif; ?>

		</div>

	</section>

<?php

endif;

// EOF