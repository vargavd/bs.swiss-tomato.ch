<?php

/**
 * * Block Name: resources-list
 * * Block Title: Resources List
 * * Block Description: Resources List Block
 * * Block Icon: admin-post
 * * Block Keywords: resources list
 * * Post Types: page
 */

// Basic Settings
$id 		= !empty($block['anchor']) ? $block['anchor'] : 'resources';
$class		= !empty($block['className']) ? $block['className'] : '';
$align		= $block['align'] !== 'center' ? ' ' . $block['align'] : '';
$id_comment = str_replace('_', ' ', $id);
$id_comment = ucwords(preg_replace('!\s+!', ' ', $id_comment));

// Advanced Settings
$post_type 		= 'resources';
$resources_title = trim(get_field('resource_title')) ?: esc_html__('Results', 'bs');
$ppp			= (int) get_field('resource_limit') ?: (int) 6;

// Get resource_type by $_GET['resource_type] value
$args = [];
$resource_type_url_param = isset($_GET['resource_type']) ? $_GET['resource_type'] : '';
if ($resource_type_url_param) {
	$args = [
		[
			'taxonomy' => 'resource-type',
			'field'	   => 'term_id',
			'terms'    => $resource_type_url_param,
			'operator' => 'IN',
		]
	];
}

// Query 
$posts = new WP_Query([
	'post_type'		 => $post_type,
	'post_status' 	 => 'publish',
	'posts_per_page' => $ppp,
	'order' 		 => 'DESC',
	'tax_query'		 => $args,
]);


if ($posts->have_posts()) : ?>

	<?php if ($id_comment) : ?>
		<!-- <?= $id_comment ?> -->
	<?php endif; ?>
	<section id="<?= $id ?>" class="<?= $class ?>">

		<div class="filter__area">

			<div class="inner narrow">

				<!-- Filter Form -->
				<form action="<?= admin_url('admin-ajax.php') ?>" method="POST" id="<?= $post_type ?>__filter" class="<?= $post_type ?>__filter">

					<!-- Hidden fields -->
					<input type="hidden" name="action" value="ajaxfilter">
					<input type="hidden" name="cpt" id="cpt" value="<?= $post_type ?>">
					<input type="hidden" name="ppp" id="ppp" value="<?= (int) $ppp ?>">
					<input type="hidden" name="max" id="max" value="<?= (int) $posts->max_num_pages ?>">

					<div class="<?= $post_type ?>__filter__items grid col__3 gap__2">

						<?php

						// Product
						$products = get_terms('product', [
							'hide_empty' => false,
							'orderby'	 => 'id',
						]);

						if (!empty($products) && taxonomy_exists('product')) : ?>
							<div class="resources__filter__item">
								<small class="red"><?= esc_html__('Product', 'bs') ?></small>
								<div class="select__custom select__product placeholder form__container" data-placeholder="<?= esc_html__('Product', 'bs') ?>">
									<select name="product" id="product" class="select">
										<option value="" selected><?= esc_html__('Product', 'bs') ?></option>
										<option value=""><?= esc_html__('Product', 'bs') ?></option>
										<?php foreach ($products as $product) : ?>
											<option value="<?= (int) $product->term_id; ?>"><?= $product->name ?></option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
						<?php endif; ?>

						<?php

						// Resource Type
						$resource_types = get_terms('resource-type', [
							'hide_empty' => false,
							'orderby'	 => 'id',
							'order'		 => 'DESC'
						]);

						if (!empty($resource_types) && taxonomy_exists('resource-type')) : ?>
							<div class="resources__filter__item">
								<small class="red"><?= esc_html__('Resource type', 'bs') ?></small>
								<div class="select__custom select__resource__type placeholder form__container" data-placeholder="<?= esc_html__('Resource type', 'bs') ?>">
									<select name="resource__type" id="resource__type" class="select">
                    <?php if (!$resource_type_url_param): ?>
										  <option value="" selected><?= esc_html__('Resource type', 'bs') ?></option>
                    <?php endif; ?>


										<option value=""><?= esc_html__('Resource type', 'bs') ?></option>
										<?php foreach ($resource_types as $resource_type) : ?>
											<option 
                        value="<?= (int) $resource_type->term_id; ?>"
                        <?=($resource_type_url_param == $resource_type->term_id) ? ' selected' : ''?>
                      >
                        <?= $resource_type->name ?>
                      </option>
										<?php endforeach; ?>
									</select>
								</div>
							</div>
						<?php endif; ?>

						<!-- Search -->
						<div class="resources__filter__item">
							<small class="red"><?= esc_html__('Search', 'bs') ?></small>
							<div class="resources__search__container" data-info="<?= esc_html__('Enter at least 3 characters', 'bs') ?>">
								<input type="search" id="<?= $post_type ?>__search" name="<?= $post_type ?>__search" class="<?= $post_type ?>__search" placeholder="<?= esc_html__('Keyword', 'bs') ?>">
							</div>

							<!-- Reset -->
							<button type="reset" class="<?= $post_type ?>__reset"><?= esc_html__('Reset', 'bs') ?></button>
						</div>

					</div>

			</div>

			</form>
		</div>



		<section class="inner narrow">

			<header class="resources__header">
				<h2 class="resources__filter__title small__title"><?= $resources_title ?></h2>
			</header>

			<div id="<?= $post_type ?>__result">

				<div class="grid col__2 gap__2 <?= $post_type ?>__container">

					<?php

					// Loop
					while ($posts->have_posts()) : $posts->the_post();
						$post_id = get_the_ID() ?>
						<?php get_template_part('parts/components/filter', NULL, ['post_id' => $post_id, 'post_type' => $post_type]) ?>
					<?php endwhile;
					wp_reset_postdata(); ?>

				</div>

				<?php if ($posts->found_posts > $ppp) : ?>
					<div class="loadmore text-center">
						<button class="load__more resources__loader button border"><?= esc_html__('Load more', 'bs') ?></button>
					</div>
				<?php endif; ?>

			</div>

		</section>

	</section>

<?php

endif;
