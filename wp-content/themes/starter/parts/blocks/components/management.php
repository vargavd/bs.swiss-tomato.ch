<?php

/**
 * * Block Name: management
 * * Block Title: Management
 * * Block Description: Management Block
 * * Block Icon: businessman
 * * Block Keywords: management, member, team
 * * Post Types: page
 */

// Basic Settings
$id 		= !empty($block['anchor']) ? $block['anchor'] : 'management';
$id_comment = ucwords(preg_replace('!\s+!', ' ', str_replace('_', ' ', $id)));
$class		= !empty($block['className']) ? 'management ' . $block['className'] : 'management';

// Advanced Settings
$management_title  	= trim(get_field('management_title')) ?: esc_html__('Management', 'bs');
$random_str			= random_str(10);

// ? Has Management Row
if (trim(have_rows('management_members'))) : ?>

	<?php if ($id_comment) : ?>
		<!-- <?= $id_comment ?> -->
	<?php endif; ?>
	<section id="<?= esc_attr($id) ?>" class="<?= esc_attr($id) ?>">

		<div class="inner narrow">

			<header class="management__header">
				<h2 class="management__title small__title"><?= $management_title ?></h2>
			</header>

			<div class="management__grid grid col__3 gap__2">

				<?php

				$x = 0;
				// * Loop Management Members
				while (have_rows('management_members')) : the_row();
					$x++;

					// Vars
					$management_members_img  = get_sub_field('management_member_img') ?: '';
					$management_members_name = trim(get_sub_field('management_member_name')) ?: '';
					$management_member_rank  = trim(get_sub_field('management_member_rank')) ?: '';
					$management_member_bio	 = trim(get_sub_field('management_member_bio')) ?: ''; ?>

					<div class="management__member">
						<?php if ($management_members_img) : ?>
							<div class="management__member__image">
								<a href="#management__item__<?= $x . $random_str ?>" class="management__url absolute" data-lity><span class="screen-reader-text"><?= $management_members_name ?: esc_html__('Member without name', 'bs') ?></span></a>
								<!-- Member Image -->
								<?= custom_image_size($management_members_img, 'management', $management_members_name ?: esc_html__('Member without name', 'bs'), 'management__member__img') ?>
							</div>
						<?php endif; ?>
						<div class="management__member__content">
							<?php if ($management_members_name) : ?>
								<!-- Member Name -->
								<h3 class="management__member__name"><?= $management_members_name ?></h3>
							<?php endif; ?>
							<?php if ($management_member_rank) : ?>
								<!-- Member Rank -->
								<p class="management__member__rank"><?= $management_member_rank ?></p>
							<?php endif; ?>
						</div>
						<?php if ($management_members_img) : ?>
							<div id="management__item__<?= $x . $random_str ?>" class="management__member__content popup lity-hide">
								<div class="management__member__grid grid column gap__3">
									<?= custom_image_size($management_members_img, 'full', $management_members_name ?: esc_html__('Member without name', 'bs'), 'management__member__img', '', 'none') ?>
									<div class="management__member__text">
										<?php if ($management_members_name) : ?>
											<!-- Member Name -->
											<h3 class="management__member__name"><?= $management_members_name ?></h3>
										<?php endif; ?>
										<?php if ($management_member_rank) : ?>
											<!-- Member Rank -->
											<p class="management__member__rank"><?= $management_member_rank ?></p>
										<?php endif; ?>
										<?php if ($management_member_bio) : ?>
											<!-- Member Rank -->
											<small class="management__member__bio"><?= $management_member_bio ?></small>
										<?php endif; ?>
									</div>
								</div>
							</div>
						<?php endif; ?>
					</div>
				<?php

				// * Loop end
				endwhile; ?>
			</div>

		</div>

	</section>

<?php

endif;

// EOF