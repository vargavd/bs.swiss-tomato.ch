<?php

/**
 * * Block Name: job
 * * Block Title: Job
 * * Block Description: Job Block
 * * Block Icon: portfolio
 * * Block Keywords: job, carrier
 * * Post Types: jobs
 */

// Basic Settings
$id 		= 'job';
$id_comment = ucwords(preg_replace('!\s+!', ' ', str_replace('_', ' ', $id)));
$class		= !empty($block['className']) ? 'job ' . $block['className'] : 'job';

// Advanced Settings
$job_location = trim(get_field('job_location')) ?: '';
$job_type 	  = trim(get_field('job_type')) ?: '';
$job_apply 	  = trim(get_field('job_apply')) ?: '';

// Job Environment
$job_environment_title = trim(get_field('job_environment_title')) ?: '';
$job_environment_text  = trim(get_field('job_environment_text')) ?: '';

// Job Main Tasks
$job_profile_title = trim(get_field('job_profile_title')) ?: '';
$job_profile_text  = trim(get_field('job_profile_text')) ?: '';

// Job Interested
$job_interested_title = trim(get_field('job_interested_title')) ?: '';
$job_interested_content = get_field('job_interested_content') ?: '';

$job_send_text 			 = '';
$job_question_text 		 = '';
$job_contact_person_img  = '';
$job_contact_person_name = '';
$job_contact_person_rank = '';
$job_data_text 			 = '';

if ($job_interested_content) {
	$job_send_text 			 = trim($job_interested_content['job_send_text']) ?: '';
	$job_question_text 		 = trim($job_interested_content['job_question_text']) ?: '';
	$job_contact_person_img  = $job_interested_content['contact_person_img'] ?: '';
	$job_contact_person_name = trim($job_interested_content['job_contact_person_name']) ?: '';
	$job_contact_person_rank = trim($job_interested_content['job_contact_person_rank']) ?: '';
	$job_data_text 			 = trim($job_interested_content['job_data_text']) ?: '';
}

?>

<?php if ($id_comment) : ?>
	<!-- <?= $id_comment ?> -->
<?php endif; ?>
<section <?= $id ? 'id="' . esc_attr($id) . '"' : '' ?> class="<?= esc_attr($class) ?>">

	<div class="inner">
		<?php /* if ($job_title) : ?>
			<header class="trusted__by__header text__center">
				<h1 class="trusted__by__title"><?= $job_title ?></h1>
			</header>
		<?php endif; */ ?>
		<?php get_the_title() ?>

	</div>

</section>