<?php

/**
 * * Block Name: gallery
 * * Block Title: Gallery
 * * Block Description: Gallery Block
 * * Block Icon: format-gallery
 * * Block Multiple: true
 * * Block Keywords: gallery, products, shop
 * * Post Types: page
 */

// Basic Settings
$id 		= !empty($block['anchor']) ? $block['anchor'] : 'gallery-block';
$id_comment = ucwords(preg_replace('!\s+!', ' ', str_replace('_', ' ', $id)));
$class		= !empty($block['className']) ? 'gallery-block ' . $block['className'] : 'gallery-block';

// Advanced Settings

$gallery_title = get_field('gallery_title') ?: '';
$gallery_text  = get_field('gallery_text') ?: ''; ?>

<?php if ($id_comment) : ?>
	<!-- <?= $id_comment ?> -->
<?php endif; ?>
<section <?= $id ? 'id="' . esc_attr($id) . '"' : '' ?> class="<?= esc_attr($class) ?>">
	<div class="inner narrow">

		<?php if ($gallery_title) : ?>
			<h2 class="gallery__title small__title"><?= $gallery_title ?></h2>
		<?php endif; ?>
		<?php if ($gallery_text) : ?>
			<p class="gallery__text"><?= $gallery_text ?></p>
		<?php endif; ?>

		<?php

		// ? Has Galleries
		if (trim(have_rows('galleries'))) : ?>
			<div class="grid gallery__grid col__<?= $cols ?>">
				<?php

				// * Loop Galleries
				$i = 0;
				while (have_rows('galleries')) : the_row();

					// * Loop Gallery Items
					$i++;
					while (have_rows('gallery_items')) : the_row();

						$gallery_category_title = get_sub_field('gallery_category_title') ?: '';

						// ? Has Gallery Products
						if (trim(have_rows('gallery_products'))) :

							if ($gallery_category_title) : ?>
								<h2 class="gallery__title small__title"><?= $gallery_category_title ?></h2>
							<?php endif; ?>

							<div class="grid gallery__product__grid col__3 gap__3">

								<?php

								// * Loop Gallery Products
								$x = 0;
								while (have_rows('gallery_products')) : the_row();
									$x++;

									$random_str = random_str(10);

									// Vars
									$gallery_product_image  = get_sub_field('gallery_product_image') ?: '';
									$gallery_product_title  = get_sub_field('gallery_product_title') ?: '';
									$gallery_product_short  = get_sub_field('gallery_product_short_description') ?: '';
									$gallery_product_serial = get_sub_field('gallery_product_serial_number') ?: '';
									// Later
									$gallery_product_full   = get_sub_field('gallery_product_full_description') ?: '';
									$gallery_product_link   = get_sub_field('gallery_product_link') ?: '';

									$gallery_product_url    = '';

									if ($gallery_product_link) {
										$gallery_product_url 	   = $gallery_product_link['url'] ?: '';
										$gallery_product_url_title = $gallery_product_link['title'] ?: esc_html__('Link to the product', 'bs');
									}

									if ($gallery_product_title) :

								?>

										<div class="gallery__product">
											<a href="#gallery__item__<?= $i . $x . $random_str ?>" class="absolute" data-lity><span class="screen-reader-text"><?= $gallery_product_title ?></span></a>
											<?php if ($gallery_product_image) : ?>
												<?= custom_image_size($gallery_product_image, 'management', $gallery_product_title, 'gallery__product__image') ?>
											<?php endif; ?>
											<h3 class="gallery__product__title"><?= $gallery_product_title ?></h3>
											<?php if ($gallery_product_short) : ?>
												<p class="gallery__product__text"><?= $gallery_product_short ?></p>
											<?php endif; ?>
											<?php if ($gallery_product_serial) : ?>
												<p class="gallery__product__serial"><?= $gallery_product_serial ?></p>
											<?php endif; ?>

										</div>

										<!-- Image -->
										<div id="gallery__item__<?= $i . $x . $random_str ?>" class="gallery__popup lity-hide">
											<?php if ($gallery_product_image) : ?>
												<?= custom_image_size($gallery_product_image, 'full', $gallery_product_title, 'gallery__popup__image') ?>
											<?php endif; ?>
											<?php if ($gallery_product_title) : ?>
												<!-- Title -->
												<h2 class="gallery__popup__title"><?= $gallery_product_title ?></h2>
											<?php endif; ?>
											<?php if ($gallery_product_serial) : ?>
												<p class="gallery__product__serial"><?= $gallery_product_serial ?></p>
											<?php endif; ?>
											<?php if ($gallery_product_full) : ?>
												<!-- Text -->
												<p class="gallery__popup__text"><?= $gallery_product_full ?></p>
											<?php endif; ?>
											<?php if ($gallery_product_url) : ?>
												<a href="<?= $gallery_product_url ?>" class="gallery__popup__url" target="_blank"><?= $gallery_product_url_title ?></a>
											<?php endif; ?>
										</div>

									<?php endif; ?>

								<?php endwhile; ?>

							</div>

						<?php endif; ?>

					<?php endwhile; ?>

				<?php

				endwhile;
				?>
			</div>
		<?php endif; ?>

	</div>
</section>


<?php

// EOF