<?php

/**
 * * Block Name: faq
 * * Block Title: Faq
 * * Block Description: Faq Block
 * * Block Icon: editor-justify
 * * Block Multiple: true
 * * Block Script: true
 * * Block Keywords: faq, questions, question, answer, answers, frequently
 * * Post Types: post, page
 */

// Basic Settings
$id 		= !empty($block['anchor']) ? $block['anchor'] : 'faq-block';
$id_comment = ucwords(preg_replace('!\s+!', ' ', str_replace('_', ' ', $id)));
$class		= !empty($block['className']) ? 'faq-block ' . $block['className'] : 'faq-block';

$faq_label = get_field('faq_label');
$questions = get_field('questions');
$background_color = strtolower(get_field('background_color'));
$padding_top = get_field('padding_top');
$padding_bottom = get_field('padding_bottom');

$class .= " $background_color-bc"; 
$style = "padding-top: ${padding_top}px; padding-bottom: ${padding_bottom}px;"

?>

	<?php if ($id_comment) : ?>
		<!-- <?= $id_comment ?> -->
	<?php endif; ?>
	<div 
    <?=$id ? 'id="' . esc_attr($id) . '"' : ''?> 
    class="<?=esc_attr($class)?>"
    style="<?=$style?>"
  >
    <div class="inner narrow">
      <h1><?=$faq_label?></h1>
  
      <div class="faq-list">
        <?php foreach ($questions as $faqItem): ?>
          <div class="faq-item">
            <div class="question-row">
              <h2><?=$faqItem['question']?></h2>
              
              <span data-icon="" class="faq-chevron"></span>
            </div>
            <div class="answer">
              <?=$faqItem['answer']?>
            </div>
          </div>
        <?php endforeach; ?>
      </div>
    </div>
	</div>

<?php

// EOF