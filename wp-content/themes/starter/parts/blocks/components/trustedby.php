<?php

/**
 * * Block Name: trustedby
 * * Block Title: Trusted by
 * * Block Description: Trusted by Block
 * * Block Multiple: true
 * * Block Script: true
 * * Block Icon: superhero-alt
 * * Block Keywords: logos, trusted
 * * Post Types: page
 */

// Basic Settings
$id 		= 'trusted__by';
$id_comment = ucwords(preg_replace('!\s+!', ' ', str_replace('_', ' ', $id)));
$class		= !empty($block['className']) ? 'trusted__by ' . $block['className'] : 'trusted__by';

// Advanced Settings
$trusted_by_title = trim(get_field('trustedby_title')) ?: '';
$trusted_by_logos = get_field('trustedby_logos') ?: '';

?>

<?php if ($id_comment) : ?>
	<!-- <?= $id_comment ?> -->
<?php endif; ?>
<section class="<?= esc_attr($class) ?>">

	<div class="inner">
		<?php if ($trusted_by_title) : ?>
			<header class="trusted__by__header text__center">
				<h2 class="trusted__by__title"><?= $trusted_by_title ?></h2>
			</header>
		<?php endif; ?>

		<?php if (trim(have_rows('trusted_by_logos'))) : ?>
			<div class="grid col__6 center trusted__by__grid" data-sal>
				<?php

				$i = 0;
				// Loop
				while (trim(have_rows('trusted_by_logos'))) : the_row();
					$i++;

					$trusted_by_logo = get_sub_field('trusted_by_logo') ?: '';
					$trusted_by_link = get_sub_field('trusted_by_link') ?: '';
					$trusted_by_link_url = '';
					$trusted_by_link_title = '';
					$trusted_by_link_target = '';

					if ($trusted_by_link) {
						$trusted_by_link_url = trim($trusted_by_link['url']) ?: '';
						$trusted_by_link_title = trim($trusted_by_link['title']) ?: esc_html__('Logo ' . $i . ' description', 'bs');
						$trusted_by_link_target = trim($trusted_by_link['target']) ?: '_blank';
					}

				?>
					<?php if ($trusted_by_logo) : ?>
						<div class="trusted__by__item <?= $i > 6 ? 'hidden' : 'active' ?>" data-sal="slide-up" data-sal-duration="800" data-sal-delay="<?= $i ?>00" data-sal-easing="ease-out-cubic">
							<?php if ($trusted_by_link_url) : ?>
								<a href="<?= esc_url($trusted_by_link_url) ?>" target="<?= $trusted_by_link_target ?>"><span class="screen-reader-text"><?= $trusted_by_link_title ?></span>
								<?php endif; ?>
								<?= custom_image_size($trusted_by_logo, 'medium', $trusted_by_link_title, 'trusted__by__logo', '', 'none') ?>
								<?php if ($trusted_by_link_url) : ?>
								</a>
							<?php endif; ?>
						</div>
					<?php endif; ?>

				<?php endwhile; ?>
			</div>
		<?php endif; ?>
	</div>

</section>