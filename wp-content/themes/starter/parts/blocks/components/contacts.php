<?php

/**
 * * Block Name: contacts
 * * Block Title: Contacts
 * * Block Description: Contacts Block
 * * Block Icon: phone
 * * Block Multiple: true
 * * Block Keywords: contacts, email, phone
 * * Post Types: page
 */

// Basic Settings
$id 		= !empty($block['anchor']) ? $block['anchor'] : 'contacts';
$class		= !empty($block['className']) ? 'contacts ' . $block['className'] : 'contacts';

// Advanced Settings
$contacts_background = get_field('contacts_background') ?: 'gray';

// ? Has contacts Row
if (trim(have_rows('contacts_items'))) : ?>

	<!-- Contacts -->
	<section id="<?= esc_attr($id) ?>" class="<?= esc_attr($class) . ' ' . $contacts_background ?>">

		<div class="inner narrow">

			<ul class="contacts__items">

				<?php

				$x = 0;
				// * Loop Contacts Items
				while (have_rows('contacts_items')) : the_row();
					$x++;

					// Vars
					$contacts_item  = get_sub_field('contacts_item') ?: '';
					$contact_url 	= '';
					$contact_title  = '';
					$contact_target = '';

					if ($contacts_item) {
						$contacts_url 	= $contacts_item['url'] ?: '';
						$contacts_title  = $contacts_item['title'] ?: '';
						$contacts_target = $contacts_item['target'] ?: '_self';
					}
				?>

					<?php if ($contacts_url && $contacts_title) : ?>
						<li class="contacts__item"><a href="<?= esc_url($contacts_url) ?>" target="<?= $contacts_target ?>" class="contacts__url read__more dark"><?= $contacts_title ?></a></li>
					<?php endif; ?>
				<?php

				// * Loop end
				endwhile; ?>
			</ul>

		</div>

	</section>

<?php

endif;

// EOF