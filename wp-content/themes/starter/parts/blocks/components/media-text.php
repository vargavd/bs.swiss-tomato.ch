<?php

/**
 * * Block Name: media-text
 * * Block Title: Media Text
 * * Block Description: Media Text Block
 * * Block Icon: embed-photo
 * * Block Multiple: true
 * * Block Script: true
 * * Block Keywords: media, image, text, lightbox, popup
 * * Post Types: post, page
 */

// Basic Settings
$id 		= !empty($block['anchor']) ? $block['anchor'] : '';
$id_comment = ucwords(preg_replace('!\s+!', ' ', str_replace('_', ' ', $id)));
$class		= !empty($block['className']) ? 'media-text ' . $block['className'] : 'media-text';


$gray_bc = get_field('gray_background');
$header_title = get_field('header_title');
$rows = get_field('rows');

if ($gray_bc) {
  $class .= ' gray-bc';
}

?>

	<?php if ($id_comment) : ?>
		<!-- <?= $id_comment ?> -->
	<?php endif; ?>
	<section 
    <?=$id ? 'id="' . esc_attr($id) . '"' : ''?> 
    class="<?=esc_attr($class)?>"
  >
    <div class="inner">
      <?php if ($header_title) : ?>
        <header class="media-text__header">
          <h2 class="media-text__title small__title">
            <?=$header_title?>
          </h2>
        </header>
      <?php endif; ?>
  
      <div class="rows">
        <?php 
          foreach ($rows as $row): 
            $order = $row['order'];

            // CONTENT
            $content_group = $row['content'];
            $title = $content_group['title'];
            $text = $content_group['text'];
            $link = $content_group['link'];
            
            // MEDIA
            $media_group = $row['media'];
            $media_type = $media_group['media_type'];
            if ($media_type === 'Image') {
              $image = isset($media_group['image']) ?  $media_group['image'] : '';
              $target = $image;
            } elseif ($media_type === 'Video') {
              $image = isset($media_group['video_thumbnail']) ?  $media_group['video_thumbnail'] : '';
              $target = isset($media_group['video']) ?  $media_group['video'] : '';
            }
        ?>
          <div class="row <?=$order?>"> <!-- order: media-text or text-media -->
            <div class="row__media <?=strtolower($media_type)?>" data-lity data-lity-target="<?=$target?>">
              <img src="<?=$image?>" alt="<?=$title?>" loading="lazy">
            </div>

            <div class="row__text">
              <h3><?=$title?></h3>

              <?=$text?>

              <?php if (!empty($link)): ?>
                <a href="<?=$link['url']?>" target="<?=$link['target']?>" class="button">
                  <?=$link['title']?>
                </a>
              <?php endif; ?>
            </div>  
          </div>
        <?php endforeach; ?>
      </div>
    </div>
	</section>

<?php

// EOF