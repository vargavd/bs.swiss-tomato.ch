<?php

/**
 * * Block Name: jobs
 * * Block Title: Jobs
 * * Block Description: Jobs Block
 * * Block Icon: portfolio
 * * Block Keywords: jobs, carriers
 * * Post Types: page
 */

// Basic Settings
$id 		= 'jobs';
$id_comment = ucwords(preg_replace('!\s+!', ' ', str_replace('_', ' ', $id)));
$class		= !empty($block['className']) ? 'jobs ' . $block['className'] : 'jobs';

// Advanced Settings
$jobs_title = trim(get_field('jobs_title')) ?: esc_html__('Current Job Openings', 'bs');


?>

<?php if ($id_comment) : ?>
	<!-- <?= $id_comment ?> -->
<?php endif; ?>
<section <?= $id ? 'id="' . esc_attr($id) . '"' : '' ?> class="<?= esc_attr($class) ?>">

	<div class="inner narrow">

		<header class="jobs__header">
			<h2 class="jobs__title small__title"><?= $jobs_title ?></h2>
		</header>

		<?php

		// Query Jobs
		$jobs = get_posts([
			'suppress_filters' => false,
			'fields'	 	   => 'ids',
			'post_type'	 	   => 'jobs',
			'numberposts'	   => -1,
			'status'	 	   => 'public'
		]);

		if (!$jobs) : ?>

			<?php

			$i = 0;
			// Loop Jobs
			foreach ($jobs as $job_id) : setup_postdata($job_id);
				$i++;

				// Vars
				$title = get_the_title($job_id) ?: '';
				$job_text = wp_trim_words(trim(get_field('job_tasks_text', $job_id)), 18, '') ?: '';
				$job_location = trim(get_field('job_location', $job_id)) ?: '';

			?>
				<div class="job__item" data-sal="slide-up" data-sal-duration="800" data-sal-delay="300" data-sal-easing="ease-out-cubic">
					<a href="<?= the_permalink($job_id) ?>" target="_self" class="absolute"></a>
					<h3 class="job__title"><?= $title ?></h3>
					<?php if ($job_text || $job_location) : ?>
						<div class="grid job__grid">
							<?php if ($job_text) : ?>
								<p class="job__text"><?= $job_text ?></p>
							<?php endif; ?>
							<?php if ($job_location) : ?>
								<div class="job__location">
									<small class="job__location__title"><?= esc_html__('Location', 'bs') ?></small>
									<p class="job__location__text"><?= $job_location ?></p>
								</div>
							<?php endif; ?>
						</div>
					<?php endif; ?>
				</div>

			<?php endforeach;
			wp_reset_postdata(); ?>

		<?php else : ?>

			<div class="job__item" data-sal="slide-up" data-sal-duration="800" data-sal-easing="ease-out-cubic">
				<h3 class="job__title"><?= esc_html__('No open jobs', 'bs') ?></h3> <!--Keine offenen Stellen-->
				<div class="grid job__grid">
					<p class="job__text"><?= esc_html__('We\'re sorry, but there are no open jobs currently.', 'bs') ?></p>
					<div class="job__location">
					</div>
				</div>
			</div>

		<?php endif; ?>

	</div>

</section>