<?php

/**
 * * Block Name: sub-header
 * * Block Title: Sub Header
 * * Block Description: Sub Header Block
 * * Block Icon: heading
 * * Block Multiple: true
 * * Block Keywords: heading, header, subheader, subheading, image
 * * Post Types: post
 */

// Basic Settings
$id 		= !empty($block['anchor']) ? $block['anchor'] : 'sub-header';
$id_comment = ucwords(preg_replace('!\s+!', ' ', str_replace('_', ' ', $id)));
$class		= !empty($block['className']) ? 'sub-header ' . $block['className'] : 'sub-header';

$image = get_field('image');
$subheader = get_field('subheader');
?>

	<?php if ($id_comment) : ?>
		<!-- <?= $id_comment ?> -->
	<?php endif; ?>
	<div <?= $id ? 'id="' . esc_attr($id) . '"' : '' ?> class="<?= esc_attr($class) ?>">

    <img src="<?=$image['url']?>" alt="<?=$subheader?>" />

    <div class="vline"></div>
		
    <h2><?=$subheader?></h2>
	</div>

<?php

// EOF