<?php

/**
 * * Block Name: posts
 * * Block Title: Posts
 * * Block Description: Posts Block
 * * Block Icon: admin-post
 * * Block Keywords: posts, blog
 * * Post Types: page
 */

// Basic Settings
$id 		= !empty($block['anchor']) ? $block['anchor'] : 'posts';
$class		= !empty($block['className']) ? 'posts ' . $block['className'] : 'posts';
$align		= $block['align'] !== 'center' ? ' ' . $block['align'] : '';
$id_comment = str_replace('_', ' ', $id);
$id_comment = ucwords(preg_replace('!\s+!', ' ', $id_comment));

// Advanced Settings
$posts_title = trim(get_field('posts_title')) ?: esc_html__('Recent Posts', 'bs');
$ppp		 = (int) get_field('post_limit') ?: (int) 2;

// Query 
$posts = new WP_Query([
	'post_type'		 => 'post',
	'post_status' 	 => 'publish',
	'posts_per_page' => $ppp,
	'order' 		 => 'DESC',
]);

if ($posts->have_posts()) : ?>

	<?php if ($id_comment) : ?>
		<!-- <?= $id_comment ?> -->
	<?php endif; ?>
	<section id="<?= $id ?>" class="<?= $class ?>">

		<div class="inner narrow">

			<header class="posts__header">
				<h2 class="posts__title small__title"><?= $posts_title ?></h2>
			</header>

			<!-- Filter Form -->
			<form action="<?= admin_url('admin-ajax.php') ?>" method="POST" id="post__filter" class="post__filter">

				<!-- Hidden fields -->
				<input type="hidden" name="action" value="ajaxfilter">
				<input type="hidden" name="cpt" id="cpt" value="post">
				<input type="hidden" name="cat" id="cat" value="">
				<input type="hidden" name="ppp" id="ppp" value="<?= (int) $ppp ?>">
				<input type="hidden" name="max" id="max" value="<?= (int) $posts->max_num_pages ?>">

				<div class="posts__tab">
					<h2 class="posts__tab__item all post__reset active"><?= esc_html__('All', 'bs') ?></h2>
					<?php

					// Check Post Categories
					if (trim(have_rows('post_categories'))) : ?>

						<?php
						// Loop Post Categories
						while (have_rows('post_categories')) : the_row();

							// Get Category
							$category = get_sub_field('post_category') ? get_term_by('id', get_sub_field('post_category'), 'category') : '';

							if ($category) : ?>
								<h2 class="posts__tab__item post__cat <?= $category->slug ?>" data-cat="<?= $category->term_id ?>"><?= $category->name ?></h2>
							<?php endif; ?>

						<?php endwhile; ?>

					<?php endif; ?>
				</div>

			</form>



			<div id="post__result">

				<div class="grid col__2 gap__2 post__container">

					<?php

					// Loop
					while ($posts->have_posts()) : $posts->the_post();
						$post_id = get_the_ID() ?>
						<?php get_template_part('parts/components/filter', NULL, ['post_id' => $post_id, 'post_type' => 'post'])
						?>
					<?php endwhile;
					wp_reset_postdata(); ?>

				</div>

				<?php if ($posts->found_posts > $ppp) : ?>
					<div class="loadmore text-center">
						<button class="load__more post__loader button"><?= esc_html__('Load more', 'bs') ?></button>
					</div>
				<?php endif; ?>

			</div>

		</div>
	</section>

<?php

endif;
