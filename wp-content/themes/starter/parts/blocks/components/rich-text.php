<?php

/**
 * * Block Name: rich-text
 * * Block Title: Rich Text
 * * Block Description: Rich Text Block
 * * Block Icon: text
 * * Block Multiple: true
 * * Block Script: false
 * * Block Keywords: media, image, text, rich
 * * Post Types: post, page
 */

// Basic Settings
$id 		= !empty($block['anchor']) ? $block['anchor'] : '';
$id_comment = ucwords(preg_replace('!\s+!', ' ', str_replace('_', ' ', $id)));
$class		= !empty($block['className']) ? 'rich-text ' . $block['className'] : 'rich-text';


$gray_bc = get_field('gray_background');
$narrow = get_field('narrow');
$header_title = get_field('header_title');
$text = get_field('rich_text_content');

if ($gray_bc) {
  $class .= ' gray-bc';
}
?>

	<?php if ($id_comment) : ?>
		<!-- <?= $id_comment ?> -->
	<?php endif; ?>
	<section 
    <?=$id ? 'id="' . esc_attr($id) . '"' : ''?> 
    class="<?=esc_attr($class)?>"
  >
    <div class="inner<?=$narrow ? ' narrow' : ''?>">
      <?php if ($header_title) : ?>
        <h2 class="rich-text__title small__title">
          <?=$header_title?>
        </h2>
      <?php endif; ?>
  
      <div class="rich-text__text">
        <?=$text; ?>
      </div>
    </div>
	</section>

<?php

// EOF