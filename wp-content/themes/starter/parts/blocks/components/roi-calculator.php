<?php

/**
 * * Block Name: roi-calculator
 * * Block Title: ROI Calculator
 * * Block Description: ROI Calculator Block
 * * Block Icon: calculator
 * * Block Keywords: roi, calculator, roi calculator
 * * Post Types: page
 */

// Basic Settings
$id 		= !empty($block['anchor']) ? $block['anchor'] : 'roi__calculator';
$class		= !empty($block['className']) ? 'roi ' . $block['className'] : 'roi';
$id_comment = str_replace('_', ' ', $id);
$id_comment = ucwords(preg_replace('!\s+!', ' ', $id_comment));

// ROI Input Values
$roi_days 	  = get_field('roi_days', 'option') ?: '';
$roi_calls	  = get_field('roi_calls', 'option') ?: '';
$roi_agents	  = get_field('roi_agents', 'option') ?: '';
$roi_idle	  = get_field('roi_idle', 'option') ?: '';
$roi_salary	  = get_field('roi_salary', 'option') ?: '';
$roi_crm	  = get_field('roi_crm', 'option') ?: '';
$roi_outbound = get_field('roi_outbound', 'option') ?: '';
$roi_voice	  = get_field('roi_voice', 'option') ?: '';
$roi_uq		  = get_field('roi_uq', 'option') ?: '';
$roi_dial 	  = get_field('roi_dial', 'option') ?: '';
$roi_pop  	  = get_field('roi_pop', 'option') ?: '';
$roi_desktop  = get_field('roi_desktop', 'option') ?: '';
$roi_info  	  = get_field('roi_info', 'option') ?: '';

// Advanced Settings
$roi_calc_title = trim(get_field('roi_calc_title')) ?: esc_html__('ROI calculator', 'bs'); ?>

<?php if ($id_comment) : ?>
	<!-- <?= $id_comment ?> -->
<?php endif; ?>
<section id="<?= $id ?>" class="<?= $class ?> gray">

	<!-- ROI -->
	<div class="inner narrow">
		<h2 class="roi__title small__title"><?= $roi_calc_title ?></h2>
	</div>

	<div class="grid roi__grid inner narrow">

		<!-- ROI metrics -->
		<div class="roi__metrics">

			<!-- Hidden fields -->
			<input type="hidden" name="action" value="ajaxfilter">
			<input type="hidden" name="cpt" id="cpt" value="<?= 1 ?>">

			<!-- ROI Input Title -->
			<header class="roi__input__header">
				<h2 class="roi__input__title"><?= esc_html__('Contact Center Metrics', 'bs') ?></h2>
			</header>

			<!-- ROI Input Container -->
			<div class="roi__input__container">
				<div class="roi__input__text">
					<p><?= esc_html__('Days open for business', 'bs') ?></p>
					<div class="roi__input__info">
						<div class="roi__input__info__text"><?= esc_html__('Number of days out of 365 that your contact center runs', 'bs') ?></div>
					</div>
				</div>
				<div class="roi__input__wrapper">
					<input type="number" name="roi__days" id="roi__days" class="roi__input" min="1" max="365" placeholder="<?= $roi_days ?>">
				</div>
			</div>

			<!-- ROI Input Container -->
			<div class="roi__input__container">
				<div class="roi__input__text">
					<p><?= esc_html__('Calls connected per day', 'bs') ?></p>
					<div class="roi__input__info">
						<div class="roi__input__info__text"><?= esc_html__('Average number of total calls taken per day', 'bs') ?></div>
					</div>
				</div>
				<div class="roi__input__wrapper">
					<input type="number" name="roi__calls" id="roi__calls" class="roi__input" placeholder="<?= $roi_calls ?>">
				</div>
			</div>

			<!-- ROI Input Container -->
			<div class="roi__input__container">
				<div class="roi__input__text">
					<p><?= esc_html__('% of calls connected to agents', 'bs') ?></p>
					<div class="roi__input__info">
						<div class="roi__input__info__text"><?= esc_html__('Excluding IVR self service and abandoned', 'bs') ?></div>
					</div>
				</div>
				<div class="roi__input__wrapper roi__agents">
					<input type="number" name="roi__agents" id="roi__agents" class="roi__input" min="1" max="100" placeholder="<?= $roi_agents ?>">
				</div>
			</div>

			<!-- ROI Input Container -->
			<div class="roi__input__container">
				<div class="roi__input__text">
					<p><?= esc_html__('Avg agent idle time per day (in minutes)', 'bs') ?></p>
					<div class="roi__input__info">
						<div class="roi__input__info__text"><?= esc_html__('Unplanned time your agents spend waiting for calls', 'bs') ?></div>
					</div>
				</div>
				<div class="roi__input__wrapper roi__idle">
					<input type="number" name="roi__idle" id="roi__idle" class="roi__input" placeholder="<?= $roi_idle ?>">
				</div>
			</div>

			<!-- ROI Input Container -->
			<div class="roi__input__container">
				<div class="roi__input__text">
					<p><?= esc_html__('Agent hourly salary', 'bs') ?></p>
					<div class="roi__input__info">
						<div class="roi__input__info__text"><?= esc_html__('Remember to include benefits (e.g. healthcare, retirement)', 'bs') ?></div>
					</div>
				</div>
				<div class="roi__input__wrapper roi__salary">
					<input type="number" name="roi__salary" id="roi__salary" class="roi__input" placeholder="<?= $roi_salary ?>">
				</div>
			</div>

			<!-- ROI Input Container -->
			<div class="roi__input__container">
				<div class="roi__input__text">
					<p><?= esc_html__('% of calls with a known caller, matched in CRM', 'bs') ?></p>
					<div class="roi__input__info">
						<div class="roi__input__info__text"><?= esc_html__('Not all calls will match and pop', 'bs') ?></div>
					</div>
				</div>
				<div class="roi__input__wrapper roi__crm">
					<input type="number" name="roi__crm" id="roi__crm" class="roi__input" min="1" max="100" placeholder="<?= $roi_crm ?>">
				</div>
			</div>

			<!-- ROI Input Container -->
			<div class="roi__input__container">
				<div class="roi__input__text">
					<p><?= esc_html__('Outbound calls per day', 'bs') ?></p>
					<div class="roi__input__info">
						<div class="roi__input__info__text"><?= esc_html__('Number of calls your contact center makes per day', 'bs') ?></div>
					</div>
				</div>
				<div class="roi__input__wrapper roi__outbound">
					<input type="number" name="roi__outbound" id="roi__outbound" class="roi__input" placeholder="<?= $roi_outbound ?>">
				</div>
			</div>

			<!-- ROI Input Container -->
			<div class="roi__input__container">
				<div class="roi__input__text">
					<p><?= esc_html__('# of agents that can handle voice & omnichannel ', 'bs') ?></p>
					<div class="roi__input__info">
						<div class="roi__input__info__text"><?= esc_html__('Agents capable of handling phone calls, emails, chats, etc.', 'bs') ?></div>
					</div>
				</div>
				<div class="roi__input__wrapper roi__unplanned">
					<input type="number" name="roi__voice" id="roi__voice" class="roi__input" placeholder="<?= $roi_voice ?>">
				</div>
			</div>

			<!-- ROI Input Container -->
			<div class="roi__input__container">
				<div class="roi__input__text">
					<p><?= esc_html__('Anticipated % reduction in unplanned idle time due to omnichannel support', 'bs') ?></p>
					<div class="roi__input__info">
						<div class="roi__input__info__text"><?= esc_html__('Where available, with omnichannel support, agents can handle email and chat during lulls in telephone traffic', 'bs') ?></div>
					</div>
				</div>
				<div class="roi__input__wrapper roi__voice roi__uq">
					<input type="number" name="roi__uq" id="roi__uq" class="roi__input" min="1" max="100" placeholder="<?= $roi_uq ?>">
				</div>
			</div>

			<!-- ROI Input Title -->
			<header class="roi__input__header">
				<h2 class="roi__input__title">
					<?= esc_html__('Seconds saved per call', 'bs') ?>
				</h2>
				<small><?= esc_html__('(ranges are customer averages)', 'bs') ?></small>
			</header>

			<!-- ROI Input Container -->
			<div class="roi__input__container">
				<div class="roi__input__text">
					<p><?= esc_html__('Click-to-dial vs manual dialing', 'bs') ?></p>
					<div class="roi__input__info">
						<div class="roi__input__info__text"><?= esc_html__('Click to dial is faster and more accurate than manual dialing', 'bs') ?></div>
					</div>
				</div>
				<div class="roi__input__wrapper roi__dial">
					<input type="number" name="roi__dial" id="roi__dial" class="roi__input" placeholder="<?= $roi_dial ?>">
				</div>
			</div>

			<!-- ROI Input Container -->
			<div class="roi__input__container">
				<div class="roi__input__text">
					<p><?= esc_html__('Screen pop', 'bs') ?></p>
					<span class="roi__input__info">
						<div class="roi__input__info__text"><?= esc_html__('# of seconds it would have taken the agent to verbally identify caller and pull up the CRM record', 'bs') ?></div>
					</span>
				</div>
				<div class="roi__input__wrapper roi__pop">
					<input type="number" name="roi__pop" id="roi__pop" class="roi__input" placeholder="<?= $roi_pop ?>">
				</div>
			</div>

			<!-- ROI Input Container -->
			<div class="roi__input__container">
				<div class="roi__input__text">
					<p><?= esc_html__('Unified desktop', 'bs') ?></p>
					<span class="roi__input__info">
						<div class="roi__input__info__text"><?= esc_html__('Time saved per call by not having to juggle windows', 'bs') ?></div>
					</span>
				</div>
				<div class="roi__input__wrapper roi__desktop">
					<input type="number" name="roi__desktop" id="roi__desktop" class="roi__input" placeholder="<?= $roi_desktop ?>">
				</div>
			</div>

		</div>

		<!-- ROI Calculation -->
		<div class="roi__calc">

			<!-- ROI Calc Shell -->
			<div class="roi__calc__shell">

				<!-- ROI Calc Title -->
				<header class="roi__calc__header">
					<h2 class="roi__calc__title"><?= esc_html__('Calculation', 'bs') ?></h2>
				</header>

				<!-- ROI Calc Interval -->
				<div class="roi__calc__interval">
					<div class="roi__calc__interval__item daily active"><?= esc_html__('Daily', 'bs') ?></div>
					<div class="roi__calc__interval__item monthly"><?= esc_html__('Monthly', 'bs') ?></div>
					<div class="roi__calc__interval__item yearly"><?= esc_html__('Yearly', 'bs') ?></div>
				</div>

				<!-- ROI Calc Summary -->
				<div class="roi__calc__summary">
					<div class="roi__calc__summary__item">
						<p class="roi__calc__summary__title">
							<?= esc_html__('Total hours saved', 'bs') ?>
						</p>
						<strong id="roi__total__hours" class="roi__calc__summary__value">0</strong>
					</div>
					<div class="roi__calc__summary__item">
						<p class="roi__calc__summary__title">
							<?= esc_html__('Total $ saved', 'bs') ?></span>
						</p>
						<strong class="roi__calc__summary__value">$ <span id="roi__total__usd">0</span></strong>
					</div>
				</div>

				<!-- ROI Calc Item - Dial -->
				<div class="roi__calc__item">
					<div class="roi__calc__item__container">
						<p class="roi__calc__item__text"><?= esc_html__('Saving in hours with click to dial', 'bs') ?></p>
						<span id="roi__dial__hours" class="roi__calc__item__amount">0</span>
					</div>
					<div class="roi__calc__item__container">
						<p class="roi__calc__item__text small">$ <?= esc_html__('saved from click to dial', 'bs') ?></p>
						<span class="roi__calc__item__amount small">$ <span id="roi__dial__usd">0</span></span>
					</div>
				</div>

				<!-- ROI Calc Item - Pop -->
				<div class="roi__calc__item">
					<div class="roi__calc__item__container">
						<p class="roi__calc__item__text"><?= esc_html__('Saving in hours with screen pop', 'bs') ?></p>
						<span id="roi__pop__hours" class="roi__calc__item__amount">0</span>
					</div>
					<div class="roi__calc__item__container">
						<p class="roi__calc__item__text small">$ <?= esc_html__('saved from screen pop', 'bs') ?></p>
						<span class="roi__calc__item__amount small">$ <span id="roi__pop__usd">0</span></span>
					</div>
				</div>

				<!-- ROI Calc Item - Desktop -->
				<div class="roi__calc__item">
					<div class="roi__calc__item__container">
						<p class="roi__calc__item__text"><?= esc_html__('Saving in hours of a unified desktop', 'bs') ?></p>
						<span id="roi__desktop__hours" class="roi__calc__item__amount">0</span>
					</div>
					<div class="roi__calc__item__container">
						<p class="roi__calc__item__text small">$ <?= esc_html__('saved with unified desktop', 'bs') ?></p>
						<span class="roi__calc__item__amount small">$ <span id="roi__desktop__usd">0</span></span>
					</div>
				</div>

				<!-- ROI Calc Item - UQ -->
				<div class="roi__calc__item">
					<div class="roi__calc__item__container">
						<p class="roi__calc__item__text"><?= esc_html__('Saving in hours by using omnichannel integration', 'bs') ?></p>
						<span id="roi__uq__hours" class="roi__calc__item__amount">0</span>
					</div>
					<div class="roi__calc__item__container">
						<p class="roi__calc__item__text small">$ <?= esc_html__('saved with omnichannel agents', 'bs') ?></p>
						<span class="roi__calc__item__amount small">$ <span id="roi__uq__usd">0</span></span>
					</div>
				</div>

			</div>

			<!-- ROI Calc Button -->
			<div class="roi__calc__button">
				<a href="#contact__form" class="button"><?= esc_html__('Contact us', 'bs') ?></a>
			</div>

			<?php if ($roi_info) : ?>
				<!-- ROI Calc Info -->
				<p class="roi__calc__info"><?= $roi_info ?></p>
			<?php endif; ?>

		</div>
	</div>

</section>

<?php


// EOF
