<?php

/**
 * * Block Name: contact-person
 * * Block Title: Contact Us
 * * Block Description: Contact Us Block
 * * Block Icon: businessperson
 * * Block Keywords: interested, contact person
 * * Post Types: page
 */

// Basic Settings
$id 		= 'contact__person';
$id_comment = ucwords(preg_replace('!\s+!', ' ', str_replace('_', ' ', $id)));
$class		= !empty($block['className']) ? 'contact__person ' . $block['className'] : 'contact__person';

// Advanced Settings
$interested_title 		 = trim(get_field('interested_title')) ?: esc_html__('Interested?', 'bs');
$contact_person_title 	 = trim(get_field('contact_person_title')) ?: '';
$contact_person_img 	 = get_field('contact_person_img') ?: '';
$contact_person_name 	 = trim(get_field('contact_person_name')) ?: '';
$contact_person_rank 	 = trim(get_field('contact_person_rank')) ?: '';
$contact_person_bio 	 = trim(get_field('contact_person_bio')) ?: '';
$contact_person_phone 	 = trim(get_field('contact_person_phone')) ?: '';
$contact_person_email 	 = trim(get_field('contact_person_email')) ?: '';
$contact_person_linkedin = trim(get_field('contact_person_linkedin')) ?: ''; ?>

<?php if ($id_comment) : ?>
	<!-- <?= $id_comment ?> -->
<?php endif; ?>
<section <?= $id ? 'id="' . esc_attr($id) . '"' : '' ?> class="<?= esc_attr($class) ?>">

	<div class="inner narrow grid contact__person__grid">
		<div class="contact__person__interested__container">
			<h2 class="contact__person__interested small__title"><?= $interested_title ?></h2>
		</div>

		<div class="contact__person__container">
			<?php if ($contact_person_title) : ?>
				<h3 class="contact__person__title"><?= $contact_person_title  ?></h3>
			<?php endif; ?>
			<?php if ($contact_person_img && $contact_person_name && $contact_person_rank) : ?>
				<div class="contact__person__info">
					<div class="contact__person__image">
						<?= custom_image_size($contact_person_img, 'thumbnail', $contact_person_name, '', '', 'none') ?>
					</div>
					<div class="contact__person__text">
						<h3 class="contact__person__name"><?= $contact_person_name ?></h3>
						<p class="contact__person__rank"><?= $contact_person_rank ?></p>
					</div>
				</div>
			<?php endif; ?>
			<?php if ($contact_person_bio) : ?>
				<p class="contact__person__bio"><?= $contact_person_bio ?></p>
			<?php endif; ?>
			<?php if ($contact_person_phone || $contact_person_email || $contact_person_linkedin) : ?>
				<ul class="contact__person__contacts">
				<?php endif; ?>
				<?php if ($contact_person_phone) : ?>
					<li><a href="<?= esc_url('tel:' . clean_phone($contact_person_phone)) ?>" class="contact__person__phone read__more dark"><?= $contact_person_phone ?></a></li>
				<?php endif; ?>
				<?php if ($contact_person_email) : ?>
					<li><a href="<?= esc_url('mailto:' . antispambot($contact_person_email)) ?>" class="contact__person__email read__more dark"><?= antispambot($contact_person_email) ?></a></li>
				<?php endif; ?>
				<?php if ($contact_person_linkedin) : ?>
					<li><a href="<?= esc_url($contact_person_linkedin) ?>" target="_blank" class="contact__person__linkedin read__more dark"><?= esc_html__('LinkedIn', 'bs') ?></a></li>
				<?php endif; ?>
				<?php if ($contact_person_phone || $contact_person_email || $contact_person_linkedin) : ?>
				</ul>
			<?php endif; ?>
		</div>
	</div>

</section>