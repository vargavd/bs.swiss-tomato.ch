<?php

/**
 * * Block Name: video-list
 * * Block Title: Video List
 * * Block Description: Video List Block
 * * Block Icon: format-video
 * * Block Multiple: true
 * * Block Keywords: video list, videos, youtube
 * * Post Types: post
 */

// Basic Settings
$id 		= !empty($block['anchor']) ? $block['anchor'] : 'video__list';
$id_comment = ucwords(preg_replace('!\s+!', ' ', str_replace('_', ' ', $id)));
$class		= !empty($block['className']) ? 'video__list ' . $block['className'] : 'video__list';


// ? Has Video List Row
if (trim(have_rows('video_list'))) : ?>

	<?php if ($id_comment) : ?>
		<!-- <?= $id_comment ?> -->
	<?php endif; ?>
	<div <?= $id ? 'id="' . esc_attr($id) . '"' : '' ?> class="<?= esc_attr($class) ?>">

		<?php

		// * Loop Video List
		while (have_rows('video_list')) : the_row();

			// Vars
			$video_list_title   = trim(get_sub_field('video_list_title')) ?: '';
			$video_list_youtube = trim(get_sub_field('video_list_youtube')) ?: '';

			if ($video_list_title && $video_list_youtube) ?>

			<div class="video__list__grid grid">
				<h3 class="video__list__title"><?= $video_list_title ?></h3>
				<div class="video__list__youtube">
					<iframe class="youtube__player" width="310" height="165" src="//youtube.com/embed/<?= $video_list_youtube ?>" frameborder="0" allowfullscreen></iframe>
				</div>
			</div>

		<?php

		// * End Loop
		endwhile; ?>
	</div>

<?php

endif;

// EOF