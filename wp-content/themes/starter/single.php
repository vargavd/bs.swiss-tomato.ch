<?php
/*
 *
 * Template - Single Posts
 *
 */
get_header();


// Vars
$post_id  	  = get_the_ID();
$category 	  = get_the_category($post_id) ? get_the_category($post_id)[0]->name : '';
$author	  	  = get_field('post_author', $post_id) ?: '';
$event_start  = trim(get_field('event_start')) ?: '';
$event_end 	  = trim(get_field('event_end')) ?: '';
$date		  = get_the_date('F j, Y');

if (ICL_LANGUAGE_CODE === 'de') {
	$date = get_the_date('j. F Y');
}

// ? Event Start Date Exists
if ($event_start) {

	if (!$event_end) {
		$event_start = date_i18n('F j, Y', strtotime($event_start));
		if (ICL_LANGUAGE_CODE === 'de') {
			$event_start = date_i18n('j. F Y', strtotime($event_start));
		}
	}

	// ? Event End Date Exists
	if ($event_end) {

		// Event Start Split
		$start_year  = date_i18n('Y', strtotime($event_start));
		$start_month = date_i18n('m', strtotime($event_start));
		$start_day   = date_i18n('d', strtotime($event_start));

		// Event End Split
		$end_year  = date_i18n('Y', strtotime($event_end));
		$end_month = date_i18n('m', strtotime($event_end));
		$end_day   = date_i18n('d', strtotime($event_end));


		// Year ++
		if ($start_year < $end_year) {
			$event_start = date_i18n('F j, Y', strtotime($event_start)) . ' - ' . date_i18n('F j, Y', strtotime($event_end));
			if (ICL_LANGUAGE_CODE === 'de') {
				$event_start = date_i18n('j. F Y', strtotime($event_start)) . ' - ' . date_i18n('j. F Y', strtotime($event_end));
			}
		}
		// Month ++
		if ($start_year === $end_year && $start_month < $end_month) {
			$event_start = date_i18n('F j', strtotime($event_start)) . ' - ' . date_i18n('F j', strtotime($event_end)) . ', ' . $start_year;
			if (ICL_LANGUAGE_CODE === 'de') {
				$event_start = date_i18n('j. F', strtotime($event_start)) . ' - ' . date_i18n('j F', strtotime($event_end)) . ', ' . $start_year;
			}
		}
		// Day ++
		if ($start_year === $end_year && $start_month === $end_month && $start_day < $end_day) {
			$event_start = date_i18n('F j', strtotime($event_start)) . ' - ' . date_i18n('j', strtotime($event_end)) . ', ' . $start_year;
			if (ICL_LANGUAGE_CODE === 'de') {
				$event_start = date_i18n('j.', strtotime($event_start)) . ' - ' . date_i18n('j. F', strtotime($event_end)) . ' ' . $start_year;
			}
		}
	}
}

// WPML apply_filters('wpml_object_id', 352, 'post')

while (have_posts()) : the_post(); 
  $ID = get_the_ID();

  $header_background = get_field('header_background', $ID);
  $background_color = get_field('background_color', $ID);
  $color_1 = get_field('color_1', $ID) ?? '#060F77';
  $color_2 = get_field('color_2', $ID) ?? '#F53136';
  $background_image = get_field('background_image', $ID);
  $header_logo = get_field('header_logo', $ID);

  $style = "background: ";

  switch ($header_background) {
    case "color":
      $style .= $background_color;
      break;
    case "image":
      $style .= "url(" . $background_image['url'] . ")";
      break;
    default:
      $style .= "linear-gradient(90deg, $color_1 0%, $color_2 100%)";
  }

  if (is_numeric($header_logo)) {
    $header_logo = wp_get_attachment_image_src($header_logo, 'full')[0];
  } elseif (is_array($header_logo)) {
    $header_logo = $header_logo['url'];
  }
?>

  <section 
    id="hero__header" 
    class="page__<?= get_the_ID() ?>"
    style="<?=$style?>"
  >
    <?php if ($header_background === 'image' && is_array($background_image)) : ?>
      <?= custom_image_size($background_image['url'], 'full', get_the_title(), 'hero__header__bg', 'none') ?>
    <?php endif; ?>
    <div class="grid hero__header__grid inner narrow">
      <header class="hero__header">
        <?php if ($header_logo) : ?>
          <?= custom_image_size($header_logo, 'logo', get_the_title(), 'hero__header__logo', '', 'none') ?>
        <?php endif; ?>
                  
        <h1 class="hero__header__title">
          <a href="<?= esc_url(the_permalink(352)) ?>" data-icon="" class="to__parent"><span class="screen-reader-text"><?= esc_html__('News & Events', 'bs') ?></span></a>
          <?= the_title() ?>
        </h1>
      </header>
    </div>
  </section>

	<article id="news__container" <?php post_class('inner'); ?>>

		<div class="grid news__grid">

			<!-- Meta -->
			<div class="news__meta">
				<div class="news__meta__helper">
					<?php if ($category) : ?>
						<!-- Catefory -->
						<div class="news__meta__item">
							<h4 class="news__meta__title"><?= esc_html__('Category', 'bs') ?></h4>
							<small class="news__category"><?= $category ?></small>
						</div>
					<?php endif; ?>

					<!-- Publication Date -->
					<div class="news__meta__item">
						<h4 class="news__meta__title"><?= esc_html__('Date', 'bs') ?></h4>
						<small class="news__time"><?= $event_start ?: $date ?></small>
					</div>

					<?php if ($author) : ?>
						<!-- Author -->
						<div class="news__meta__item">
							<h4 class="news__meta__title"><?= esc_html__('Author', 'bs') ?></h4>
							<small class="news__author__name"><?= $author ?></small>
						</div>
					<?php endif; ?>

					<!-- Share -->
					<div class="news__meta__item">
						<h4 class="news__meta__title"><?= esc_html__('Share', 'bs') ?></h4>
						<div class="news__share">
							<a href="//twitter.com/intent/tweet?url=<?php the_permalink(); ?>" data-icon="" class="news__share__link twitter" target="_blank" title="Twitter"></a>
							<a href="//linkedin.com/sharing/share-offsite/?url=<?php the_permalink(); ?>" data-icon="" class="news__share__link linkedin" target="_blank" title="LinkedIn"></a>
							<a href="mailto:?&subject=&body=<?php the_permalink(); ?>" data-icon="" class="news__share__link mailto" target="_blank" title="E-mail"></a>
						</div>
					</div>
				</div>
			</div>


			<!-- Content -->
			<div id="news__content" class="news__content">
        
        <!-- Title -->
				<!-- <header>
					<h1 class="news__title"><?php the_title(); ?></h1>
				</header> -->

				<?php
				/* translators: %s: Name of current post */
				the_content(sprintf(
					__('Continue reading<span class="screen-reader-text"> "%s"</span>', 'bs'),
					get_the_title()
				));
				?>

				<?php
				wp_link_pages([
					'before'      => '<div class="page-links">' . esc_html__('Pages:', 'bs'),
					'after'       => '</div>',
					'link_before' => '<span class="page-number">',
					'link_after'  => '</span>',
				]);
				?>

				<?= show_tags(); ?>

			</div><!-- .entry-content -->

		</div><!-- .news__grid -->

	</article><!-- #post -->

<?php endwhile; ?>

<?php

get_footer();

// EOF