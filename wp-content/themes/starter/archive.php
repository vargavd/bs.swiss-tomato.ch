<?php

/**
 * Archive
 */
get_header(); ?>

<?php

if (have_posts()) :

	while (have_posts()) : the_post(); ?>


		<article class="content">

			<h1 class="content__title">
				<a href="<?php the_permalink(); ?>">
					<?php the_title(); ?>
				</a>
			</h1>


			<div class="content__content">

				<?php the_content(); ?>
				<?php
				wp_link_pages([
					'before' => '<div class="pagelinks">' . esc_html__('Pages:', 'bs'),
					'after'  => '</div>',
				]);
				?>

			</div>
			<!-- C O N T E N T _ _ C O N T E N T -->

		</article>

	<?php endwhile; ?>

	<?php the_posts_navigation(); ?>

<?php else : ?>

	<section class="no__results">

		<?php esc_html_e('Nothing Found', 'bs'); ?>

		<?php if (is_home() && current_user_can('publish_posts')) : ?>

			<p>
				<?php printf(wp_kses(__('Ready to publish your first post? <a href="%1$s">Get started here</a>.', 'bs'), ['a' => ['href' => []]]), esc_url(admin_url('post-new.php'))); ?></p>

		<?php elseif (is_search()) : ?>

			<p>
				<?php esc_html_e('Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'bs'); ?>
			</p>
			<?php get_search_form(); ?>

		<?php else : ?>

			<p>
				<?php esc_html_e('It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'bs'); ?>
			</p>
			<?php get_search_form(); ?>

		<?php endif; ?>


	</section>

<?php endif; ?>

</div>

<?php

get_footer();

// EOF