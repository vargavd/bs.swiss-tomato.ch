<?php
/*
 *
 * Template - Single Posts
 *
 */
get_header();


// Vars
$post_id  = get_the_ID();
$category = get_the_category($post_id) ? get_the_category($post_id)[0]->name : '';
// WPML apply_filters('wpml_object_id', 369, 'post')

// Job Basics
$job_location = trim(get_field('job_location')) ?: '';
$job_type 	  = trim(get_field('job_type')) ?: '';
$job_apply 	  = trim(get_field('job_apply')) ?: '';

// Job Evironment
$job_environment_title = trim(get_field('job_environment_title')) ?: esc_html__('Environment', 'bs');
$job_environment_text  = trim(get_field('job_environment_text')) ?: '';

// Job Tasks
$job_tasks_title = trim(get_field('job_tasks_title')) ?: esc_html__('Main tasks', 'bs');
$job_tasks_text  = trim(get_field('job_tasks_text')) ?: '';

// Job Profile
$job_profile_title = trim(get_field('job_profile_title')) ?: esc_html__('Your profile', 'bs');
$job_profile_text  = trim(get_field('job_profile_text')) ?: '';

// Job Offer
$job_offer_title = trim(get_field('job_offer_title')) ?: esc_html__('What we offer', 'bs');
$job_offer_text  = trim(get_field('job_offer_text')) ?: '';

// Job Interested
$job_interested_title    = trim(get_field('job_interested_title')) ?: esc_html__('Interested', 'bs');
$job_interested_content  = get_field('job_interested_content') ?: '';
$job_send_text 			 = '';
$job_question_text 		 = '';
$job_contact_person_img  = '';
$job_contact_person_name = '';
$job_contact_person_rank = '';
$job_data_text 			 = '';

// If Job Interested contains data
if ($job_interested_content) {
	$job_send_text 			 = trim($job_interested_content['job_send_text']) ?: esc_html__('Please send your complete application!', 'bs');
	$job_question_text 		 = trim($job_interested_content['job_question_text']) ?: '';
	$job_contact_person_img  = $job_interested_content['job_contact_person_img'] ?: '';
	$job_contact_person_name = trim($job_interested_content['job_contact_person_name']) ?: '';
	$job_contact_person_rank = trim($job_interested_content['job_contact_person_rank']) ?: '';
	$job_data_text 			 = trim($job_interested_content['job_data_text']) ?: '';
}

?>

<!-- Hero Header -->
<section id="hero__header" class="job__hero__header page__<?= $post_id ?>">
	<div class="grid hero__header__grid inner narrow">
		<div class="hero__header__helper">
			<header class="hero__header job__header">
				<h1 class="hero__header__title job__header__title">
          <a href="<?= esc_url(the_permalink(369)) ?>#jobs" data-icon="" class="to__parent"><span class="screen-reader-text"><?= esc_html__('Careers', 'bs') ?></span></a>
          <?= the_title() ?>
        </h1>
			</header>
			<div class="job__data job__data__grid">
				<?php if ($job_location) : ?>
					<!-- Job Location -->
					<div class="job__location__container">
						<small class="job__data__title"><?= esc_html__('Location', 'bs') ?></small>
						<p class="job__location__value job__data__value"><?= $job_location ?></p>
					</div>
				<?php endif; ?>
				<?php if ($job_type) : ?>
					<!-- Job Type -->
					<div class="job__type__container">
						<small class="job__data__title"><?= esc_html__('Job type', 'bs') ?></small>
						<p class="job__type__value job__data__value"><?= $job_type ?></p>
					</div>
				<?php endif; ?>
				<?php if ($job_apply) : ?>
					<!-- Job Apply -->
					<div class="job__apply__container">
						<a href="mailto:<?= $job_apply ?>" class="job__apply__button button"><?= esc_html__('Apply now', 'bs') ?></a>
					</div>
				<?php endif; ?>
			</div>
		</div>
	</div>
</section>

<?php while (have_posts()) : the_post(); ?>

	<article id="job__container" class="job__container inner narrow">
		<?php if ($job_environment_text) : ?>
			<!-- Job Environment -->
			<section class="job__environment job__grid grid">
				<header class="job__environment__header">
					<h3 class="job__environment__title small__title"><?= $job_environment_title ?></h3>
				</header>
				<div class="job__environment__text">
					<?= $job_environment_text ?>
				</div>
			</section>
		<?php endif; ?>

		<?php if ($job_tasks_text) : ?>
			<!-- Job Tasks -->
			<section class="job__tasks job__grid grid">
				<header class="job__tasks__header">
					<h3 class="job__tasks__title small__title"><?= $job_tasks_title ?></h3>
				</header>
				<div class="job__tasks__text">
					<?= $job_tasks_text ?>
				</div>
			</section>
		<?php endif; ?>

		<?php if ($job_profile_text) : ?>
			<!-- Job Tasks -->
			<section class="job__tasks job__grid grid">
				<header class="job__tasks__header">
					<h3 class="job__tasks__title small__title"><?= $job_profile_title ?></h3>
				</header>
				<div class="job__tasks__text">
					<?= $job_profile_text ?>
				</div>
			</section>
		<?php endif; ?>

		<?php if ($job_offer_text) : ?>
			<!-- Job Tasks -->
			<section class="job__tasks job__grid grid">
				<header class="job__tasks__header">
					<h3 class="job__tasks__title small__title"><?= $job_offer_title ?></h3>
				</header>
				<div class="job__tasks__text">
					<?= $job_offer_text ?>
				</div>
			</section>
		<?php endif; ?>

	</article><!-- #post -->

	<?php if ($job_interested_content) : ?>
		<!-- Job Interested -->
		<section class="job__interested">

			<div class="inner narrow">

				<div class="job__grid grid">

					<header class="job__interested__header">
						<h3 class="job__interested__title small__title"><?= $job_interested_title ?></h3>
					</header>

					<div class="job__interested__content">

						<h3 class="job__send__text"><?= $job_send_text ?></h3>

						<?php if ($job_apply) : ?>
							<a href="mailto:<?= $job_apply ?>" class="job__apply__button button"><?= esc_html__('Apply now', 'bs') ?></a>
						<?php endif; ?>

						<?php if ($job_question_text) : ?>
							<div class="job__question"><?= $job_question_text ?></div>
						<?php endif; ?>

						<?php if ($job_contact_person_img && $job_contact_person_name && $job_contact_person_rank) : ?>
							<!--  -->
							<small class="job__contact__person__title"><?= esc_html__('Your contact person:', 'bs') ?></small>
							<div class="job__contact__person__info">
								<div class="job__contact__person__image">
									<?= custom_image_size($job_contact_person_img, 'thumbnail', $job_contact_person_name, '', '', 'none') ?>
								</div>
								<div class="job__contact__person__text">
									<h3 class="job__contact__person__nane"><?= $job_contact_person_name ?></h3>
									<p class="job__contact__person__rank"><?= $job_contact_person_rank ?></p>
								</div>
							</div>
						<?php endif; ?>

						<?php if ($job_data_text) : ?>
							<div class="job__data__protection"><?= $job_data_text ?></div>
						<?php endif; ?>
					</div>

				</div>

			</div>

		</section>
	<?php endif; ?>

<?php endwhile; ?>

<?php

get_footer();

// EOF