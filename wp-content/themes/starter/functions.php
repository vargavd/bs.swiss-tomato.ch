<?php

/**
 * WordPress Core - Customization
 * 
 * This file contains the following configurations:
 * 
 * * Adding Security Headers (CSP)
 * * Define THEMEPATH, SUPERADMIN
 * * Enqueue Scripts & Styles
 * * Custom Theme Setup
 * * Register Menus
 * * Logout Redirection
 */


// WPML support suggested this:
// https://wpml.org/errata/page-builders-translated-content-is-overwritten-with-the-original-when-updating-the-original-post/
define( 'WPML_TRANSLATION_AUTO_UPDATE_ENABLED', false );


// SECURITY HEADERS
add_filter( 'wp_headers', function ($headers) {
  if (is_admin()) {
    return $headers;
  }

  $permission_policies = [
    "accelerometer",
    "ambient-light-sensor",
    "autoplay",
    "battery",
    "camera",
    "clipboard-read",
    "clipboard-write",
    "document-domain",
    "encrypted-media",
    "execution-while-not-rendered",
    "execution-while-out-of-viewport",
    "fullscreen",
    "gamepad",
    "geolocation",
    "gyroscope",
    "hid",
    "idle-detection",
    "local-fonts",
    "magnetometer",
    "microphone",
    "midi",
    "payment",
    "payment",
    "picture-in-picture",
    "publickey-credentials-get",
    "screen-wake-lock",
    "serial",
    "speaker-selection",
    "sync-xhr",
    "usb",
    "web-share",
    "xr-spatial-tracking",
  ];

  $permission_policy_string = trim(implode('=(), ', $permission_policies));

  $headers['X-Frame-Options']         = 'SAMEORIGIN';
  $headers['Content-Security-Policy'] = "frame-ancestors https://app.socio.events http://app.socio.events http://*.socio.events https://*socio.events";
  $headers['Permissions-Policy']      = $permission_policy_string;

  return $headers; 
});


// THEMEPATH
if (!defined('THEMEPATH')) {
	define('THEMEPATH', get_template_directory_uri());
}


// SUPERADMIN
if (!defined('SUPERADMIN')) {
	define('SUPERADMIN', 'kormoskrisztian');
}


// Enqueue Scipts / Styles
add_action('wp_enqueue_scripts', 'starter_theme_scripts', 9999);

function starter_theme_scripts()
{

	// Deregister Extra CSS
	wp_deregister_style('wp-block-library');
	wp_deregister_style('wc-block-style');
	wp_deregister_style('moove_gdpr_frontend');
	wp_deregister_style('contact-form-7');

	// Deregister Extra JS
	wp_deregister_script('jquery');
	wp_deregister_script('jquery-core');
	wp_deregister_script('jquery-ui-core');
	wp_deregister_script('jquery-ui-datepicker');

	// Enqueue main CSS
	wp_enqueue_style('style', THEMEPATH . '/style.min.css', NULL, filemtime(get_stylesheet_directory() . '/style.min.css'));

	// Enqueue Own JS
	wp_enqueue_script('jquery', THEMEPATH . '/assets/js/jquery.min.js', false, NULL, false);
	wp_enqueue_script('custom', THEMEPATH . '/assets/js/custom.min.js', array('jquery'), NULL, true);
	wp_enqueue_script('lity', THEMEPATH . '/assets/js/lity/lity.min.js', array('jquery'), NULL, true);

  // SWIPER PROVIDER
  bs_swisst_enqueue_script_with_filetime('bs-swiper-provider-script', '/assets/js/swiper/swiper-provider.js', array('jquery'));  

  // BS Scroll Triggerer
  bs_swisst_enqueue_script_with_filetime('bs-scroll-triggerer-script', '/assets/js/scroll-triggerer/scroll-triggerer.js', array('jquery'));

  // BS Mega Menu
  bs_swisst_enqueue_script_with_filetime('bs-mega-menu-scripts', '/assets/js/mega-menu/mega-menu.js', array('jquery'));

	if (isset($_COOKIE['moove_gdpr_popup'])) {
		// Remove GDPR script
		wp_deregister_script('moove_gdpr_frontend');
	}

	if (true === is_block_exists('acf/cases') || true === is_block_exists('acf/testimonials') || true === is_block_exists('acf/gadgets')) {
		// Enqueue Swiper
		// wp_enqueue_script('swiper', THEMEPATH . '/assets/js/swiper/swiper.min.js', false, NULL, true);
    // wp_enqueue_script('swiper', 'https://cdn.jsdelivr.net/npm/swiper@6.8.4/swiper-bundle.min.js', false, NULL, false);
	}

	//if (!is_page_template('template-contact.php') && false === is_block_exists('acf/contact')) {
	if (is_singular('jobs')) {
		// Deregister Contact Form 7 Script
		wp_deregister_script('contact-form-7');
	}

	if (true === is_block_exists('acf/event-filter')) {
		// Enqueue Custom Filter
		wp_enqueue_script('custom-filter', THEMEPATH . '/assets/js/filter/custom-filter.min.js', false, NULL, true);
	}

	if (true === is_block_exists('acf/gadgets')) {
		// Enqueue LightGallery
		wp_enqueue_script('lightlity', THEMEPATH . '/assets/js/gallery/lightgallery.min.js', false, NULL, true);
	}

	// Scroll Animation Library
	wp_enqueue_script('sal', THEMEPATH . '/assets/js/animation/sal.min.js', false, NULL, true);

	// Splitting JS
	wp_enqueue_script('splitting', THEMEPATH . '/assets/js/animation/splitting.min.js', false, NULL, true);

	// Loadmore + Filter for Posts & Resources
	if (true === is_block_exists('acf/posts') || true === is_block_exists('acf/resources-list')) {

		global $wp_query;

		// AJAX Filter + Load More
		wp_enqueue_script('ajax-loadmore', THEMEPATH . '/assets/js/filter/loadmore.min.js', ['jquery'], NULL, true);

		wp_localize_script('ajax-loadmore', 'ajax_filter_loadmore_params', [
			'ajaxurl' 	   => admin_url('admin-ajax.php'),
			'nonce'		   => wp_create_nonce('ajax-filter-loadmore-nonce'),
			'loadmore'	   => esc_html__('Load more', 'bs'),
			'loading'	   => esc_html__('Loading...', 'bs'),
			'current_page' => get_query_var('paged') ? get_query_var('paged') : 1,
			'max_page' 	   => $wp_query->max_num_pages,
		]);
	}

	// ROI Calculator 
	if (true === is_block_exists('acf/roi-calculator')) {

		wp_enqueue_script('roi-calculator', THEMEPATH . '/assets/js/blocks/roi-calculator.min.js', ['jquery'], NULL, true);

		wp_localize_script('roi-calculator', 'roi_calculator_params', [
			/*'saved_day'	    => esc_html__('saved per day', 'bs'),
			'saved_month'	=> esc_html__('saved per month', 'bs'),
			'saved_year'	=> esc_html__('saved per year', 'bs'),
			'daily'	   		=> esc_html__('Daily', 'bs'),
			'monthly'	   	=> esc_html__('Monthly', 'bs'),
			'yearly'	   	=> esc_html__('Yearly', 'bs')*/]);
	}

  // Footer JS
  bs_swisst_enqueue_script_with_filetime('bs-footer-script', '/assets/js/footer.js', array('jquery', 'sal', 'splitting', 'lity'));

	/*
	// Test GLOBAL Scripts / Styles
	pr1($GLOBALS['wp_scripts']);
	pr1($GLOBALS['wp_styles']);
	*/
}


// Custom Admin Style
add_action('admin_enqueue_scripts', 'custom_admin_style');
function custom_admin_style()
{
	wp_enqueue_style('custom_admin_css', THEMEPATH . '/assets/css/admin.min.css', false, '1.0.0');
	//wp_enqueue_script( 'custom_admin_js', THEMEPATH .'/assets/js/admin.min.js', false, '1.0.0' );

  bs_swisst_enqueue_script_with_filetime('bs-vdani-admin-scripts', '/assets/js/vdani-admin-scripts.js', NULL);
}

// Better quality images: 90%
add_filter('jpeg_quality', function ($arg) {
	return 90;
});

// Remove auto lazy loading
add_filter('wp_lazy_loading_enabled', '__return_false');


// Custom Theme Setup
if (!function_exists('custom_theme_setup')) {

	add_action('after_setup_theme', 'custom_theme_setup');

	function custom_theme_setup()
	{

		// Remove useless image sizes
		remove_image_size('1536x1536');
		remove_image_size('2048x2048');

		// Update thumbnail size width & height
		update_option('thumbnail_size_w', 100);
		update_option('thumbnail_size_h', 100);

		// Add custom image size - Logo
		if (!has_image_size('logo')) {
			add_image_size('logo', 9999, 60, false);
		}

		// Update medium size width & height
		update_option('medium_size_w', 433);
		update_option('medium_size_h', 325);

		// Add custom image size - Management
		if (!has_image_size('management')) {
			add_image_size('management', 346, 296, true);
		}

		// Add custom image size - News & Event Image
		if (!has_image_size('medium_post')) {
			add_image_size('medium_post', 441, 256, true);
		}

		// Add custom image size - Page Featured Image
		if (!has_image_size('medium_feature')) {
			add_image_size('medium_feature', 650, 352, true);
		}

		// Add support for editor styles.
		add_theme_support('editor-styles');
		add_theme_support('align-wide');
		add_editor_style('assets/css/editor.min.css');

		// Remove gutenberg patterns
		remove_theme_support('core-block-patterns');

		// Add default posts and comments RSS feed links to head.
		add_theme_support('automatic-feed-links');
		add_theme_support('title-tag');
		add_theme_support('post-thumbnails');

		// Disable custom font sizes
		add_theme_support('disable-custom-font-sizes');

		// Editor Font Styles
		add_theme_support('editor-font-sizes', [
			[
				'name'      => __('Small', 'bs'),
				'shortName' => __('S', 'bs'),
				'size'      => 18,
				'slug'      => 'small'
			],
			[
				'name'      => __('Normal', 'bs'),
				'shortName' => __('M', 'bs'),
				'size'      => 22,
				'slug'      => 'normal'
			],
			[
				'name'      => __('Large', 'bs'),
				'shortName' => __('L', 'bs'),
				'size'      => 25,
				'slug'      => 'large'
			],
		]);

		// Disable Custom Colors
		add_theme_support('disable-custom-colors');

		// Editor Color Palette
		add_theme_support('editor-color-palette', [
			[
				'name'  => __('Primary', 'bs'),
				'slug'  => 'primary',
				'color' => '#005479',
			],
			[
				'name'  => __('Secondary', 'bs'),
				'slug'  => 'secondary',
				'color'	=> '#00cdcc',
			],
			[
				'name'  => __('Blue', 'bs'),
				'slug'  => 'blue',
				'color' => '#000e79',
			],
		]);

		// Switch default core markup for search form, comment form, and comments to output valid HTML5.
		add_theme_support('html5', [
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
			'script',
			'stlye'
		]);

		// Set up the WordPress core custom background feature.
		add_theme_support('custom-background', apply_filters('custom_background_args', [
			'default-color' => 'ffffff',
			'default-image' => '',
		]));

		// Add theme support for selective refresh for widgets.
		add_theme_support('customize-selective-refresh-widgets');

		// Add support for core custom logo
		add_theme_support('custom-logo', [
			'height'      => 50,
			'width'       => 200,
			'flex-width'  => true,
			'flex-height' => true,
		]);

		// Include everíthing inside of /inc folder
		$includes = array_merge(
			glob(get_theme_root() . '/' . get_template() . '/inc/*.php') // All Includes
		);

		// Ignore files starting with an underscore (e.g., inc/_translate.php...)
		if ($includes) {
			foreach ($includes as $include) {
				$exploded_path = explode('/', $include);
				$filename = end($exploded_path);
				if (strpos($filename, '_') !== 0) {
					include_once($include);
				}
			}
		}
	}
}


// Register Custom Menus
add_action('init', 'register_theme_menu');
function register_theme_menu()
{
	register_nav_menu('mainmenu', 'Main menu');
	register_nav_menu('footermenu', 'Footer menu');
  register_nav_menu('megamenu', 'Mega Menu');
  register_nav_menu('megamenu-sidebar', 'Mega Menu Sidebar');
  register_nav_menu('topmenu', 'Top Menu');
}

// Logout Redirect to Home or Any
add_action('wp_logout', 'logout_redirect');
function logout_redirect()
{
	wp_redirect(esc_url(home_url('/')));
	exit;
}

// EOF