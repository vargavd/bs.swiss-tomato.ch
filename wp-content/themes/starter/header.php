<?php

// Set Locale
setlocale(LC_ALL, 'hu_HU.UTF-8');

// ? Is Mobile Device
$mobile = !wp_is_mobile() ?: 'is__mobile'; ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>

	<!-- Meta -->
	<meta charset="UTF-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<?php /* ?>
	<meta http-equiv="Content-Security-Policy" content="
	default-src 'self' https:;
	font-src 'self' data:;
	style-src 'self' https: 'unsafe-inline';
	script-src 'self' https: 'unsafe-inline';
	base-uri 'self';
	form-action 'self';
	object-src 'none';">
	<?php */ ?>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="language" content="Hungarian" />
	<meta name="format-detection" content="telephone=no" />
	<meta name="msapplication-tap-highlight" content="no" />

	<!-- Favicon -->
	<link rel="shortcut icon" href="<?= THEMEPATH ?>/assets/img/favicon.ico" />

	<!-- Fonts Preloading -->
	<!-- <link rel="preload" href="<?= THEMEPATH ?>/fonts/SourceSansPro-Regular.woff2" as="font" type="font/woff2" crossorigin>
	<link rel="preload" href="<?= THEMEPATH ?>/fonts/SourceSansPro-SemiBold.woff2" as="font" type="font/woff2" crossorigin>
	<link rel="preload" href="<?= THEMEPATH ?>/fonts/SourceSansPro-Bold.woff2" as="font" type="font/woff2" crossorigin>
  <link rel="preload" href="<?= THEMEPATH ?>/fonts/SourceSansPro-Black.woff2" as="font" type="font/woff2" crossorigin>
  <link rel="preload" href="<?= THEMEPATH ?>/fonts/fa-brands-400.woff2" as="font" type="font/woff2" crossorigin>
	<link rel="preload" href="<?= THEMEPATH ?>/fonts/fa-light-300.woff2" as="font" type="font/woff2" crossorigin>
  <link rel="preload" href="<?= THEMEPATH ?>/fonts/fa-regular-400.woff2" as="font" type="font/woff2" crossorigin>
  <link rel="preload" href="<?= THEMEPATH ?>/fonts/fa-sharp-regular-400.woff2" as="font" type="font/woff2" crossorigin>
  <link rel="preload" href="<?= THEMEPATH ?>/fonts/fa-solid-900.woff2" as="font" type="font/woff2" crossorigin> -->


	<?php wp_head(); ?>


	<!-- OneTrust Cookies Consent Notice start for www.bucher-suter.com -->
	<!-- <script type="text/javascript" src="https://cdn.cookielaw.org/consent/fdc74086-f806-4e24-bb1d-5ed8df2ccde9/OtAutoBlock.js"></script>
	<script src="https://cdn.cookielaw.org/scripttemplates/otSDKStub.js" type="text/javascript" charset="UTF-8" data-domain-script="fdc74086-f806-4e24-bb1d-5ed8df2ccde9"></script>
	<script type="text/javascript">
		function OptanonWrapper() {}
	</script> -->
	<!-- OneTrust Cookies Consent Notice end for www.bucher-suter.com -->

	<!-- Google Tag Manager -->
	<script>
		(function(w, d, s, l, i) {
			w[l] = w[l] || [];
			w[l].push({
				'gtm.start': new Date().getTime(),
				event: 'gtm.js'
			});
			var f = d.getElementsByTagName(s)[0],
				j = d.createElement(s),
				dl = l != 'dataLayer' ? '&l=' + l : '';
			j.async = true;
			j.src =
				'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
			f.parentNode.insertBefore(j, f);
		})(window, document, 'script', 'dataLayer', 'GTM-MGQVF6S');
	</script>
	<!-- End Google Tag Manager -->

</head>

<body onload="onLoaded()" <?php body_class($mobile); ?>>

	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MGQVF6S" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->


	<?php wp_body_open(); ?>

	<!-- Header -->
	<header id="header">
    <div class="blue-top-bar">
      <?php
        echo get_top_menu();
      ?>
    </div>
		
		<div id="menu-bar">
      <div class="container">
        <div class="logo-and-menu-container">
          <div class="header__logo">
            <?php if (is_page_template('template-simple.php')) : ?>
              <a href="<?= esc_url(home_url('/')) ?>" class="custom-logo-link" rel="home">
                <img src="<?= THEMEPATH . '/assets/img/logo_alt.svg' ?>" class="custom-logo" alt="Bucher + Suter logo" width="115" height="53">
              </a>
            <?php else : ?>
              <?php the_custom_logo(); ?>
            <?php endif; ?>
          </div>
          
          <?php
            wp_nav_menu([
              'theme_location'    => 'megamenu',
              'container'         => 'div',
              'container_id'      => 'mega-menu-container',
              'container_class'   => 'bs-mega-menu-container',
              'menu'              => '310',
              'menu_id'           => 'mega-menu',
              'items_wrap'        => '<ul id="%1$s" class="%2$s">%3$s <li class="blue-top-bar">' . get_top_menu() . '</li><li class="menu-side-links">' . get_menu_side_links() . '</li></ul>',
              'walker' => new BS_Menu_Walker()
            ]);
          ?>
        </div>

        <?php
          $menu_button_icon = get_field('contact_menu_button_menu_icon', 'option');
          $menu_button_link = get_field('contact_menu_button', 'option');
        ?>
        <a class="menu-button" href="<?=$menu_button_link['url']?>">
          <span data-icon="<?=$menu_button_icon?>"></span>
          <?=$menu_button_link['title']?>
        </a>

        <svg 
          id="mobile-menu-toggle" 
          class="<?= is_singular('post') || is_page_template('template-simple.php') ? 'post' : '' ?>" 
          width="40px" 
          height="40px" 
          viewBox="0 0 40 40"
        >
          <g>
            <line x1="0" y1="17" x2="40" y2="17" stroke-width="2" />
            <line x1="0" y1="25" x2="40" y2="25" stroke-width="2" />
          </g>
          <g>
            <line x1="0" y1="20" x2="40" y2="20" stroke-width="2" />
            <line x1="0" y1="20" x2="40" y2="20" stroke-width="2" />
          </g>
        </svg>
      </div>
		</div>

	</header>

	<!-- Main -->
	<main id="main">
