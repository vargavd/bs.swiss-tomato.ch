<?php

// Contact Vars
$contact_title 	   = get_field('contact_title', 'option') ?: esc_html__('Let\'s talk', 'bs');
$contact_url   	   = get_field('contact_url', 'option') ? get_field('contact_url', 'option')['url'] : '';
$contact_url_title = get_field('contact_url', 'option') ? get_field('contact_url', 'option')['title'] : esc_html__('Global 24x7 Customer Service Hotline', 'bs');

// Footer Vars
$footer_logo	   = get_field('footer_logo', 'option') ?: THEMEPATH . '/assets/img/logo_footer.svg';
$footer_text	   = trim(get_field('footer_text', 'option')) ?: '';
$count 		   	   = have_rows('contact_countries', 'option') ? count(get_field('contact_countries', 'option')) : 0;

?>

<?php if (!is_singular('jobs')) : ?>
	<!-- Footer Contact -->
	<section id="contact" class="contact">

		<?php if (trim(have_rows('contact_countries', 'option'))) : ?>
			<div class="contact__maps">
				<?php

				// Loop Contact Countries
				$x = 0;
				while (have_rows('contact_countries', 'option')) : the_row();
					$x++;
					// Vars
					$is_active 		= $x === 1 ? ' active' : '';
					$contact_map	 = get_sub_field('contact_map') ?: '';
					$contact_country = trim(get_sub_field('contact_country')) ?: '';

					// Check country exists
					if ($contact_map) : ?>
						<?= custom_image_size($contact_map, 'full', $contact_country, 'contact__map tab__' . $x . $is_active, '', 'none') ?>
					<?php endif; ?>
				<?php endwhile; ?>
			</div>
		<?php endif; ?>

		<div class="inner narrow grid contact__grid col__2 gap__2">

			<!-- Contact Contents -->
			<div class="contact__contents">

				<!-- Contact Header -->
				<header class="contact__header">
					<!-- Contact Title -->
					<h2 class="big__title contact__title"><?= $contact_title ?></h2>
					<?php if ($contact_url) : ?>
						<a href="<?= esc_url($contact_url) ?>" class="contact__url custom__link"><?= $contact_url_title ?></a>
					<?php endif; ?>
				</header>

				<?php if (trim(have_rows('contact_countries', 'option'))) : ?>
					<div class="contact__countries">

						<?php if ($count > 1) : ?>
							<div class="contact__tabs">
								<?php

								// Loop Contact Countries
								$x = 0;
								while (have_rows('contact_countries', 'option')) : the_row();
									$x++;
									// Vars
									$is_active 		= $x === 1 ? ' active' : '';
									$contact_country = trim(get_sub_field('contact_country')) ?: '';

									// Check country exists
									if ($contact_country) : ?>
										<h3 class="contact__links contact__country<?= $is_active ?>" onclick="openTab(event, 'tab__<?= $x ?>', 'contact__container', 'contact__links', 'contact__map')"><?= $contact_country ?></h3>
									<?php endif; ?>
								<?php endwhile; ?>
							</div>
						<?php endif; ?>
						<?php

						// Loop Contact Countries
						$x = 0;
						while (have_rows('contact_countries', 'option')) : the_row();
							$x++;

							// Vars
							$is_active 		 = $x === 1 ? ' active' : '';
							$contact_address = trim(get_sub_field('contact_address')) ?: '';
							$contact_phone 	 = trim(get_sub_field('contact_phone')) ?: '';
							$contact_email 	 = trim(get_sub_field('contact_email')) ?: '';

							// If any of address type fields are filled
							if ($contact_address || $contact_phone || $contact_email) : ?>
								<address class="contact__container<?= $is_active ?>" data-tab="tab__<?= $x ?>">
									<?php if ($contact_address) : ?>
										<p class="contact__address"><?= $contact_address ?></p>
									<?php endif; ?>
									<?php if ($contact_phone) : ?>
										<div>
											<a href="<?= esc_url('tel:' . clean_phone($contact_phone)) ?>" class="contact__phone read__more white"><?= $contact_phone ?></a>
										</div>
									<?php endif; ?>
									<?php if ($contact_email) : ?>
										<a href="<?= esc_url('mailto:' . antispambot($contact_email)) ?>" class="contact__email read__more white"><?= antispambot($contact_email) ?></a>
									<?php endif; ?>
								</address>
							<?php endif; ?>
						<?php endwhile; ?>
					</div>
				<?php endif; ?>
			</div>

			<!-- Contact Form -->
			<div id="contact__form">
        <?php
          $footer_form = get_field('footer_form', 'option');

          if ($footer_form === 'Contact Form 7') {
            print do_shortcode(get_field('contact_form_7_shortcode', 'option'));
          } else {
            print get_field('form_html_field', 'option');
          }
        ?>

				<?php if (get_field('social_media', 'option')) { ?>
					<!-- Contact Social -->
					<div class="contact__social">
						<!-- Social Icons -->
						<?= get_template_part('parts/components/social'); ?>
					</div>
				<?php } ?>
			</div>
		</div>

		<!-- Contact Cities -->
		<div id="contact__cities" class="contact__cities">
			<div class="inner">
				<?php if (trim(have_rows('contact_countries', 'option'))) : ?>

					<ul class="contact__city__container grid col__3">

						<?php

						// Loop Contact Countries
						$x = 0;
						while (have_rows('contact_countries', 'option')) : the_row();
							$x++;

							// Vars
							$contact_city_img = get_sub_field('contact_city_img') ?: THEMEPATH . '/assets/img/video_bg.jpg';
							$contact_country  = trim(get_sub_field('contact_country')) ?: '';
							$contact_city	  = trim(get_sub_field('contact_city')) ?: '';
							$contact_phone	  = trim(get_sub_field('contact_phone')) ?: '';
							$contact_email	  = trim(get_sub_field('contact_email')) ?: '';

							// If any of address type fields are filled
							if ($contact_city || $contact_phone || $contact_email) : ?>
								<li class="contact__city">
									<?php if ($contact_city_img) : ?>
										<div class="contact__city__bg">
											<?= custom_image_size($contact_city_img, 'medium', $contact_city . ', ' . $contact_country, 'contact__city__img') ?>
										</div>
									<?php endif; ?>

									<div class="contact__city__helper">
										<div class="contact__city__content">
											<h3 class="contact__city__title"><?= $contact_city . ', ' . $contact_country ?></h3>
											<div class="contact__city__text">
												<?php if ($contact_phone) : ?>
													<div>
														<a href="<?= esc_url('tel:' . clean_phone($contact_phone)) ?>" class="contact__phone read__more white"><?= $contact_phone ?></a>
													</div>
												<?php endif; ?>
												<?php if ($contact_email) : ?>
													<a href="<?= esc_url('mailto:' . antispambot($contact_email)) ?>" class="contact__email read__more white"><?= antispambot($contact_email) ?></a>
												<?php endif; ?>
											</div>
										</div>
									</div>
								</li>
							<?php endif; ?>
						<?php endwhile; ?>

					</ul>

				<?php endif; ?>
			</div>
		</div>

	</section>
<?php endif; ?>

</main>

<!-- Footer -->
<footer id="footer">

	<div class="grid footer__grid inner">

		<div class="footer__logo__container">
			<!-- Footer Logo -->
			<div class="footer__logo">
				<a href="<?= site_url() ?>" class="custom-logo-link" rel="home" aria-current="page">
					<img loading="lazy" src="<?= esc_url($footer_logo) ?>" alt="<?= get_bloginfo('name'); ?>" class="custom-logo" width="267" height="150">
				</a>
			</div>
		</div>

		<div class="footer__menu">
			<ul id="menu-footer-menu" class="footer__menu__list">
				<?php

				// Footer Menu
				wp_nav_menu([
					'theme_location'    => 'footermenu',
					'menu'              => 'footer__menu__list',
					'menu_class' 	 	=> 'footer__menu__list',
					'items_wrap' 		=> '%3$s',
					'container'         => '',
					'container_id'      => '',
				]);
				?>
				<li>
					<!-- OneTrust Cookies Settings button start -->
					<button id="ot-sdk-btn" class="ot-sdk-show-settings">Cookie Settings</button>
					<!-- OneTrust Cookies Settings button end -->
				</li>
			</ul>
		</div>

		<?php /* ?>
			<div class="footer__menu">
				<!-- Footer Contact Title -->
				<h4 class="footer__menu__title"><?= esc_html('Sitemap', 'bs') ?></h4>

				<!-- Main Menu -->
				<?php

				// Main Menu
				wp_nav_menu([
					'theme_location' => 'mainmenu',
					'menu'           => 'primary',
					'menu_id' 		 => 'footer__menu',
					'menu_class' 	 => 'footer__menu__ul',
					'container'      => '',
					'container_id'   => '',
				]);
				?>
			</div>
			<?php */ ?>

		<?php /* if (trim(have_rows('contact_countries', 'option'))) : ?>
				<!-- Footer Contact Countries -->
				<div class="footer__contact__countries">
					<!-- Footer Contact Title -->
					<h4 class="footer__contact__title"><?= esc_html('Contact', 'bs') ?></h4>
					<?php if ($count > 1) : ?>
						<div class="footer__contact__tabs">
							<?php

							// Loop Contact Countries
							$x = 0;
							while (have_rows('contact_countries', 'option')) : the_row();
								$x++;
								// Vars
								$is_active 		= $x === 1 ? ' active' : '';
								$contact_country = trim(get_sub_field('contact_country')) ?: '';

								// Check country exists
								if ($contact_country) : ?>
									<h4 class="footer__contact__links footer__contact__country<?= $is_active ?>" onclick=" openTab(event, 'tab__<?= $x ?>', 'footer__contact__container', 'footer__contact__links')"><?= $contact_country ?></h4>
								<?php endif; ?>
							<?php endwhile; ?>
						</div>
					<?php endif; ?>
					<?php

					// Loop Contact Countries
					$x = 0;
					while (have_rows('contact_countries', 'option')) : the_row();
						$x++;

						// Vars
						$is_active 		 = $x === 1 ? ' active' : '';
						$contact_address = trim(get_sub_field('contact_address')) ?: '';
						$contact_phone 	 = trim(get_sub_field('contact_phone')) ?: '';
						$contact_email 	 = trim(get_sub_field('contact_email')) ?: '';

						// If any of address type fields are filled
						if ($contact_address || $contact_phone || $contact_email) : ?>
							<address class="footer__contact__container<?= $is_active ?>" data-tab="tab__<?= $x ?>">
								<?php if ($contact_address) : ?>
									<p class="footer__contact__address"><?= $contact_address ?></p>
								<?php endif; ?>
								<?php if ($contact_phone) : ?>
									<div>
										<a href="<?= esc_url('tel:' . clean_phone($contact_phone)) ?>" class="footer__contact__phone read__more dark"><?= $contact_phone ?></a>
									</div>
								<?php endif; ?>
								<?php if ($contact_email) : ?>
									<a href="<?= esc_url('mailto:' . antispambot($contact_email)) ?>" class="footer__contact__email read__more dark"><?= antispambot($contact_email) ?></a>
								<?php endif; ?>
							</address>
						<?php endif; ?>
					<?php endwhile; ?>
				</div>
			<?php endif; */ ?>

		<div class="footer__language">

			<?php

			// WPML is active?
			if (function_exists('icl_object_id')) : ?>
				<?= custom_languages_switcher(); ?>
			<?php endif; ?>

			<?php if ($footer_text) : ?>
				<p class="footer__copyrights">&copy <?= date('Y') . ' ' .  $footer_text ?></p>
			<?php endif; ?>

		</div>

	</div>

</footer>

<!-- Back To Top -->
<div id="back__to__top"></div>

<!-- Loading -->
<?php if (is_front_page()) : ?>
	<div id="page__loading__box"></div>
	<div id="page__loading__logo">
		<div id="page__loading">
			<svg id="page__loading__svg" viewBox="0 0 91 37" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
				<path id="page__loading__svg__left" d="M48.2878772,19.8963464 L2.09610107e-13,19.8963464 L2.09610107e-13,19.8022083 C7.89719881,19.5751696 14.7143436,19.5059505 26.9415011,17.9360606 C33.0293311,17.1552689 37.0823172,15.632448 41.2035026,13.3855951 C43.2313874,12.2808578 46.2878288,10.0436956 47.8174413,8.42812111 C51.6853151,4.3455769 52.4174135,7.10542736e-15 52.4174135,7.10542736e-15 L52.5092738,7.10542736e-15 L48.2878772,19.8963464 Z"></path>
				<path id="page__loading__svg__right" d="M48.2878772,19.8963464 C48.2878772,19.8963464 51.2775112,19.8963464 90.021664,19.8963464 L90.021664,19.9904844 C77.9517824,20.731129 71.8917889,21.0052368 65.7357595,22.2414904 C61.2777532,23.0472011 57.7021622,24.4301993 54.2908061,26.3877162 C51.9400185,27.7361049 49.7492905,29.5482617 48.4562877,30.8343531 C45.4638701,33.830157 44.8904394,36.3580395 44.8904394,36.3580395 L44.7944037,36.3580395 L48.2878772,19.8963464 Z"></path>
			</svg>
		</div>
	</div>
<?php endif; ?>

<?php
// Edit Post / Term Link
if (is_tax()) {
	edit_term_link(__('<span></span>'));
} else {
	edit_post_link(__('<span></span>'));
}
?>

<?php wp_footer(); ?>

</body>

</html>