

class BSMegaMenu {
  // private consts
  private subMenuColors = ['#000e79', '#001193', '#0014ac', '#0017c6', '#001adf', '#001df9', '#132eff', '#2d45ff', '#465bff'];
  private subMenuZIndexes = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1];

  // private DOM
  private $topMenu: JQuery<HTMLUListElement>;
  private $subMenuContainer: JQuery<HTMLDivElement>;
  private $topMenuItems: JQuery<HTMLLIElement>;

  // private helper properties
  private mouseOnTopMenuItem: boolean = false;
  private mouseOnSubMenuContainer: boolean = false;

  // private helper funcs
  private createSubmenuForAnItem(_i: number, elem: Element) {
    const 
      // DOM
      $topMenuItem  = this.$(elem),
      $subMenu      = $topMenuItem.find('.sub-menu').detach(),
      $subMenuItems = $subMenu.find('.menu-item'),
      $link         = $topMenuItem.find('a'),

      // misc
      id       = $topMenuItem.attr('id') as string,
      linkText = $link.text(),

      // CSS properties
      subMenuContainerInnerHeight = this.$subMenuContainer.innerHeight()!;
      
    // setting app attrs
    $subMenu.attr('data-top-menu-id', id);

    // adding sub menu title
    $subMenu.prepend(`<div class="sub-menu-title">${linkText}</div>`);

    // adding columns and move sub menu items into themFigy, 
    $subMenuItems.each((_i, elem: HTMLElement) => {
      let 
        // DOM
        $subMenuItem = this.$(elem),
        $column:JQuery<HTMLElement>,
        
        // misc
        columnAttr  = $subMenuItem.attr('data-item-column'),
        columnClass = `sub-menu-${columnAttr}`;

      // get the column
      $column = $subMenu.find(`.${columnClass}`);
      if ($column.length === 0) {
        $column = this.$('<div>').addClass(`sub-menu-column ${columnClass}`);
        console.log($subMenu, $column);
        $subMenu.append($column);
      }

      // adding the menu item to column
      $subMenuItem.detach().appendTo($column);
    });

    // add sub menu to the container
    this.$subMenuContainer.append($subMenu);

    // sub menu css stuffs
    let
      subMenuOuterHeight = $subMenu.outerHeight()!,
      subMenuStyles      = getComputedStyle($subMenu[0]),
      subMenuVBorder     = +(subMenuStyles.getPropertyValue('border-top-width').replace('px', '')) + +(subMenuStyles.getPropertyValue('border-bottom-width').replace('px', '')),
      subMenuVPadding    = +(subMenuStyles.getPropertyValue('padding-top').replace('px', '')) + +(subMenuStyles.getPropertyValue('padding-bottom').replace('px', ''));

    // setting height for sub menus
    if (subMenuOuterHeight > subMenuContainerInnerHeight) {
      this.$subMenuContainer.height(subMenuOuterHeight);
    } else {
      $subMenu.height(subMenuContainerInnerHeight - subMenuVPadding - subMenuVBorder);
    }
  }
  
  // DOM events
  private topMenuItem_mouseEnter(e: Event) {
    let
      // DOM
      $topMenuItem   = $(e.currentTarget!),
      $subMenus      = this.$subMenuContainer.find('.sub-menu'),
      $targetSubMenu = $subMenus.filter(`[data-top-menu-id="${$topMenuItem.attr('id')}"]`),
      
      // misc
      numberOfSubMenus = $subMenus.length,
      indexOfTargetSM  = $targetSubMenu.index();

    // setting styles for the target sub menu
    $targetSubMenu
      .css({ zIndex: this.subMenuZIndexes[0], backgroundColor: this.subMenuColors[0] })
      .find('*').css('visibility', 'visible');

    // set styles for submenus left to target
    if (indexOfTargetSM > 0) {
      for (let i = indexOfTargetSM-1, k = 1; i > -1; i--, k++) {
        $subMenus.eq(i)
          .css({ zIndex: this.subMenuZIndexes[k], backgroundColor: this.subMenuColors[k] })
          .find('*').css('visibility', 'hidden');
      }
    }

    // set styles for submenus right to target
    if (indexOfTargetSM + 1 < numberOfSubMenus) {
      for (let i = indexOfTargetSM + 1, k = 1; i < numberOfSubMenus; i++, k++) {
        $subMenus.eq(i)
          .css({ zIndex: this.subMenuZIndexes[k], backgroundColor: this.subMenuColors[k] })
          .find('*').css('visibility', 'hidden');;
      }
    }

    // setting z-index for every subMenu
    // $subMenus.each((_i, element: HTMLElement) => {
    //   let $subMenu = $(element);

    //   $subMenu.css('z-index', zIndexToSet);

    //   if ($subMenu.attr('data-top-menu-id') === topMenuItemId) {
    //     increasingZIndex = false;
    //     $subMenu.find('*').css('visibility', 'visible');
    //   } else {
    //     $subMenu.find('*').css('visibility', 'hidden');
    //   }

    //   zIndexToSet = increasingZIndex ? (zIndexToSet+1) : (zIndexToSet-1);
    // });

    // if ((indexOfOpenedSubMenu + 1) < numberOfSubMenus) {
    //   for (let i = indexOfOpenedSubMenu; i > 0; i--) {
    //     this.$subMenuContainer.find(`.sub-menu:nth-child(${i-1})`)
    //   }
    // }

    this.mouseOnTopMenuItem = true;
    this.$subMenuContainer.show();
  }
  private topMenuItem_mouseLeave(_e: Event) {
    this.mouseOnTopMenuItem = false;

    const that = this;

    setTimeout(function () {
      if (!that.mouseOnSubMenuContainer && !that.mouseOnTopMenuItem) {
        //that.$subMenuContainer.hide();
      }
    }, 100);

    // {
    //   width: '100%',
    //   height: '200px',
    //   backgroundColor: 'red',
    //   display: 'none'
    // }
  }
  private subMenuContainer_mouseEnter(_e: Event) {
    this.mouseOnSubMenuContainer = true;
    this.$subMenuContainer.show();
  }
  private subMenuContainer_mouseLeave(_e: Event) {
    this.mouseOnSubMenuContainer = false;

    const that = this;

    setTimeout(function () {
      if (!that.mouseOnTopMenuItem && !that.mouseOnSubMenuContainer) {
        //that.$subMenuContainer.hide();
      }
    }, 100);

    // {
    //   width: '100%',
    //   height: '200px',
    //   backgroundColor: 'red',
    //   display: 'none'
    // }
  }

  constructor(private $:JQueryStatic) {
    // initialize DOM
    this.$subMenuContainer = this.$('#sub-menu-container');
    this.$topMenu = this.$('ul#top-menu');
    this.$topMenuItems = this.$topMenu.find('> li') as JQuery<HTMLLIElement>;

    // DOM
    this.$topMenuItems.on('mouseenter', this.topMenuItem_mouseEnter.bind(this));
    this.$topMenuItems.on('mouseleave', this.topMenuItem_mouseLeave.bind(this));
    this.$subMenuContainer.on('mouseenter', this.subMenuContainer_mouseEnter.bind(this));
    this.$subMenuContainer.on('mouseleave', this.subMenuContainer_mouseLeave.bind(this));

    // create sub menus
    this.$topMenuItems.each(this.createSubmenuForAnItem.bind(this));
    this.$subMenuContainer.css('visibility', 'visible');
    
  }
}


jQuery(function (_$) {
  let
    // DOM
    $window  = $(window),
    $body    = $('body'),
    $menuBar = $('#menu-bar'),
    $mobileMenuToggle = $('#mobile-menu-toggle'),
    $megaMenuContainer = $('#mega-menu-container'),
    $megaMenuMainLinks = $megaMenuContainer.find('a.depth-0'),
    $megaMenuSubLinks = $megaMenuContainer.find('a.depth-1, a.depth-2'),
    $megaMenuSideLinks = $megaMenuContainer.find('a.menu-side-link'),
    $megaMenuTopLinks = $megaMenuContainer.find('.blue-top-bar a'),
    $megaMenuMobileBackLinks = $megaMenuContainer.find('.mobile-back'),
    $wpmlCurrentLangA = $('li.wpml-ls-current-language > a'),
    $whiteBCMenus = $megaMenuContainer.find('.white-bc-menu'),

    // misc
    previousScrollTop = 0,
    currentMenuBarClass = '',
    isHome = $body.hasClass('home'),

    // event
    browserScrolled = function () {
      var
        currentScrollTop = $window.scrollTop()!;

      currentMenuBarClass = previousScrollTop > currentScrollTop ? 'up' : 'down';

      if ((currentScrollTop > 77 && currentMenuBarClass === 'up') ||
          (currentScrollTop > 140 && currentMenuBarClass === 'down')) {
        currentMenuBarClass += " fixed-scrollable";

        if (previousScrollTop > currentScrollTop) {
          currentMenuBarClass += " visible";
        }
      }

      

      // if (
      //   (currentScrollTop <= 600 && isHome) ||
      //   (currentScrollTop <= 300 && !isHome)
      // ) {
      //   currentMenuBarClass = 'fixed-top-0';
      // } else {
        
      // }

      

      $menuBar.attr('class', currentMenuBarClass);
      
      previousScrollTop = currentScrollTop;
    },
    mobileMenuToggleClicked = function (e?: JQuery.ClickEvent) {
      if (!$mobileMenuToggle.is(':visible')) {
        return;
      }
      
      let mobileMenuActive = $mobileMenuToggle.hasClass('active');

      $mobileMenuToggle.toggleClass('active', !mobileMenuActive);
      $megaMenuContainer.toggleClass('opened', !mobileMenuActive);
      $body.toggleClass('menu__active', !mobileMenuActive);

      if (e) {
        e.preventDefault();
        e.stopPropagation();
      }

      setTimeout(function () {
        $whiteBCMenus.find('.white-bc-content').scrollTop(0);
        $whiteBCMenus.removeClass('opened');
      }, 400);
    },
    mobileMenuMainAClicked = function (e: JQuery.ClickEvent) {
      let
        // DOM
        $link = $(e.currentTarget),
        $menuItem = $link.closest('.menu-item'),
        $whiteBCMenu = $menuItem.find('.white-bc-menu');

      e.preventDefault();

      $whiteBCMenu.addClass('opened');

      console.log($whiteBCMenu);
    },
    mobileMenuSubAClicked = function (e: JQuery.ClickEvent) {
      let
        // DOM
        $link = $(e.currentTarget),

        // MISC
        href = $link.attr('href') as string;

      if (!href || href === '#') {
        e.preventDefault();
        e.stopPropagation();
        return;
      }

      mobileMenuToggleClicked();
    },
    mobileMenuSideAClicked = function (e: JQuery.ClickEvent) {
      let
        // DOM
        $link = $(e.currentTarget),
        $parentLi = $link.closest('li');
      
      if ($parentLi.hasClass('wpml-ls-current-language')) {
        return;
      }

      mobileMenuToggleClicked();
    },
    mobileBackLinkClicked = function (this: JQuery<HTMLDivElement>) {
      let
        // DOM
        $mobileBack = $(this),
        $whiteBCMenu = $mobileBack.closest('.white-bc-menu');

      $whiteBCMenu.removeClass('opened');
    },
    mobileWPMLMenuClicked = function (e:JQuery.ClickEvent) {
      e.preventDefault();
    },
    mouseLeaveParentLi = function (e: JQuery.MouseLeaveEvent) {
      let
        // DOM
        $li = $(e.currentTarget),
        $subMenuContent = $li.find('.sub-menu-content');

      $subMenuContent.scrollTop(0);
    },
    bodyClicked = function () {
      $mobileMenuToggle.toggleClass('active', false);
      $megaMenuContainer.toggleClass('opened', false);
      $body.toggleClass('menu__active', false);

      $megaMenuContainer.find('.white-bc-menu.opened').removeClass('opened');
    },
    clickEventInMegaMenuContainerBar = function (e: JQuery.ClickEvent) {
      e.stopPropagation();
    };

  $window.on('scroll', browserScrolled);
  $mobileMenuToggle.on('click', mobileMenuToggleClicked);
  $megaMenuMainLinks.on('click', mobileMenuMainAClicked);
  $megaMenuSubLinks.on('click', mobileMenuSubAClicked);
  $megaMenuMobileBackLinks.on('click', mobileBackLinkClicked);
  $megaMenuSideLinks.on('click', mobileMenuSideAClicked);
  $megaMenuTopLinks.on('click', mobileMenuSideAClicked);
  $wpmlCurrentLangA.on('click', mobileWPMLMenuClicked);
  $megaMenuMainLinks.closest('li').on('mouseleave', mouseLeaveParentLi);
  $body.on('click', bodyClicked);
  $megaMenuContainer.on('click', clickEventInMegaMenuContainerBar);

  browserScrolled();

  // let the dance begin
  // new BSMegaMenu(_$);
});
