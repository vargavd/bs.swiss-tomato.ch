"use strict";
var BSMegaMenu = (function () {
    function BSMegaMenu($) {
        this.$ = $;
        this.subMenuColors = ['#000e79', '#001193', '#0014ac', '#0017c6', '#001adf', '#001df9', '#132eff', '#2d45ff', '#465bff'];
        this.subMenuZIndexes = [10, 9, 8, 7, 6, 5, 4, 3, 2, 1];
        this.mouseOnTopMenuItem = false;
        this.mouseOnSubMenuContainer = false;
        this.$subMenuContainer = this.$('#sub-menu-container');
        this.$topMenu = this.$('ul#top-menu');
        this.$topMenuItems = this.$topMenu.find('> li');
        this.$topMenuItems.on('mouseenter', this.topMenuItem_mouseEnter.bind(this));
        this.$topMenuItems.on('mouseleave', this.topMenuItem_mouseLeave.bind(this));
        this.$subMenuContainer.on('mouseenter', this.subMenuContainer_mouseEnter.bind(this));
        this.$subMenuContainer.on('mouseleave', this.subMenuContainer_mouseLeave.bind(this));
        this.$topMenuItems.each(this.createSubmenuForAnItem.bind(this));
        this.$subMenuContainer.css('visibility', 'visible');
    }
    BSMegaMenu.prototype.createSubmenuForAnItem = function (_i, elem) {
        var _this = this;
        var $topMenuItem = this.$(elem), $subMenu = $topMenuItem.find('.sub-menu').detach(), $subMenuItems = $subMenu.find('.menu-item'), $link = $topMenuItem.find('a'), id = $topMenuItem.attr('id'), linkText = $link.text(), subMenuContainerInnerHeight = this.$subMenuContainer.innerHeight();
        $subMenu.attr('data-top-menu-id', id);
        $subMenu.prepend("<div class=\"sub-menu-title\">".concat(linkText, "</div>"));
        $subMenuItems.each(function (_i, elem) {
            var $subMenuItem = _this.$(elem), $column, columnAttr = $subMenuItem.attr('data-item-column'), columnClass = "sub-menu-".concat(columnAttr);
            $column = $subMenu.find(".".concat(columnClass));
            if ($column.length === 0) {
                $column = _this.$('<div>').addClass("sub-menu-column ".concat(columnClass));
                console.log($subMenu, $column);
                $subMenu.append($column);
            }
            $subMenuItem.detach().appendTo($column);
        });
        this.$subMenuContainer.append($subMenu);
        var subMenuOuterHeight = $subMenu.outerHeight(), subMenuStyles = getComputedStyle($subMenu[0]), subMenuVBorder = +(subMenuStyles.getPropertyValue('border-top-width').replace('px', '')) + +(subMenuStyles.getPropertyValue('border-bottom-width').replace('px', '')), subMenuVPadding = +(subMenuStyles.getPropertyValue('padding-top').replace('px', '')) + +(subMenuStyles.getPropertyValue('padding-bottom').replace('px', ''));
        if (subMenuOuterHeight > subMenuContainerInnerHeight) {
            this.$subMenuContainer.height(subMenuOuterHeight);
        }
        else {
            $subMenu.height(subMenuContainerInnerHeight - subMenuVPadding - subMenuVBorder);
        }
    };
    BSMegaMenu.prototype.topMenuItem_mouseEnter = function (e) {
        var $topMenuItem = $(e.currentTarget), $subMenus = this.$subMenuContainer.find('.sub-menu'), $targetSubMenu = $subMenus.filter("[data-top-menu-id=\"".concat($topMenuItem.attr('id'), "\"]")), numberOfSubMenus = $subMenus.length, indexOfTargetSM = $targetSubMenu.index();
        $targetSubMenu
            .css({ zIndex: this.subMenuZIndexes[0], backgroundColor: this.subMenuColors[0] })
            .find('*').css('visibility', 'visible');
        if (indexOfTargetSM > 0) {
            for (var i = indexOfTargetSM - 1, k = 1; i > -1; i--, k++) {
                $subMenus.eq(i)
                    .css({ zIndex: this.subMenuZIndexes[k], backgroundColor: this.subMenuColors[k] })
                    .find('*').css('visibility', 'hidden');
            }
        }
        if (indexOfTargetSM + 1 < numberOfSubMenus) {
            for (var i = indexOfTargetSM + 1, k = 1; i < numberOfSubMenus; i++, k++) {
                $subMenus.eq(i)
                    .css({ zIndex: this.subMenuZIndexes[k], backgroundColor: this.subMenuColors[k] })
                    .find('*').css('visibility', 'hidden');
                ;
            }
        }
        this.mouseOnTopMenuItem = true;
        this.$subMenuContainer.show();
    };
    BSMegaMenu.prototype.topMenuItem_mouseLeave = function (_e) {
        this.mouseOnTopMenuItem = false;
        var that = this;
        setTimeout(function () {
            if (!that.mouseOnSubMenuContainer && !that.mouseOnTopMenuItem) {
            }
        }, 100);
    };
    BSMegaMenu.prototype.subMenuContainer_mouseEnter = function (_e) {
        this.mouseOnSubMenuContainer = true;
        this.$subMenuContainer.show();
    };
    BSMegaMenu.prototype.subMenuContainer_mouseLeave = function (_e) {
        this.mouseOnSubMenuContainer = false;
        var that = this;
        setTimeout(function () {
            if (!that.mouseOnTopMenuItem && !that.mouseOnSubMenuContainer) {
            }
        }, 100);
    };
    return BSMegaMenu;
}());
jQuery(function (_$) {
    var $window = $(window), $body = $('body'), $menuBar = $('#menu-bar'), $mobileMenuToggle = $('#mobile-menu-toggle'), $megaMenuContainer = $('#mega-menu-container'), $megaMenuMainLinks = $megaMenuContainer.find('a.depth-0'), $megaMenuSubLinks = $megaMenuContainer.find('a.depth-1, a.depth-2'), $megaMenuSideLinks = $megaMenuContainer.find('a.menu-side-link'), $megaMenuTopLinks = $megaMenuContainer.find('.blue-top-bar a'), $megaMenuMobileBackLinks = $megaMenuContainer.find('.mobile-back'), $wpmlCurrentLangA = $('li.wpml-ls-current-language > a'), $whiteBCMenus = $megaMenuContainer.find('.white-bc-menu'), previousScrollTop = 0, currentMenuBarClass = '', isHome = $body.hasClass('home'), browserScrolled = function () {
        var currentScrollTop = $window.scrollTop();
        currentMenuBarClass = previousScrollTop > currentScrollTop ? 'up' : 'down';
        if ((currentScrollTop > 77 && currentMenuBarClass === 'up') ||
            (currentScrollTop > 140 && currentMenuBarClass === 'down')) {
            currentMenuBarClass += " fixed-scrollable";
            if (previousScrollTop > currentScrollTop) {
                currentMenuBarClass += " visible";
            }
        }
        $menuBar.attr('class', currentMenuBarClass);
        previousScrollTop = currentScrollTop;
    }, mobileMenuToggleClicked = function (e) {
        if (!$mobileMenuToggle.is(':visible')) {
            return;
        }
        var mobileMenuActive = $mobileMenuToggle.hasClass('active');
        $mobileMenuToggle.toggleClass('active', !mobileMenuActive);
        $megaMenuContainer.toggleClass('opened', !mobileMenuActive);
        $body.toggleClass('menu__active', !mobileMenuActive);
        if (e) {
            e.preventDefault();
            e.stopPropagation();
        }
        setTimeout(function () {
            $whiteBCMenus.find('.white-bc-content').scrollTop(0);
            $whiteBCMenus.removeClass('opened');
        }, 400);
    }, mobileMenuMainAClicked = function (e) {
        var $link = $(e.currentTarget), $menuItem = $link.closest('.menu-item'), $whiteBCMenu = $menuItem.find('.white-bc-menu');
        e.preventDefault();
        $whiteBCMenu.addClass('opened');
        console.log($whiteBCMenu);
    }, mobileMenuSubAClicked = function (e) {
        var $link = $(e.currentTarget), href = $link.attr('href');
        if (!href || href === '#') {
            e.preventDefault();
            e.stopPropagation();
            return;
        }
        mobileMenuToggleClicked();
    }, mobileMenuSideAClicked = function (e) {
        var $link = $(e.currentTarget), $parentLi = $link.closest('li');
        if ($parentLi.hasClass('wpml-ls-current-language')) {
            return;
        }
        mobileMenuToggleClicked();
    }, mobileBackLinkClicked = function () {
        var $mobileBack = $(this), $whiteBCMenu = $mobileBack.closest('.white-bc-menu');
        $whiteBCMenu.removeClass('opened');
    }, mobileWPMLMenuClicked = function (e) {
        e.preventDefault();
    }, mouseLeaveParentLi = function (e) {
        var $li = $(e.currentTarget), $subMenuContent = $li.find('.sub-menu-content');
        $subMenuContent.scrollTop(0);
    }, bodyClicked = function () {
        $mobileMenuToggle.toggleClass('active', false);
        $megaMenuContainer.toggleClass('opened', false);
        $body.toggleClass('menu__active', false);
        $megaMenuContainer.find('.white-bc-menu.opened').removeClass('opened');
    }, clickEventInMegaMenuContainerBar = function (e) {
        e.stopPropagation();
    };
    $window.on('scroll', browserScrolled);
    $mobileMenuToggle.on('click', mobileMenuToggleClicked);
    $megaMenuMainLinks.on('click', mobileMenuMainAClicked);
    $megaMenuSubLinks.on('click', mobileMenuSubAClicked);
    $megaMenuMobileBackLinks.on('click', mobileBackLinkClicked);
    $megaMenuSideLinks.on('click', mobileMenuSideAClicked);
    $megaMenuTopLinks.on('click', mobileMenuSideAClicked);
    $wpmlCurrentLangA.on('click', mobileWPMLMenuClicked);
    $megaMenuMainLinks.closest('li').on('mouseleave', mouseLeaveParentLi);
    $body.on('click', bodyClicked);
    $megaMenuContainer.on('click', clickEventInMegaMenuContainerBar);
    browserScrolled();
});
//# sourceMappingURL=mega-menu.js.map