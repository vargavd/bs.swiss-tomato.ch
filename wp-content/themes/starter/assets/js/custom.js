"use strict"; // #region Debounce Function

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function debounce(func, wait, immediate) {
  var timeout;
  return function executedFunction() {
    var context = this,
        args = arguments;

    var later = function later() {
      timeout = null;
      if (!immediate) func.apply(context, args);
    };

    var callNow = immediate && !timeout;
    clearTimeout(timeout);
    timeout = setTimeout(later, wait);
    if (callNow) func.apply(context, args);
  };
} // #endregion
// #region Splitting Function


var splitText = /*#__PURE__*/function () {
  function splitText() {
    var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    _classCallCheck(this, splitText);

    this.domElement = options.element || "div";
    this.staggering = options.staggering || true;
    this.staggeringAll = options.staggeringAll || false;
    this.delay = options.delay || 0.03;
    this.elements = document.querySelectorAll("[data-split-text]");
    this.counterAll = 0;
    this.start();
  }

  _createClass(splitText, [{
    key: "start",
    value: function start() {
      var _this = this;

      // Create new elements
      this.elements.forEach(function (element) {
        var textInside = element.innerText,
            textLength = textInside.length,
            words = textInside.split(" "),
            wordsLength = words.length,
            newElements = [];
        element.innerHTML = ""; // delete inital text

        var counter = 0;

        for (var _i = 0; _i < wordsLength; _i++) {
          counter++;
          _this.counterAll++;
          var newElement = document.createElement(_this.domElement),
              wordText = words[_i],
              characters = [];
          var whiteSpace = void 0;

          for (var _j = 0; _j < words[_i].length; _j++) {
            counter++;
            _this.counterAll++;
            characters[_j] = document.createElement(_this.domElement);
            characters[_j].innerHTML = words[_i][_j];

            characters[_j].classList.add("split__char"); // Make it invisible for screen-readers.


            characters[_j].setAttribute("aria-hidden", "true");

            if (_this.staggering) {
              characters[_j].style.animationDelay = _this.delay * counter + "s";
            }

            if (_this.staggeringAll) {
              characters[_j].style.animationDelay = _this.delay * _this.counterAll + "s";
            }

            newElement.appendChild(characters[_j]);
          }

          newElement.classList.add("split__word"); // Word for screenreaders

          newElement.setAttribute("aria-label", wordText);
          newElements.push(newElement);

          if (_i < wordsLength - 1) {
            whiteSpace = document.createElement(_this.domElement);
            whiteSpace.innerHTML = " ";
            whiteSpace.classList.add("split__whitespace");
            newElements.push(whiteSpace);
          }
        }

        newElements.forEach(function (arrElement) {
          element.appendChild(arrElement);
        });
      });
    }
  }]);

  return splitText;
}();

new splitText(); // #endregion
// #region Custom Select Function

var x, i, j, l, ll, selEl, a, b, c, u;
x = document.getElementsByClassName("select__custom");
l = x.length;

for (i = 0; i < l; i++) {
  selEl = x[i].getElementsByTagName("select")[0];
  ll = selEl.length; // For each element create a new DIV that will act as the {selected item}

  a = document.createElement("DIV");
  a.setAttribute("class", "select__selected");
  a.innerHTML = selEl.options[selEl.selectedIndex].innerHTML;
  u = x[i].getAttribute("data-placeholder");
  u === a.innerHTML ? x[i].classList.add("placeholder") : x[i].classList.remove("placeholder");
  x[i].appendChild(a); // For each element create a new DIV that will contain the {option list}

  b = document.createElement("DIV");
  b.setAttribute("class", "select__items select__hide");

  for (j = 1; j < ll; j++) {
    // For each element create a new DIV that will act as an {option item}
    c = document.createElement("DIV");
    c.innerHTML = selEl.options[j].innerHTML;
    c.addEventListener("click", function (e) {
      // When an item is clicked, update the original select box, and the selected item
      var y, i, k, s, h, sl, yl, d;
      s = this.parentNode.parentNode.getElementsByTagName("select")[0];
      sl = s.length;
      h = this.parentNode.previousSibling;
      d = this.parentNode.parentNode.getAttribute("data-placeholder");

      for (i = 0; i < sl; i++) {
        if (s.options[i].innerHTML == this.innerHTML) {
          s.selectedIndex = i;
          h.innerHTML = this.innerHTML; // Selected = data-placeholder?

          d === h.innerHTML ? h.parentNode.classList.add("placeholder") : h.parentNode.classList.remove("placeholder");
          y = this.parentNode.getElementsByClassName("select__same");
          yl = y.length;

          for (k = 0; k < yl; k++) {
            y[k].removeAttribute("class");
          }

          this.classList.add("select__same");
          break;
        }
      }

      h.click();
    });
    b.appendChild(c);
  }

  x[i].appendChild(b); // When the select box is clicked, close any other select boxes, and open/close the current

  a.addEventListener("click", function (e) {
    e.stopPropagation();
    closeAllSelect(this);
    this.parentElement.classList.add("focus");
    this.nextSibling.classList.toggle("select__hide");
    this.classList.toggle("select__arrow--active");
  });
} // Close all select boxes, except the current


function closeAllSelect(elmnt) {
  var a,
      x,
      y,
      i,
      xl,
      yl,
      arrNo = [];
  a = document.getElementsByClassName("select__custom");
  x = document.getElementsByClassName("select__items");
  y = document.getElementsByClassName("select__selected");
  xl = x.length;
  yl = y.length;

  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i);
    } else {
      y[i].classList.remove("select__arrow--active");
      a[i].classList.remove("focus");
    }
  }

  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select__hide");
    }
  }
} // User clicks outside the select box > close all


document.addEventListener("click", closeAllSelect); // #endregion
// #region Sticky Header Function

window.onscroll = function () {
  scrollFunction();
};

function scrollFunction() {
  if (document.body.scrollTop > 50 || document.documentElement.scrollTop > 50) {
    document.body.classList.add("sticky");
  } else {
    document.body.classList.remove("sticky");
  }
} // #endregion
// #region Scrollbar Width


function getScrollbarWidth() {
  var scrollbarWidth = window.innerWidth - document.documentElement.clientWidth;
  return scrollbarWidth;
} // #endregion
// #region onLoaded Function


function onLoaded() {
  document.body.classList.add("loaded");
} // #endregion
// #region addListenerMulti

/**
 * Add one or more listeners to an element
 *
 * @param {DOMElement} element - DOM element to add listeners to
 * @param {string} eventNames - space separated list of event names, e.g. 'click change'
 * @param {Function} listener - function to attach for each event as a listener
 */


function addListenerMulti(element, eventNames, listener) {
  var events = eventNames.split(" ");

  for (var i = 0, iLen = events.length; i < iLen; i++) {
    element.addEventListener(events[i], listener, false);
  }
} // #endregion
// #region Tab Function

/**
 * Tab function
 */


function openTab(evt, tabName, tabContentClass, tabLinksClass) {
  var tabMapClass = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : "";
  var i, tabcontent, tablinks, tabmaps;
  tabcontent = document.getElementsByClassName(tabContentClass);

  for (i = 0; i < tabcontent.length; i++) {
    if (tabName === tabcontent[i].dataset.tab) {
      if (!tabcontent[i].classList.contains("active")) {
        tabcontent[i].className += " active";
      }
    } else {
      tabcontent[i].className = tabcontent[i].className.replace(" active", "");
    }
  }

  if (tabMapClass !== "") {
    tabmaps = document.getElementsByClassName(tabMapClass);

    for (i = 0; i < tabmaps.length; i++) {
      if (tabmaps[i].classList.contains(tabName)) {
        if (!tabmaps[i].classList.contains("active")) {
          tabmaps[i].className += " active";
        }
      } else {
        tabmaps[i].className = tabmaps[i].className.replace(" active", "");
      }
    }
  }

  tablinks = document.getElementsByClassName(tabLinksClass);

  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }

  evt.currentTarget.className += " active";
} // #endregion
// #region Item Height Function

/**
 * Set Title height as CSS variable
 */


window.addListenerMulti(window, "load resize", function (event) {
  // Vars
  var cityContent = document.getElementsByClassName("contact__city__content"),
      cityTitle = document.getElementsByClassName("contact__city__title");

  for (i = 0; i < cityContent.length; i++) {
    cityContent[i].setAttribute("style", "--title-height:" + cityTitle[i].offsetHeight);
  }
}, true); // #endregion
// #region getUrlParameter

var getUrlParameter = function getUrlParameter(sParam) {
  var sPageURL = window.location.search.substring(1),
      sURLVariables = sPageURL.split("&"),
      sParameterName,
      i;

  for (i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split("=");

    if (sParameterName[0] === sParam) {
      return _typeof(sParameterName[1]) === undefined ? true : decodeURIComponent(sParameterName[1]);
    }
  }

  return false;
}; // #endregion

/**
 * Custom jQuery functions
 * @description Theme default jQuery functions
 */


jQuery(function () {
  var BURGER = $("#burger__menu"),
      NAVCLOSE = $("#burger__menu--close"),
      CURRENTMENU = $("li.current-menu-item"),
      BACKTOTOP = $("#back__to__top"),
      LOGGEDIN = $(".logged__in"),
      SUBMNEU = $(".sub-menu-toggle"),
      COLUMNMORE = $(".column__more"),
      DOCUMENT = $(document),
      BODY = $(document.body),
      INPUT = $("input"); // #region Click Functions

  /** Burger icon */

  BURGER.on("click", function () {
    BODY.toggleClass("menu__active");
    /*$("#nav").css("margin-right", SCROLLWIDTH);
    		if (!BODY.hasClass("menu__active")) {
    	$("#nav").removeAttr("style");
    }*/
  });
  /** Prevent useless  */

  CURRENTMENU.on("click", function (e) {
    e.preventDefault();
    BODY.removeClass("menu__active");
  });
  /** Sub menu */

  SUBMNEU.on("click", function () {
    $(this).attr("aria-expanded", function (i, val) {
      return val == "false" ? true : false;
    });
    $(this).next("ul").slideToggle("fast");
    $(this).next("ul").toggleClass("active");
  });
  /** Login menu */

  LOGGEDIN.on("click", function () {
    $("#profile__menu").toggleClass("active");
  });
  /** Nav close */

  NAVCLOSE.on("click", function () {
    BODY.removeClass("menu__active");
  });
  /** Top */

  BACKTOTOP.on("click", function () {
    $("body,html").animate({
      scrollTop: 0
    }, 800);
    return false;
  });
  /** More */

  COLUMNMORE.on("click", function (e) {
    e.preventDefault(); // Vars

    var ez = $(this),
        item = $(".column__item");

    if (!ez.parent().hasClass("active")) {
      item.removeClass("active");
      COLUMNMORE.text("Read more");
    }

    ez.parent().toggleClass("active");
    ez.text() === "Read more" ? ez.text("Read less") : ez.text("Read more");
  });
  /** Accordion */

  if ($(".accordion").length) {
    var question = $(".accordion__item__question"),
        answer = $(".accordion__item__answer");
    question.on("click", function () {
      $(this).toggleClass("active");
      $(this).next(answer).slideToggle("fast");
      $(this).parent().siblings().children().next().slideUp("fast");
      $(this).parent().siblings().children(question).removeClass("active");
      return false;
    });
  } // #endregion
  // #region Mouseup Functions

  /** Profile menu */


  DOCUMENT.on("mouseup", function (e) {
    var container = $("#profile__menu");

    if (!$(container).is(e.target) && $(container).has(e.target).length === 0) {
      container.removeClass("active");
      container.off("click", document);
    }
  }); // #endregion
  // #region Keyup Functions

  /** Input focus */

  INPUT.on("keyup", function () {
    var self = $(this);
    self.val() !== "" ? self.addClass("focus") : self.removeClass("focus");
  }); // #endregion
  // #region Touch Functions

  /** IOS Helper */

  var mobileHover = function mobileHover() {
    $("*").on("touchstart", function () {
      $(this).trigger("hover");
    }).on("touchend", function () {
      $(this).trigger("hover");
    });
  };

  mobileHover(); // #endregion
});