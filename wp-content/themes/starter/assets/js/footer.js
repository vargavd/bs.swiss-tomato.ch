jQuery(document).ready(function($) {
  const scrollAnimations = sal({
    thresold: 1,
  });

  scrollAnimations.enable();

  // If contains Hero Video Block
  if (document.querySelector('#video__hero__header')) {
    Splitting();
  }

  lity.handlers("video", function(target) {
    if (typeof target === "string" && target.indexOf(".mp4") > 0) {
      var html =
        '<video style="width: 100%;height:100%" autoplay playsinline controls disablePictureInPicture controlslist="nodownload">';
      html += '<source src="' + target + '" type="video/mp4">';
      html += "</video>";
      return html;
    }
    return false;
  });
});