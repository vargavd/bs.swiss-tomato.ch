"use strict";

jQuery(function () {
	// Constants
	const CPT = $("input#cpt").val(),
		SEARCH = $("input#resources__search"),
		CAT = $("h2.post__cat"),
		TABS = $("h2.posts__tab__item"),
		FILTER = $("form#" + CPT + "__filter"),
		RESULT = $("div#" + CPT + "__result"),
		RESET = $("." + CPT + "__reset"),
		SELECTED = $("div.select__items > div");

	// Apply Filter
	FILTER.on("submit", function (e) {
		e.preventDefault();

		// Vars
		let max = $("input#max");

		// AJAX request
		$.ajax({
			url: FILTER.attr("action"),
			data: FILTER.serialize() + "&nonce=" + ajax_filter_loadmore_params.nonce,
			dataType: "json",
			type: FILTER.attr("method"),
			beforeSend: function (xhr) {
				// Add Loading class
				FILTER.addClass("loading");
			},
			success: function (data) {
				// Add animation to Post Items
				setTimeout(function () {
					$("." + CPT + "__item").addClass("sal-animate");
				}, 50);

				// Reset Page Param
				ajax_filter_loadmore_params.current_page = 1;

				// Update Max Page value
				max.val(data.max_page);

				// Remove Loading class
				FILTER.removeClass("loading");

				// Result
				RESULT.html(data.content); // insert new posts
			},
			error: function (request, status, error) {
				alert(request.error);
			},
		});
		return false;
	});

	// Search Function
	function dosearch(keyword) {
		// Var
		let info = $(".resources__search__container");

		if (keyword.length > 0 && keyword.length < 3) {
			info.addClass("error");
		} else {
			info.removeClass("error");
			FILTER.trigger("submit");
		}
	}

	// Debounced Search
	SEARCH.on(
		"keyup",
		debounce(function () {
			let $this = $(this);
			dosearch($this.val());
		}, 500)
	);

	SELECTED.on("click", function () {
		FILTER.trigger("submit");
	});

	// Category Selector
	CAT.on("click", function () {
		// Vars
		let $this = $(this),
			value = $(this).attr("data-cat"),
			cat = $('input[name="cat"]');

		// Check active status
		if (!$this.hasClass("active")) {
			// Remove / Add active class
			TABS.removeClass("active");
			$this.addClass("active");

			// Add value
			cat.val(value);

			// Trigger Submit
			FILTER.trigger("submit");
		}
	});

	// Kill them all
	RESET.on("click", function () {
		// Vars
		let $this = $(this),
			cat = $('input[name="cat"]'),
			info = $(".resources__search__container"),
			allSel = $(".select__custom"),
			allSame = $(".select__custom .select__items > div"),
			allSelected = $(".select__custom .select__selected");

		if (CPT === "resources") {
			info.removeClass("error");

			// Add placeholder class
			allSel.addClass("placeholder");

			// Remove same class
			allSame.removeClass("select__same");

			// Reset all selected items
			allSelected.each(function () {
				let $this = $(this),
					allReset = $this.next().find("div:first").text();
				$this.text(allReset);
			});

			// Reset Filter
			FILTER[0].reset();

			// Filter reset
			FILTER.trigger("submit");
		}

		// Check active status
		if (!$this.hasClass("active")) {
			// Remove / Add active class
			TABS.removeClass("active");
			$this.addClass("active");

			// Reset value
			cat.val("");

			// Trigger Submit
			FILTER.trigger("submit");
		}
	});

	// Load more
	$(document).on("click", "button.load__more", function (e) {
		e.preventDefault();

		// Vars
		let button = $(this),
			cpt = $("#cpt").val(),
			ppp = $("#ppp").val(),
			max = $("#max").val(),
			product = $("#product").val(),
			resourcetype = $("#resource__type").val(),
			search = $("#resources__search").val(),
			items = $("." + cpt + "__container"),
			cat = $("#cat").val(),
			lodamore = $("#loadmore__security").val(),
			data = {
				action: "loadmore",
				cpt: cpt,
				ppp: ppp,
				page: ajax_filter_loadmore_params.current_page,
				cat: cat,
				product: product,
				resource_type: resourcetype,
				resources__search: search,
				loadmore__security: lodamore,
				nonce: ajax_filter_loadmore_params.nonce,
			};

		// AJAX request
		$.ajax({
			url: ajax_filter_loadmore_params.ajaxurl, // AJAX handler
			data: data,
			type: "POST",
			beforeSend: function (xhr) {
				// Loading message
				button.text(ajax_filter_loadmore_params.loading);
			},
			success: function (data) {
				if (data) {
					button.text(ajax_filter_loadmore_params.loadmore);
					items.append(data); // insert new posts
					ajax_filter_loadmore_params.current_page++;

					// Checking
					/*console.log("ppp: " + ppp);
					console.log("cur: " + ajax_filter_loadmore_params.current_page);
					console.log("max: " + max);*/

					// If reach the last page, remove the button
					if (ajax_filter_loadmore_params.current_page == max) {
						button.remove();
					}

					// Add animation to Post Items
					setTimeout(function () {
						$("." + cpt + "__item").addClass("sal-animate");
					}, 50);
				} else {
					// if no data, remove the button as well
					button.remove();
				}
			},
		});
	});
});
