let swiperProvider = {
  scriptAdded: false,
  addSwiperScriptToHead: function () {
    if (!this.scriptAdded) {
      let swiperScript = document.createElement("script");
      swiperScript.src = "/wp-content/themes/starter/assets/js/swiper/swiper.min.js";
      document.head.appendChild(swiperScript);
      this.scriptAdded = true;

      // console.log('swiper has been added to head');
    }
  },
  callMeBackWhenSwiperIsLoaded: function (callback) {
    console.log("Swiper provider has been called");

    if (!this.scriptAdded) {
      this.addSwiperScriptToHead();
    }

    let intervalId = setInterval(function () {
      if (typeof Swiper !== "undefined") {
        callback();
        clearInterval(intervalId);
      }
    }, 200);
  }
};
