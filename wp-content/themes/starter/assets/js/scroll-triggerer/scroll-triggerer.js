jQuery(document).ready(function () {
  let
    // DOM
    scrollTriggerElements = document.querySelectorAll('.scroll-trigger-elem'),

    // EVENTS
    handleIntersection = function (entries) {
      entries.map((entry) => {
        if (entry.isIntersecting) {
          // Item has crossed our observation
          entry.target.classList.add('scrolled-to');
          // Job done for this item - no need to watch it!
          observer.unobserve(entry.target);
        }
      });
    },
    
    // MISC
    observer = new IntersectionObserver(handleIntersection);

    scrollTriggerElements.forEach(element => observer.observe(element));
});