jQuery(document).ready(function ($) {
  let
    // DOM
    scrollTriggerElements = document.querySelectorAll('.cases .swiper-container'),

    // EVENTS
    handleIntersection = function (entries) {
      entries.map(entry => {
        if (!entry.isIntersecting) {
          return;
        }

        swiperProvider.callMeBackWhenSwiperIsLoaded(() => {
          // Vars
          let $this = $(entry.target);

          let index = new Date().getTime();

          // Add unique classes
          $this.addClass("cases__" + index);
          $this.find(".pagination__bullets").addClass("bullets__" + index);
          $this.find(".pagination__scrollbar").addClass("bullets__" + index);

          // Settings
          new Swiper(".cases__" + index, {
            slidesPerView: "auto",
            direction: "horizontal",
            spaceBetween: 20,
            effect: "slide",
            speed: 600,
            autoplay: {
              delay: "10000",
              pauseOnMouseEnter: true,
              disableOnInteraction: false,
            },
            navigation: false,
            pagination: {
              el: ".pagination__bullets",
              type: "bullets",
              clickable: true,
            },
            allowTouchMove: true,
            loop: $(".cases__" + index + " .swiper-slide").length > 1 ? true : false,
          });
        });

        observer.unobserve(entry.target);
      });
    },
    
    // MISC
    observer = new IntersectionObserver(handleIntersection);

    scrollTriggerElements.forEach(element => observer.observe(element));
});
