"use strict";

/**
 * ROI Calulator Function
 */
function numberWithCommas(x) {
	return x.toString().replace(/\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g, ",");
}

$(".roi__input").on("keyup", function () {
	// Vars
	let // Interval related
		roi_days = $("#roi__days").val(),
		monthly = roi_days / 12,
		yearly = roi_days,
		// Pop & Desktop related
		roi_calls = $("#roi__calls").val(),
		roi_agents = $("#roi__agents").val() / (100).toFixed(2),
		// All related
		roi_salary = $("#roi__salary").val(),
		// Dial related
		roi_outbound = $("#roi__outbound").val(),
		roi_dial = $("#roi__dial").val(),
		// UQ related
		roi_idle = $("#roi__idle").val(),
		roi_voice = $("#roi__voice").val(),
		roi_uq = $("#roi__uq").val() / (100).toFixed(2),
		// Pop related
		roi_pop = $("#roi__pop").val(),
		roi_crm = $("#roi__crm").val() / (100).toFixed(2),
		// Desktop related
		roi_desktop = $("#roi__desktop").val(),
		// Results
		dial_hours = $("#roi__dial__hours"),
		dial_usd = $("#roi__dial__usd"),
		// Pop
		pop_hours = $("#roi__pop__hours"),
		pop_usd = $("#roi__pop__usd"),
		// Desktop
		destkop_hours = $("#roi__desktop__hours"),
		desktop_usd = $("#roi__desktop__usd"),
		// UQ
		uq_hours = $("#roi__uq__hours"),
		uq_usd = $("#roi__uq__usd"),
		// Total
		total_hours = $("#roi__total__hours"),
		total_usd = $("#roi__total__usd"),
		// Dial
		daily_dial_in_hours = (roi_outbound * roi_dial) / 3600,
		daily_dial_in_usd = daily_dial_in_hours * roi_salary,
		// Pop
		daily_pop_in_hours = (roi_pop * roi_calls * roi_agents * roi_crm) / 3600,
		daily_pop_in_usd = daily_pop_in_hours * roi_salary,
		// Desktop
		daily_dekstop_in_hours = (roi_desktop * roi_calls * roi_agents) / 3600,
		daily_desktop_in_usd = daily_dekstop_in_hours * roi_salary,
		// UQ
		daily_uq_in_hours = (roi_idle * roi_voice * roi_uq) / 60,
		daily_uq_in_usd = daily_uq_in_hours * roi_salary,
		// Total
		total_in_hours =
			daily_dial_in_hours +
			daily_pop_in_hours +
			daily_dekstop_in_hours +
			daily_uq_in_hours,
		total_in_usd =
			daily_dial_in_usd +
			daily_pop_in_usd +
			daily_desktop_in_usd +
			daily_uq_in_usd;

	/*if (
		roi_days &&
		((roi_outbound && roi_dial) ||
			(roi_pop && roi_calls && roi_agents && roi_crm) ||
			(roi_desktop && roi_calls && roi_agents) ||
			(roi_idle && roi_voice && roi_uq))
	) {*/
	if ($(".roi__calc__interval__item.daily").hasClass("active")) {
		// Dial
		dial_hours.text(numberWithCommas(daily_dial_in_hours.toFixed()));
		dial_usd.text(numberWithCommas(daily_dial_in_usd.toFixed(2)));
		// Pop
		pop_hours.text(numberWithCommas(daily_pop_in_hours.toFixed()));
		pop_usd.text(numberWithCommas(daily_pop_in_usd.toFixed()));
		// Desktop
		destkop_hours.text(numberWithCommas(daily_dekstop_in_hours.toFixed()));
		desktop_usd.text(numberWithCommas(daily_desktop_in_usd.toFixed()));
		// UQ
		uq_hours.text(numberWithCommas(daily_uq_in_hours.toFixed()));
		uq_usd.text(numberWithCommas(daily_uq_in_usd.toFixed()));
		// Total
		total_hours.text(numberWithCommas(total_in_hours.toFixed()));
		total_usd.text(numberWithCommas(total_in_usd.toFixed(2)));
	} else if ($(".roi__calc__interval__item.monthly").hasClass("active")) {
		dial_hours.text(
			numberWithCommas((daily_dial_in_hours * monthly).toFixed())
		);
		dial_usd.text(numberWithCommas((daily_dial_in_usd * monthly).toFixed(2)));
		pop_hours.text(numberWithCommas((daily_pop_in_hours * monthly).toFixed()));
		pop_usd.text(numberWithCommas((daily_pop_in_usd * monthly).toFixed()));
		destkop_hours.text(
			numberWithCommas((daily_dekstop_in_hours * monthly).toFixed())
		);
		desktop_usd.text(
			numberWithCommas((daily_desktop_in_usd * monthly).toFixed())
		);
		uq_hours.text(numberWithCommas((daily_uq_in_hours * monthly).toFixed()));
		uq_usd.text(numberWithCommas((daily_uq_in_usd * monthly).toFixed()));
		total_hours.text(numberWithCommas((total_in_hours * monthly).toFixed()));
		total_usd.text(numberWithCommas((total_in_usd * monthly).toFixed(2)));
	} else if ($(".roi__calc__interval__item.yearly").hasClass("active")) {
		dial_hours.text(numberWithCommas((daily_dial_in_hours * yearly).toFixed()));
		dial_usd.text(numberWithCommas((daily_dial_in_usd * yearly).toFixed(2)));
		pop_hours.text(numberWithCommas((daily_pop_in_hours * yearly).toFixed()));
		pop_usd.text(numberWithCommas((daily_pop_in_usd * yearly).toFixed()));
		destkop_hours.text(
			numberWithCommas((daily_dekstop_in_hours * yearly).toFixed())
		);
		desktop_usd.text(
			numberWithCommas((daily_desktop_in_usd * yearly).toFixed())
		);
		uq_hours.text(numberWithCommas((daily_uq_in_hours * yearly).toFixed()));
		uq_usd.text(numberWithCommas((daily_uq_in_usd * yearly).toFixed()));
		total_hours.text(numberWithCommas((total_in_hours * yearly).toFixed()));
		total_usd.text(numberWithCommas((total_in_usd * yearly).toFixed(2)));
	}
	//}
});

/**
 * ROI Calulator - Interval Function
 */
$(".roi__calc__interval__item").on("click", function () {
	// Vars
	let interval = $(this),
		//daily = $("span.daily"),
		//saved = $("span.saved"),
		interval_items = $(".roi__calc__interval__item");

	// Remove active classes from all item
	interval_items.removeClass("active");

	// Add active class to clicked element
	interval.addClass("active");

	// Trigger keyup to update values
	$("#roi__days").trigger("keyup");

	// Daily
	/*if (interval.hasClass("daily")) {
		daily.text(roi_calculator_params.daily);
		saved.text(roi_calculator_params.saved_day);
		// Monthly
	} else if (interval.hasClass("monthly")) {
		daily.text(roi_calculator_params.monthly);
		saved.text(roi_calculator_params.saved_month);
		// Yearly
	} else if (interval.hasClass("yearly")) {
		daily.text(roi_calculator_params.yearly);
		saved.text(roi_calculator_params.saved_year);
	}*/
});
