"use strict";

$(".trusted__by").each(function (index) {
	let $this = $(this);
	$this.addClass("trusted__by__" + index);

	// Active logos
	let active_logos = $(".trusted__by__" + index + " .trusted__by__item.active")
		.map(function () {
			return $(this);
		})
		.get();

	// Hidden logos
	let hidden_logos = $(".trusted__by__" + index + " .trusted__by__item.hidden")
		.map(function () {
			return $(this);
		})
		.get();

	// Check if has any hidden logos
	if (hidden_logos.length > 0) {
		setInterval(function () {
			// Vars
			let active_num = Math.floor(Math.random() * active_logos.length),
				hidden_num = Math.floor(Math.random() * hidden_logos.length),
				activeItems = $(".trusted__by__item.active"),
				activeItem = active_logos[active_num],
				hiddenItem = hidden_logos[hidden_num],
				activeLink = activeItem.find("a"),
				hiddenLink = hiddenItem.find("a"),
				activeLinkSrc = activeLink.attr("href"),
				hiddenLinkSrc = hiddenLink.attr("href"),
				activeLogo = activeItem.find(".trusted__by__logo"),
				hiddenLogo = hiddenItem.find(".trusted__by__logo"),
				activeSrc = activeLogo.attr("src"),
				hiddenSrc = hiddenLogo.attr("src"),
				activeAlt = activeLogo.attr("alt"),
				hiddenAlt = hiddenLogo.attr("alt");

			// Remove has-moved class
			activeItems.removeClass("has-moved");

			// Add has-moved class to active
			activeItem.addClass("has-moved");

			// Replace images src | alt
			hiddenLink.attr("href", activeLinkSrc);
			hiddenLogo.attr("src", activeSrc);
			hiddenLogo.attr("alt", activeAlt);
			activeItem.addClass("is-active");

			// Replace images src
			setTimeout(function () {
				activeItem.removeClass("is-active");
				activeLink.attr("href", hiddenLinkSrc);
				activeLogo.attr("src", hiddenSrc);
				activeLogo.attr("alt", hiddenAlt);
			}, 500);
		}, 4000);
	}
});
