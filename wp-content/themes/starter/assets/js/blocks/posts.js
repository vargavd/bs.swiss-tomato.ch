"use strict";

jQuery(function () {
	// Constants
	const CAT = $("h2.post__cat"),
		TABS = $("h2.posts__tab__item"),
		FILTER = $("form#post__filter"),
		RESULT = $("div#post__result"),
		RESET = $("h2.post__reset");

	// Apply Filter
	FILTER.on("submit", function (e) {
		e.preventDefault();

		// Vars
		let max = $("input#max");

		// AJAX request
		$.ajax({
			url: FILTER.attr("action"),
			data: FILTER.serialize() + "&nonce=" + ajax_filter_loadmore_params.nonce,
			dataType: "json",
			type: FILTER.attr("method"),
			beforeSend: function (xhr) {
				// Add Loading class
				FILTER.addClass("loading");
			},
			success: function (data) {
				// Reset Page Param
				ajax_filter_loadmore_params.current_page = 1;

				// Update Max Page value
				max.val(data.max_page);

				// Result
				RESULT.html(data.content); // insert new posts

				// Remove Loading class
				FILTER.removeClass("loading");

				// Add animation to Post Items
				setTimeout(function () {
					$(".post__item").addClass("sal-animate");
				}, 50);
			},
			error: function (request, status, error) {
				alert(request.error);
			},
		});
		return false;
	});

	// Category Selector
	CAT.on("click", function () {
		// Vars
		let $this = $(this),
			value = $(this).attr("data-cat"),
			cat = $('input[name="cat"]');

		// Check active status
		if (!$this.hasClass("active")) {
			// Remove / Add active class
			TABS.removeClass("active");
			$this.addClass("active");

			// Add value
			cat.val(value);

			// Trigger Submit
			FILTER.trigger("submit");
		}
	});

	// Kill them all
	RESET.on("click", function () {
		// Vars
		let $this = $(this),
			cat = $('input[name="cat"]');

		// Check active status
		if (!$this.hasClass("active")) {
			// Remove / Add active class
			TABS.removeClass("active");
			$this.addClass("active");

			// Reset value
			cat.val("");

			// Trigger Submit
			FILTER.trigger("submit");
		}
	});

	// Load more
	$(document).on("click", "button.load__more", function (e) {
		e.preventDefault();

		// Vars
		let button = $(this),
			ppp = $("#ppp").val(),
			max = $("#max").val(),
			items = $(".post__container"),
			cat = $("#cat").val(),
			lodamore = $("#loadmore__security").val(),
			data = {
				action: "loadmore",
				ppp: ppp,
				page: ajax_filter_loadmore_params.current_page,
				cat: cat,
				loadmore__security: lodamore,
				nonce: ajax_filter_loadmore_params.nonce,
			};

		// AJAX request
		$.ajax({
			url: ajax_filter_loadmore_params.ajaxurl, // AJAX handler
			data: data,
			type: "POST",
			beforeSend: function (xhr) {
				// Loading message
				button.text("Loading...");
			},
			success: function (data) {
				if (data) {
					button.text("Load more");
					items.append(data); // insert new posts
					ajax_filter_loadmore_params.current_page++;

					// If reach the last page, remove the button
					if (ajax_filter_loadmore_params.current_page == max) {
						button.remove();
					}

					// Add animation to Post Items
					setTimeout(function () {
						$(".post__item").addClass("sal-animate");
					}, 50);
				} else {
					// if no data, remove the button as well
					button.remove();
				}
			},
		});
	});
});
