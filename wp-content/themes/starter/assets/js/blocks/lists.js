"use strict";

let listsEntered = false;

if ($(".lists").hasClass("number__animated")) {
	$(window).scroll(function () {
		var listTop =
			$(".number__animated").offset().top - window.innerHeight + 200;

		if (listsEntered == false && $(window).scrollTop() > listTop) {
			$(".list__number").each(function () {
				let size = $(this).text().split(".")[1]
					? $.trim($(this).text().split(".")[1]).length
					: 0;

				$(this)
					.prop("Counter", 0)
					.animate(
						{
							Counter: $(this).text(),
						},
						{
							duration: 1500,
							easing: "swing",
							step: function (now) {
								$(this).text(parseFloat(now).toFixed(size));
							},
						}
					);
			});
			listsEntered = true;
		}
	});
}
