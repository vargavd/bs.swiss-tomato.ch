jQuery(document).ready(function ($) {
  let
    // DOM
    scrollTriggerElements = document.querySelectorAll('.testimonials .swiper-container'),

    // EVENTS
    handleIntersection = function (entries) {
      entries.map(entry => {
        if (!entry.isIntersecting) {
          return;
        }

        swiperProvider.callMeBackWhenSwiperIsLoaded(() => {
          let $this = $(entry.target);

          let index = new Date().getTime();
    
          // Add unique classes
          $this.addClass("testimonials__" + index);
          $this.find(".pagination__bullets").addClass("bullets__" + index);
          $this.find(".pagination__scrollbar").addClass("bullets__" + index);
  
          if (!$this.hasClass("history")) {
            // Settings
            new Swiper(".testimonials__" + index, {
              slidesPerView: "auto",
              direction: "horizontal",
              spaceBetween: 20,
              effect: "slide",
              speed: 600,
              autoplay: {
                delay: "4000",
                pauseOnMouseEnter: true,
                disableOnInteraction: false,
              },
              navigation: false,
              pagination: {
                el: ".pagination__bullets",
                type: "bullets",
                clickable: true,
              },
              allowTouchMove: true,
              loop:
                $(".testimonials__" + index + " .swiper-slide").length > 1
                  ? true
                  : false,
            });
          } else {
            new Swiper(".testimonials__" + index, {
              centeredSlides: true,
              slidesPerView: "auto",
              //mousewheel: true,
              spaceBetween: 20,
              scrollbar: {
                el: ".swiper-scrollbar",
                dragClass: "swiper-scrollbar-drag",
                draggable: true,
                snapOnRelease: true,
              },
              allowTouchMove: true,
            });
          }
        });

        observer.unobserve(entry.target);
      });
    },
    
    // MISC
    observer = new IntersectionObserver(handleIntersection);

    scrollTriggerElements.forEach(element => observer.observe(element));
});

