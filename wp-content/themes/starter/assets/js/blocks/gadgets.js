jQuery(document).ready(function ($) {
  let
    // DOM
    scrollTriggerElements = document.querySelectorAll('.gadgets .swiper-container'),

    // EVENTS
    handleIntersection = function (entries) {
      entries.map(entry => {
        if (!entry.isIntersecting) {
          return;
        }

        swiperProvider.callMeBackWhenSwiperIsLoaded(() => {
          // Vars
          let $this = $(entry.target);

          let index = new Date().getTime();

          // Add unique classes
          $this.addClass("gadgets__" + index);

          // Settings
          new Swiper(".gadgets__" + index, {
            slidesPerView: "auto",
            spaceBetween: 10,
            longSwipes: false,
            centeredSlides: true,
            speed: 9000,
            autoplay: {
              delay: 1,
              pauseOnMouseEnter: true,
              disableOnInteraction: false,
            },
            navigation: false,
            pagination: false,
            allowTouchMove: false,
            loop: $(".gadgets__" + index + " .swiper-slide").length > 1 ? true : false,
          });

          lightGallery(
            document.querySelector(".gadgets__" + index),
            {
              licenseKey: "KisE7ydS5b0m9wFCqokH",
              selector: ".swiper-slide",
              plugins: [lgZoom],
              zoomFromOrigin: false,
              allowMediaOverlap: false,
              download: false,
              counter: false,
              zoom: true,
              speed: 500,
            }
          );
        });

        observer.unobserve(entry.target);
      });

      
    },
    
    // MISC
    observer = new IntersectionObserver(handleIntersection);

    scrollTriggerElements.forEach(element => observer.observe(element));
});

/*
$(".gadgets__wrapper").lightGallery({
	selector: ".swiper-slide",
	mode: "lg-slide",
	//slideEndAnimatoin: true,
	appendThumbnailsTo: ".lg-outer",
	animateThumb: false,
	allowMediaOverlap: true,
	download: false,
	counter: false,
});*/
