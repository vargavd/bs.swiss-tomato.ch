"use strict";

$(window).scroll(function () {
	$(".facts").each(function (index) {
		let $this = $(this);
		$this.addClass("facts__" + index);

		let factsTop =
			$(".facts__" + index).offset().top - window.innerHeight + 200;

		if (
			!$(".facts__" + index).hasClass("in__view") &&
			$(window).scrollTop() > factsTop
		) {
			$(".facts__" + index + " .facts__number").each(function () {
				let size = $(this).text().split(".")[1]
					? $.trim($(this).text().split(".")[1]).length
					: 0;

				$(this)
					.prop("Counter", 0)
					.animate(
						{
							Counter: $(this).text(),
						},
						{
							duration: 1500,
							easing: "swing",
							step: function (now) {
								$(this).text(parseFloat(now).toFixed(size));
							},
						}
					);
			});
			$(".facts__" + index).addClass("in__view");
		}
	});
});
