"use strict";

/**
 * Tab function
 */
function openProductTab(evt, tabName) {
	let i, productContent, productLinks;

	productContent = document.getElementsByClassName("products__container");
	for (i = 0; i < productContent.length; i++) {
		if (tabName === productContent[i].dataset.product) {
			if (!productContent[i].classList.contains("active")) {
				productContent[i].className += " active";
			}
		} else {
			productContent[i].className = productContent[i].className.replace(
				" active",
				""
			);
		}
	}
	productLinks = document.getElementsByClassName("products__links");
	for (i = 0; i < productLinks.length; i++) {
		productLinks[i].className = productLinks[i].className.replace(
			" active",
			""
		);
	}
	evt.currentTarget.className += " active";
}

/**
 * Set Title height as CSS variable
 */
window.addListenerMulti(
	window,
	"load resize",
	function (event) {
		// Vars
		let productsContent = document.getElementsByClassName("products__content"),
			productsTitle = document.getElementsByClassName("products__title");

		for (i = 0; i < productsContent.length; i++) {
			productsContent[i].setAttribute(
				"style",
				"--title-height:" + productsTitle[i].offsetHeight
			);
		}
	},
	true
);
